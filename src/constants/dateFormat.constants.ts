export const ISO_DATE_FORMAT = 'YYYY-MM-DD';
// export const DATE_TIME_FORMAT = 'YYYY-MM-DD HH:mm';
export const DATE_TIME_FORMAT = 'HH:mm DD/MM/YYYY';
export const TIME_FORMAT = 'HH:mm';
export const YEAR_FORMAT = 'YYYY';