import { EquipmentStatus, RiskLevel } from './equipment.type';

export interface StatisticDashboard {
  countByDepartment: CountEquipmentByDepartment[];
  countByRiskLevels: CountEquipmentByRiskLevel[];
  countByEquipmentStatuses: CountEquipmentByStatus[];
  countRepairingByDepartment: CountEquipmentByDepartment[];
  countBrokenByDepartment: CountEquipmentByDepartment[];
  countEquipmentByDepartmentAndStatus: CountEquipmentByDepartmentAndStatus[];
}

export interface CountEquipmentByGroupAndCategory {
  groupId: number,
  groupName: string;
  count: number;
  countByCategory: CountEquipmentByCategory[];
}

export interface CountEquipmentByCategory {
  categoryId: number,
  categoryName: string;
  count: number;
}

export interface CountEquipmentByDepartment {
  departmentId: number;
  departmentName: string;
  count: number;
  // countByEquipmentStatus: Map<EquipmentStatus, number>;
}

export interface CountEquipmentByDepartmentAndStatus {
  departmentId: number;
  departmentName: string;
  count: number;
  countByEquipmentStatus: CountEquipmentByStatus[];
}

export interface CountEquipmentByDepartmentAndRiskLevel {
  departmentId: number;
  departmentName: string;
  count: number;
  countByRiskLevel: CountEquipmentByRiskLevel[];
}

export interface CountEquipmentByDepartmentAndRiskLevelTable {
  departmentId: number;
  departmentName: string;
  count: number;
  [key: string]: number | string;
}

export interface CountEquipmentByDepartmentAndRiskLevelChart {
  departmentId: number;
  departmentName: string;
  count: number;
  riskLevel: string;
  countRiskLevel: number;
}

export interface CountEquipmentByDepartmentAndStatusTable {
  departmentId: number;
  departmentName: string;
  count: number;
  [key: string]: number | string;
}
export interface CountEquipmentByDepartmentAndStatusChart {
  departmentId: number;
  departmentName: string;
  count: number;
  status: EquipmentStatus;
  countStatus: number;
}
export interface CountEquipmentByRiskLevel {
  riskLevel: string;
  count: number;
}
export interface CountEquipmentByStatus {
  status: EquipmentStatus;
  count: number;
  image?: any; //image url for frontend (not response from backend)
}
