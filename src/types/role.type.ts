import { CheckboxOptionType } from 'antd';
import { Authority } from '../constants/authority';

export interface RoleDto {
  id: number;
  name?: string;
  description?: string;
  scopes?: string[];
}

export interface GetRolesQueryParam {
  keyword?: string;
}

export interface UpsertRoleForm {
  name?: string;
  description?: string;
  permissions?: string[];
}

export interface RoleFullInfoDto {
  id: number;
  name: string;
  description: string;
  permissions: PermissionDto[];
}

export interface PermissionDto {
  id: number;
  name: string;
  description: string;
}

export const PERMISSIONS: (string | CheckboxOptionType[])[][] = [
  //key, title, authority prefix (permission group), options
  ['Dashboard', 'DASHBOARD', 
  [
    { label: 'Xem', value: Authority.DASHBOARD_READ},
    { label: 'Thiết bị y tế', value: Authority.MEDICAL_DASHBOARD_READ },
  { label: 'Tài sản công', value: Authority.OFFICE_DASHBOARD_READ }
  ]],

  // ['Dashboard Thiết bị y tế', 'MEDICAL.DASHBOARD', [{ label: 'Xem', value: Authority.MEDICAL_DASHBOARD_READ }]],
  // ['Dashboard Tài sản công', 'OFFICE.DASHBOARD', [{ label: 'Xem', value: Authority.OFFICE_DASHBOARD_READ }]],
  // ['Thống kê thiết bị y tế', 'MEDICAL', [{ label: 'Thống kê', value: Authority.MEDICAL_STATISTIC }]],
   [
    'Quản lý thiết bị y tế', 'MEDICAL', [
      { label: 'Xem', value: Authority.MEDICAL_READ },
      { label: 'Thêm', value: Authority.MEDICAL_CREATE },
      { label: 'Sửa', value: Authority.MEDICAL_UPDATE },
      { label: 'Xóa', value: Authority.MEDICAL_DELETE },
      { label: 'Thống kê', value: Authority.MEDICAL_STATISTIC }
    ],
  ],
  [
    'Quản lý tài sản công', 'OFFICE', [
      { label: 'Xem', value: Authority.OFFICE_READ },
      { label: 'Thêm', value: Authority.OFFICE_CREATE },
      { label: 'Sửa', value: Authority.OFFICE_UPDATE },
      { label: 'Xóa', value: Authority.OFFICE_DELETE },
      { label: 'Thống kê', value: Authority.OFFICE_STATISTIC }

    ],
  ],
   [
    'Bàn giao thiết bị y tế', 'MEDICAL.HANDOVER', [
      { label: 'Xem', value: Authority.MEDICAL_HANDOVER_READ },
      { label: 'Tạo phiếu', value: Authority.MEDICAL_HANDOVER_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.MEDICAL_HANDOVER_ACCEPT },
    ],
  ], 
  [
    'Bàn giao tài sản công', 'OFFICE.HANDOVER', [
      { label: 'Xem', value: Authority.OFFICE_HANDOVER_READ },
      { label: 'Tạo phiếu', value: Authority.OFFICE_HANDOVER_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.OFFICE_HANDOVER_ACCEPT },
    ],
  ], 
  [
    'Bảo trì, bảo dưỡng thiết bị y tế', 'MEDICAL.MAINTENANCE', [
      { label: 'Xem', value: Authority.MEDICAL_MAINTENANCE_READ },
      { label: 'Tạo phiếu', value: Authority.MEDICAL_MAINTENANCE_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.MEDICAL_MAINTENANCE_ACCEPT },
      { label: 'Cập nhật phiếu', value: Authority.MEDICAL_MAINTENANCE_UPDATE },
    ],
  ],
  [
    'Bảo trì, bảo dưỡng tài sản công', 'OFFICE.MAINTENANCE', [
      { label: 'Xem', value: Authority.OFFICE_MAINTENANCE_READ },
      { label: 'Tạo phiếu', value: Authority.OFFICE_MAINTENANCE_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.OFFICE_MAINTENANCE_ACCEPT },
      { label: 'Cập nhật phiếu', value: Authority.OFFICE_MAINTENANCE_UPDATE },
    ],
  ],
  [
    'Kiểm định thiết bị y tế', 'MEDICAL.INSPECTION', [
      { label: 'Xem', value: Authority.MEDICAL_INSPECTION_READ },
      { label: 'Tạo phiếu', value: Authority.MEDICAL_INSPECTION_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.MEDICAL_INSPECTION_ACCEPT },
      { label: 'Cập nhật phiếu', value: Authority.MEDICAL_INSPECTION_UPDATE },
    ],
  ], [
    'Điều chuyển thiết bị y tế', 'MEDICAL.TRANSFER', [
      { label: 'Xem', value: Authority.MEDICAL_TRANSFER_READ },
      { label: 'Tạo phiếu', value: Authority.MEDICAL_TRANSFER_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.MEDICAL_TRANSFER_ACCEPT },
    ],
  ],
  [
    'Điều chuyển tài sản công', 'OFFICE.TRANSFER', [
      { label: 'Xem', value: Authority.OFFICE_TRANSFER_READ },
      { label: 'Tạo phiếu', value: Authority.OFFICE_TRANSFER_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.OFFICE_TRANSFER_ACCEPT },
    ],
  ], [
    'Báo hỏng thiết bị y tế', 'MEDICAL.REPORT_BROKEN', [
      { label: 'Xem', value: Authority.MEDICAL_REPORT_BROKEN_READ },
      { label: 'Tạo phiếu', value: Authority.MEDICAL_REPORT_BROKEN_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.MEDICAL_REPORT_BROKEN_ACCEPT },
    ],
  ],
  [
    'Báo hỏng tài sản công', 'OFFICE.REPORT_BROKEN', [
      { label: 'Xem', value: Authority.OFFICE_REPORT_BROKEN_READ },
      { label: 'Tạo phiếu', value: Authority.OFFICE_REPORT_BROKEN_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.OFFICE_REPORT_BROKEN_ACCEPT },
    ],
  ],  [
    'Sửa chữa thiết bị y tế', 'MEDICAL.REPAIR', [
      { label: 'Xem', value: Authority.MEDICAL_REPAIR_READ },
      { label: 'Tạo phiếu', value: Authority.MEDICAL_REPAIR_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.MEDICAL_REPAIR_ACCEPT },
      { label: 'Cập nhật phiếu', value: Authority.MEDICAL_REPAIR_UPDATE },
      { label: 'Nghiệm thu', value: Authority.MEDICAL_REPAIR_ACCEPTANCE_TESTING },
    ],
  ],
  [
    'Sửa chữa tài sản công', 'OFFICE.REPAIR', [
      { label: 'Xem', value: Authority.OFFICE_REPAIR_READ },
      { label: 'Tạo phiếu', value: Authority.OFFICE_REPAIR_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.OFFICE_REPAIR_ACCEPT },
      { label: 'Cập nhật phiếu', value: Authority.OFFICE_REPAIR_UPDATE },
      { label: 'Nghiệm thu', value: Authority.OFFICE_REPAIR_ACCEPTANCE_TESTING },
    ],
  ], [
    'Thanh lý thiết bị y tế', 'MEDICAL.LIQUIDATION', [
      { label: 'Xem', value: Authority.MEDICAL_LIQUIDATION_READ },
      { label: 'Tạo phiếu', value: Authority.MEDICAL_LIQUIDATION_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.MEDICAL_LIQUIDATION_ACCEPT },
    ],
  ], 
  [
    'Thanh lý tài sản công', 'OFFICE.LIQUIDATION', [
      { label: 'Xem', value: Authority.OFFICE_LIQUIDATION_READ },
      { label: 'Tạo phiếu', value: Authority.OFFICE_LIQUIDATION_CREATE },
      { label: 'Phê duyệt phiếu', value: Authority.OFFICE_LIQUIDATION_ACCEPT },
    ],
  ],
  [
    'Kiểm kê thiết bị y tế', 'MEDICAL.INVENTORY', [
      { label: 'Xem', value: Authority.MEDICAL_INVENTORY_READ },
      { label: 'Tạo phiếu', value: Authority.MEDICAL_INVENTORY_CREATE },
      { label: 'Cập nhật phiếu', value: Authority.MEDICAL_INVENTORY_UPDATE },
    ],
  ],
  
  [
    'Quản lý vật tư theo kèm thiết bị', 'SUPPLY', [
      { label: 'Xem', value: Authority.SUPPLY_READ },
      { label: 'Thêm', value: Authority.SUPPLY_CREATE },
      { label: 'Sửa', value: Authority.SUPPLY_UPDATE },
      { label: 'Xóa', value: Authority.SUPPLY_DELETE },
    ],
  ], 
  [
    'Quản lý nhóm thiết bị y tế', 'MEDICAL_GROUP', [
      { label: 'Xem', value: Authority.MEDICAL_GROUP_READ },
      { label: 'Thêm', value: Authority.MEDICAL_GROUP_CREATE },
      { label: 'Sửa', value: Authority.MEDICAL_GROUP_UPDATE },
      { label: 'Xóa', value: Authority.MEDICAL_GROUP_DELETE },
    ],
  ],
  [
    'Quản lý loại thiết bị y tế', 'MEDICAL_CATEGORY', [
      { label: 'Xem', value: Authority.MEDICAL_CATEGORY_READ },
      { label: 'Thêm', value: Authority.MEDICAL_CATEGORY_CREATE },
      { label: 'Sửa', value: Authority.MEDICAL_CATEGORY_UPDATE },
      { label: 'Xóa', value: Authority.MEDICAL_CATEGORY_DELETE },
    ],
  ],
  [
    'Quản lý nhóm tài sản công ', 'OFFICE_GROUP', [
      { label: 'Xem', value: Authority.OFFICE_GROUP_READ },
      { label: 'Thêm', value: Authority.OFFICE_GROUP_CREATE },
      { label: 'Sửa', value: Authority.OFFICE_GROUP_UPDATE },
      { label: 'Xóa', value: Authority.OFFICE_GROUP_DELETE },
    ],
  ],
  [
    'Quản lý loại tài sản công', 'OFFICE_CATEGORY', [
      { label: 'Xem', value: Authority.OFFICE_CATEGORY_READ },
      { label: 'Thêm', value: Authority.OFFICE_CATEGORY_CREATE },
      { label: 'Sửa', value: Authority.OFFICE_CATEGORY_UPDATE },
      { label: 'Xóa', value: Authority.OFFICE_CATEGORY_DELETE },
    ],
  ],
   [
    'Quản lý loại vật tư', 'SUPPLY_UNIT', [
      { label: 'Xem', value: Authority.SUPPLY_UNIT_READ },
      { label: 'Thêm', value: Authority.SUPPLY_UNIT_CREATE },
      { label: 'Sửa', value: Authority.SUPPLY_UNIT_UPDATE },
      { label: 'Xóa', value: Authority.SUPPLY_UNIT_DELETE },
    ],
  ],  [
    'Quản lý khoa phòng', 'DEPARTMENT', [
      { label: 'Xem', value: Authority.DEPARTMENT_READ },
      { label: 'Thêm', value: Authority.DEPARTMENT_CREATE },
      { label: 'Sửa', value: Authority.DEPARTMENT_UPDATE },
      { label: 'Xóa', value: Authority.DEPARTMENT_DELETE },
    ],
  ], [
    'Quản lý người dùng', 'USER', [
      { label: 'Xem', value: Authority.USER_READ },
      { label: 'Thêm', value: Authority.USER_CREATE },
      { label: 'Sửa', value: Authority.USER_UPDATE },
      { label: 'Xóa', value: Authority.USER_DELETE },
    ],
  ], [
    'Quản lý chức vụ', 'ROLE', [
      { label: 'Xem', value: Authority.ROLE_READ },
      { label: 'Thêm', value: Authority.ROLE_CREATE },
      { label: 'Sửa', value: Authority.ROLE_UPDATE },
      { label: 'Xóa', value: Authority.ROLE_DELETE },
    ],
  ], [
    'Quản lý đơn vị tính', 'EQUIPMENT_UNIT', [
      { label: 'Xem', value: Authority.EQUIPMENT_UNIT_READ },
      { label: 'Thêm', value: Authority.EQUIPMENT_UNIT_CREATE },
      { label: 'Sửa', value: Authority.EQUIPMENT_UNIT_UPDATE },
      { label: 'Xóa', value: Authority.EQUIPMENT_UNIT_DELETE },
    ],
  ], [
    'Quản lý dự án', 'PROJECT', [
      { label: 'Xem', value: Authority.PROJECT_READ },
      { label: 'Thêm', value: Authority.PROJECT_CREATE },
      { label: 'Sửa', value: Authority.PROJECT_UPDATE },
      { label: 'Xóa', value: Authority.PROJECT_DELETE },
    ],
  ], [
    'Quản lý nhà cung cấp', 'SUPPLIER', [
      { label: 'Xem', value: Authority.SUPPLIER_READ },
      { label: 'Thêm', value: Authority.SUPPLIER_CREATE },
      { label: 'Sửa', value: Authority.SUPPLIER_UPDATE },
      { label: 'Xóa', value: Authority.SUPPLIER_DELETE },
    ],
  ], [
    'Quản lý dịch vụ', 'SERVICE', [
      { label: 'Xem', value: Authority.SERVICE_READ },
      { label: 'Thêm', value: Authority.SERVICE_CREATE },
      { label: 'Sửa', value: Authority.SERVICE_UPDATE },
      { label: 'Xóa', value: Authority.SERVICE_DELETE },
    ],
  ],
];

export interface TotalOptionAndOptionLeft {
  totalOptions: number;
  optionLeft: number;
}

export const getOptionCountForEachPermissionGroup = (): Map<string, TotalOptionAndOptionLeft> => {
  let optionCount = new Map<string, TotalOptionAndOptionLeft>();
  PERMISSIONS.forEach(permission => {
    return optionCount.set(permission[1] as string, {
      totalOptions: permission[2].length, optionLeft: permission[2].length,
    });
  });
  return optionCount;
};

