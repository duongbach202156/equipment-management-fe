import { DepartmentDto, DepartmentFullInfoDto } from './department.type';
import { EquipmentCategoryDto, EquipmentCategoryFullInfoDto } from './equipmentCategory.type';
import { EquipmentUnitDto } from './equipmentUnit.type';
import { HandoverTicketFullInfoDto } from './handover.type';
import { EquipmentSupplyUsageDto } from './equipmentSupplyUsage.type';
import { ProjectDto } from './project.type';
import { InspectionTicketDto, InspectionTicketFullInfoDto } from './equipmentInspection.type';
import { SupplierFullInfoDto } from './supplier.type';
import { TransferTicketFullInfoDto } from './transfer.type';
import { InventoryDto } from './inventory.type';
import { ReportBrokenTicketFullInfoDto } from './reportBroken.type';
import { RepairTicketFullInfoDto } from './repair.type';
import { MaintenanceTicketDto, MaintenanceTicketFullInfoDto } from './maintenance.type';
import { Moment } from 'moment';
import { LiquidationDto, LiquidationTicketFullInfoDto } from './equipmentLiquidation.type';
import { FileStorageDto } from './fileStorage.type';
import { HasAttachments } from './trait.type';
import useFormInstance from 'antd/lib/form/hooks/useFormInstance';
import { FormInstance } from 'antd';
import { EquipmentDto, EquipmentFullInfoDto, EquipmentListDto, EquipmentStatus, GetEquipmentsQueryParam, RiskLevel, UpsertEquipmentForm } from './equipment.type';


export interface MedicalEquipmentListDto extends EquipmentListDto {
  riskLevel?: RiskLevel;
  regularInspection?: number;
  regularRadiationInspection?: number;
  regularExternalQualityAssessment?: number;
  regularClinicEnvironmentInspection?: number;
  regularCVRadiation?: number;
}

export interface GetMedicalEquipmentsQueryParam extends GetEquipmentsQueryParam {
  riskLevel?: RiskLevel | string;
  jointVentureContractExpirationDateFrom?: string; // Assuming LocalDate is converted to string
  jointVentureContractExpirationDateTo?: string; // Assuming LocalDate is converted to string
  lastInspectionDateFrom?: string; // Assuming LocalDate is converted to string
  lastInspectionDateTo?: string; // Assuming LocalDate is converted to string

}

export interface MedicalEquipmentDto extends EquipmentDto{
  riskLevel?: RiskLevel;
  regularInspection?: number;
  regularRadiationInspection?: number;
  regularExternalQualityAssessment?: number;
  regularClinicEnvironmentInspection?: number;
  regularCVRadiation?: number;
  jointVentureContractExpirationDate?: string; // Assuming LocalDate is converted to string
}

export interface MedicalEquipmentFullInfoDto extends EquipmentFullInfoDto{
  riskLevel?: RiskLevel;
  regularInspection?: number;
  jointVentureContractExpirationDate?: Moment;
  equipmentSupplyUsages?: EquipmentSupplyUsageDto[];
  inspectionTickets?: InspectionTicketFullInfoDto[];
}

export interface EquipmentListInspectionDto {
  id?: number;
  name?: string;
  model?: string;
  serial?: string;
  code?: string;
  hashCode?: string;
  riskLevel?: RiskLevel;
  technicalParameter?: string;
  warehouseImportDate?: string | Moment;
  yearOfManufacture?: number;
  yearInUse?: number;
  configuration?: string;
  importPrice?: number;
  initialValue?: number;
  annualDepreciation?: number;
  usageProcedure?: string;
  note?: string;
  status?: EquipmentStatus;
  manufacturer?: string;
  manufacturingCountry?: string;
  category?: EquipmentCategoryDto;
  department?: DepartmentDto;
  regularMaintenance?: number;
  regularInspection?: number;
  lastInspectionDate?: string | Moment;
  nextInspectionDate?: string | Moment;
  regularRadiationInspection?: number;
  regularExternalQualityAssessment?: number;
  regularClinicEnvironmentInspection?: number;
  regularCVRadiation?: number;
  jointVentureContractExpirationDate?: string | Moment;
  warrantyExpirationDate?: string | Moment;
  inspectionTickets?: InspectionTicketDto[];
  deleted?: boolean;
}


export interface GetEquipmentsForTransferQueryParam {
  keyword?: string;
  equipmentStatus?: EquipmentStatus | string;
  departmentId?: number;
  categoryId?: number;
  groupId?: number;
}

export interface UpsertMedicalEquipmentForm extends UpsertEquipmentForm {
  riskLevel?: RiskLevel;
  regularInspection?: number;
  jointVentureContractExpirationDate?: Moment | string;
}

export interface AttachSupplyForm {
  equipmentId?: number;
  supplyId?: number;
  amount?: number;
  amountUsed?: number;
  note?: string;
}

export interface EquipmentImportExcelForm extends FormInstance {
  departmentId? : number;
  status?: EquipmentStatus;
  excelFile?: File;
}