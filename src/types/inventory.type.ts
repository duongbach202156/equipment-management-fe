import { Moment } from "moment";

export interface InventoryDto {
  id: number;
  name: string;
  inventoryDate: Moment | string;
  inventoryPerson: string;
  departmentName: string;
  departmentId: number;
  confirmed: boolean;
}

export interface InventoryFullInfoDto {
  id: number;
  name: string;
  inventoryDate: Moment | string;
  inventoryPerson: string;
  departmentName: string;
  departmentId: number;
  inventoryEquipmentDtoList: InventoryEquipmentDto[];
  confirmed: boolean;
}
export interface InventoryEquipmentDto {
  id: number;
  note: string;
  name: string;
  equipmentId: number;
  model: string;
  serial: string;
  inventoried: boolean;
  inventoryId: number;
}
export interface InventoryEquipmentForm {
  id: number;
  note: string;
  inventoried: boolean;
  equipmentId: number;
  inventoryId: number;
}
export interface UpsertInventoryForm {
  name: string;
  inventoryDate: string | Moment | undefined;
  departmentId: number;
  departmentName: string;
  inventoryPerson: string;
  inventoryEquipmentList: InventoryEquipmentForm[];
}
export interface GetInventoryQueryParam {
  name?: string;
  inventoryDateFrom?: Moment | string;
  inventoryDateTo?: Moment | string;
  inventoryPerson?: string;
  departmentId?: number;
  keyword?: string;
}
export interface GetInventoryEquipmentQueryParam {
  note?: string;
  name?: string;
  model?: string;
  serial?: string;
  keyword?: string;
}