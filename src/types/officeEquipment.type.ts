import { DepartmentDto, DepartmentFullInfoDto } from './department.type';
import { EquipmentCategoryDto, EquipmentCategoryFullInfoDto } from './equipmentCategory.type';
import { EquipmentUnitDto } from './equipmentUnit.type';
import { HandoverTicketFullInfoDto } from './handover.type';
import { EquipmentSupplyUsageDto } from './equipmentSupplyUsage.type';
import { ProjectDto } from './project.type';
import { InspectionTicketDto, InspectionTicketFullInfoDto } from './equipmentInspection.type';
import { SupplierFullInfoDto } from './supplier.type';
import { TransferTicketFullInfoDto } from './transfer.type';
import { InventoryDto } from './inventory.type';
import { ReportBrokenTicketFullInfoDto } from './reportBroken.type';
import { RepairTicketFullInfoDto } from './repair.type';
import { MaintenanceTicketDto, MaintenanceTicketFullInfoDto } from './maintenance.type';
import { Moment } from 'moment';
import { LiquidationDto, LiquidationTicketFullInfoDto } from './equipmentLiquidation.type';
import { FileStorageDto } from './fileStorage.type';
import { HasAttachments } from './trait.type';
import useFormInstance from 'antd/lib/form/hooks/useFormInstance';
import { FormInstance } from 'antd';
import { EquipmentDto, EquipmentFullInfoDto, EquipmentListDto, EquipmentStatus, GetEquipmentsQueryParam, RiskLevel, UpsertEquipmentForm } from './equipment.type';


export interface OfficeEquipmentListDto extends EquipmentListDto {
}

export interface GetOfficeEquipmentsQueryParam extends GetEquipmentsQueryParam {

}

export interface OfficeEquipmentDto extends EquipmentDto{
}

export interface OfficeEquipmentFullInfoDto extends EquipmentFullInfoDto{
  
}

export interface UpsertOfficeEquipmentForm extends UpsertEquipmentForm {
}