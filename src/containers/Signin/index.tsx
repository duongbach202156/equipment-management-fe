import React, { useState } from 'react';
import bg from 'assets/bg.jpeg';
import { LoginOutlined, PoweroffOutlined } from '@ant-design/icons';
import Text from 'antd/lib/typography/Text';
import { Link } from 'react-router-dom';
import { authActions, LoginPayLoad, selectIsLoading, selectMessageLogin } from 'store/slices/auth.slice';
import { useDispatch, useSelector } from 'react-redux';
import { Alert, Button, Divider, Form, Input, Space } from 'antd';
import { getCurrentUser } from 'utils/globalFunc.util';

const Signin: React.FC = () => {

  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsLoading);
  const messageLogin = useSelector(selectMessageLogin);

  const handleLogin = (values: LoginPayLoad) => {
    dispatch(authActions.login(values));
    
  }

  return (
    <>
      <div className='grid grid-cols-1 w-screen h-screen'>
        {/* <div className='flex flex-col justify-between bg-amber-50'> */}
          
          {/* <div className='mb-4 ml-4'>© 2022 All rights reserved.</div> */}
        {/* </div> */}
        <div className='p-12 bg-center bg-no-repeat bg-cover'
          style={{
            backgroundImage: `url(${bg})`,
          }}>
            <div className="flex justify-center items-center flex-col pt-10">
            <div className='bg-white p-10 rounded-3xl shadow-2xl'>
              <div className='text-center font-medium text-2xl mb-12'>
                HỆ THỐNG QUẢN LÝ <br></br> TRANG THIẾT BỊ Y TẾ VÀ TÀI SẢN CÔNG</div>
                
                <h1 className='text-center font-medium text-xl' style={{color: "#505AC9"}}>ĐĂNG NHẬP</h1>
                <Divider></Divider>
                <div style={{width: '80%', margin: '0 auto'}}>
                <Form
                name="signin-form"
                className="signin-form"
                form={form}
                initialValues={{ remember: true }}
                onFinish={handleLogin}
                autoComplete="off"
                layout="vertical"
                // layout="horizontal"
                size="large"
              >
                
                <Form.Item
                  label="Tên đăng nhập"
                  name="username"
                  required
                  rules={[
                    { required: true, message: 'Hãy nhập username hoặc email của bạn!' },
                  ]}
                >
                  <Input className='rounded-lg h-9 border-[#A3ABEB] border-2'/>
                </Form.Item>

                <Form.Item
                  label="Mật khẩu"
                  name="password"
                  required
                  rules={[
                    { required: true, message: 'Hãy nhập mật khẩu của bạn!' }
                  ]}
                >
                  <Input.Password className='rounded-lg h-9 border-[#A3ABEB] border-2'/>
                </Form.Item>

                {messageLogin && <Alert
                  message={messageLogin}
                  type="error"
                />}

                <Form.Item>
                  <Text><Link to="/reset-password">Quên mật khẩu</Link></Text>
                </Form.Item>

                <Form.Item>
                  <Button 
                    // type="primary" 
                    icon={<LoginOutlined style={{display: 'inline-flex'}}/>} 
                    loading={isLoading} 
                    htmlType="submit"
                    className='rounded-lg bg-[#505AC9] text-red-50 h-10 w-[-webkit-fill-available]'
                  >
                    Đăng nhập
                  </Button>
                </Form.Item>

                {/* <Form.Item>
                  <Text><Link to="/signup">Đăng ký</Link> nếu bạn chưa có tài khoản </Text>
                </Form.Item> */}
              </Form>
                </div>
              
               
              
            </div>
          </div>
        </div>
      </div>
    </>

  )
}

export default Signin