import React, { useState } from 'react';
import bg from 'assets/bg.jpeg';
import authApi from '../../api/auth.api';
import { useLocation, useNavigate } from 'react-router-dom';
import { ConfirmResetPasswordForm } from '../../types/auth.type';
import { toast } from 'react-toastify';
import FormInputPassword from '../../components/FormInput/Password';
import { GenericResponse } from 'types/commonResponse.type';
import { ChangePasswordForm } from 'types/user.type';
import { Button, Form } from 'antd';

const ChangePasswordFirstLogin: React.FC = () => {
  const navigate = useNavigate();
  const [newPassword, setNewPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [confirmPasswordError, setConfirmPasswordError] = useState<string>('');
  const [isValid, setIsValid] = useState<boolean>(false);
  const location = useLocation();
  const [newPasswordError, setNewPasswordError] = useState<string>();
  const searchParams = new URLSearchParams(location.search);
  const uuid = searchParams.get('uuid');
  const confirmResetPassword = () => {
    const params: ConfirmResetPasswordForm = {
      uuid: uuid || '', newPassword: newPassword, confirmPassword: confirmPassword,
    };
    authApi.confirmResetPassword(params).then(res => {
      toast.success(res.data.message);
      navigate('/signin');
    }).catch(reason => {
      toast.error(reason);
      console.log('reason', reason);
    });
  };
  const validateIfConfirmPasswordIsMatch = (confirmPassword: string) => {
    if (!(newPassword === confirmPassword)) {
      setConfirmPasswordError('Mật khẩu xác nhận sai!');
      setIsValid(false);
      return;
    }
    setNewPasswordError('');
    setConfirmPasswordError('');
    setIsValid(true);
  };
  const validateNewPassword = (value: string) => {
    if (value === '') {
      setIsValid(false);
      setNewPasswordError('Bạn chưa nhập mật khẩu!');
      return;
    }

  };
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const handleChangePassword = async (values: ChangePasswordForm) => {
    setLoading(true);
    try {
      const response = await authApi.changePassword(values);
      const { success } = response.data;
      if (success) {
        toast.success('Đổi mật khẩu thành công! Vui lòng đăng nhập lại!');
        form.resetFields();
        // await authApi.logout();
        navigate('/signin');
      } else {
        toast.error('Đổi mật khẩu thất bại!');
      }
    } catch (error: any) {
      const response: GenericResponse<any> = error?.response?.data;
      const errors: string[] | undefined = response.errors;
      toast.error('Đổi mật khẩu thất bại! ' + errors);
    } finally {
      setLoading(false);
    }
  };
  return (<>
    <div className='grid grid-cols-2 w-screen h-screen'>
      <div className='flex flex-col justify-between bg-amber-50'>
        <div className='flex justify-center items-center flex-col pt-24'>
          <div className='bg-white p-10 rounded-3xl w-96 shadow-2xl'>
            <div className='text-center font-medium text-2xl mb-12'>HỆ THỐNG QUẢN LÝ THIẾT BỊ VÀ VẬT TƯ Y TẾ</div>
            <Form form={form} onFinish={handleChangePassword}>
              <Form.Item  name='oldPassword'>
              <FormInputPassword
              title='Mật khẩu cũ'
              placeHoder='Nhập mật khẩu cũ'
             
              // value={oldPassword}
              // error={newPasswordError}
              type='password'
            />
              </Form.Item>
            <Form.Item name='newPassword'>
            <FormInputPassword
              title='Mật khẩu mới'
              placeHoder='Nhập mật khẩu mới'
              value={newPassword}
              error={newPasswordError}
              onChange={(e: any) => {
                setNewPassword(e.target.value);
                setTimeout(() => {
                  validateNewPassword(e.target.value);
                  validateIfConfirmPasswordIsMatch(confirmPassword);
                }, 100);
              }}
              type='password'
            />
            </Form.Item>
            <Form.Item name='confirmPassword'>
            <FormInputPassword
              title='Xác nhận mật khẩu'
              placeHoder='Xác nhận mật khẩu'
              value={confirmPassword}
              error={confirmPasswordError}
              onChange={(e: any) => {
                setConfirmPassword(e.target.value);
                setTimeout(() => {
                  validateIfConfirmPasswordIsMatch(e.target.value);
                }, 100);
              }}
              type='password'
            />
            </Form.Item>
            
            <div className='flex flex-col gap-4 mt-12'>
              <Button
              htmlType='submit'
                className={`rounded-lg h-10 flex items-center justify-center  ${isValid ? 'bg-blue-400 cursor-pointer' : 'bg-zinc-300 cursor-not-allowed'}`}
              >Xác nhận</Button>
            </div>
            </Form>
            
            
          </div>
        </div>
        <div className='mb-4 ml-4'>© 2022 All rights reserved.</div>
      </div>
      <div className='p-12 bg-center bg-no-repeat bg-cover'
           style={{
             backgroundImage: `url(${bg})`,
           }}>
      </div>
    </div>
  </>);
};

export default ChangePasswordFirstLogin;
