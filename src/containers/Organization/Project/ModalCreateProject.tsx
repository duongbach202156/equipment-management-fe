import { Button, Form, Input, Modal, Select } from 'antd';
import { toast } from 'react-toastify';
import { useContext, useState } from 'react';
import { options } from '../../../utils/globalFunc.util';
import { FilterContext } from '../../../contexts/filter.context';
import TextArea from 'antd/lib/input/TextArea';
import { UpsertProjectForm } from 'types/project.type';
import { projectApi } from 'api/project.api';
import DatePicker from 'components/DatePicker';

export interface ModalCreateProjectProps {
  showCreateProjectModal: boolean;
  hideCreateProjectModal: () => void;
  callback: () => void;
}

export const ModalCreateProject = (props: ModalCreateProjectProps) => {
  const { showCreateProjectModal, hideCreateProjectModal, callback } = props;
  const [form] = Form.useForm<UpsertProjectForm>();
  const [loading, setLoading] = useState<boolean>(false);
  const { services } = useContext(FilterContext);
  const createProject = (values: UpsertProjectForm) => {
    setLoading(true);
    projectApi.createProject(values).then(() => {
      toast.success('Tạo dự án thành công');
    }).finally(() => {
      setLoading(false);
      hideCreateProjectModal();
      form.resetFields();
      callback();
    });
  };

  return (
  <Modal
  forceRender
    title='Tạo mới dự án'
    open={showCreateProjectModal}
    onCancel={() => {
      hideCreateProjectModal();
    }}
    footer={null}
    width={1000}
  >
    <Form form={form} layout='vertical' size='large' onFinish={createProject}>
      <div>
        <Form.Item
          label='Tên dự án' name='name' required={true} rules={[
          { required: true, message: 'Vui lòng nhập tên dự án' },
        ]}>
          <Input className='input' />
        </Form.Item>
        <Form.Item
          label='Nguồn vốn' name='fundingSource' required={true} rules={[
          { required: true, message: 'Vui lòng nhập nguồn vốn' },
        ]}>
          <Input className='input' />
        </Form.Item>
        <div className='grid grid-cols-2 gap-10'>
        <Form.Item label='Ngày bắt đầu' name='startDate' className='mb-5'>
            <DatePicker className='input w-[-webkit-fill-available]' placeholder='Chọn ngày' />
          </Form.Item>
          <Form.Item label='Ngày kết thúc' name='endDate' className='mb-5'>
            <DatePicker className='input w-[-webkit-fill-available]' placeholder='Chọn ngày' />
          </Form.Item>
        </div>
       
      </div>
      <div className='flex flex-row justify-end gap-4'>
        <Form.Item>
          <Button htmlType='submit' className='button' loading={loading}>Xác nhận</Button>
        </Form.Item>
        <Form.Item>
          <Button onClick={() => hideCreateProjectModal()} className='button_reject'>Đóng</Button>
        </Form.Item>
      </div>
    </Form>
  </Modal>);
};
