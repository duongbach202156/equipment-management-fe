import { Button, Form,  Input, Modal, Select } from 'antd';
import { toast } from 'react-toastify';
import { useContext, useEffect, useState } from 'react';
import TextArea from 'antd/lib/input/TextArea';
import { options } from '../../../utils/globalFunc.util';
import { FilterContext } from '../../../contexts/filter.context';
import { ProjectDto, UpsertProjectForm } from 'types/project.type';
import { projectApi } from 'api/project.api';
import { mapProjectDtoToUpsertProjectForm } from 'utils/mapper.util';
import DatePicker from 'components/DatePicker';

export interface ModalUpdateProjectProps {
  showUpdateProjectModal: boolean;
  hideUpdateProjectModal: () => void;
  callback: () => void;
  updateProjectModalData?: ProjectDto;
}

export const ModalUpdateProject = (props: ModalUpdateProjectProps) => {
  const { showUpdateProjectModal, hideUpdateProjectModal, callback, updateProjectModalData } = props;
  const [form] = Form.useForm<UpsertProjectForm>();
  const [loading, setLoading] = useState<boolean>(false);
  const updateProject = (values: UpsertProjectForm) => {
    setLoading(true);
    projectApi.updateProject(updateProjectModalData?.id as number, values).then(() => {
      toast.success('Cập nhật dự án thành công');
    }).finally(() => { 
      setLoading(false);
      hideUpdateProjectModal();
      form.resetFields();
      callback();
    });
  };
  useEffect(() => {
    form.resetFields();
    form.setFieldsValue(mapProjectDtoToUpsertProjectForm(updateProjectModalData as ProjectDto));
  }, [updateProjectModalData]);
  return (<Modal
  forceRender
    width={1000}
    title='Cập nhật dự án'
    open={showUpdateProjectModal}
    onCancel={() => {
      hideUpdateProjectModal();
    }}
    footer={null}
  >
    <Form form={form} layout='vertical' size='large' onFinish={updateProject}>
      <div>
        <Form.Item
          label='Tên dự án' name='name' required={true} rules={[
          { required: true, message: 'Vui lòng nhập tên dự án' },
        ]}>
          <Input className='input' />
        </Form.Item>
        <Form.Item
          label='Nguồn vốn' name='fundingSource' required={true} rules={[
            { required: true, message: 'Vui lòng nhập nguồn vốn' },
          ]}>
          <Input className='input' />
        </Form.Item>
          </div>
        <div className='grid grid-cols-2 gap-10'>
        <Form.Item label='Ngày bắt đầu' name='startDate' className='mb-5'>
            <DatePicker className='input w-[-webkit-fill-available]' placeholder='Chọn ngày' />
          </Form.Item>
          <Form.Item label='Ngày kết thúc' name='endDate' className='mb-5'>
            <DatePicker className='input w-[-webkit-fill-available]' placeholder='Chọn ngày' />
          </Form.Item>
        </div>
      <div className='flex flex-row justify-end gap-4'>
        <Form.Item>
          <Button htmlType='submit' className='button' loading={loading}>Xác nhận</Button>
        </Form.Item>
        <Form.Item>
          <Button onClick={() => hideUpdateProjectModal()} className='button'>Đóng</Button>
        </Form.Item>
      </div>
    </Form>
  </Modal>);
};
