import { useContext, useEffect, useState } from 'react';
import { DeleteTwoTone, EditTwoTone, FilterFilled, PlusCircleFilled, SelectOutlined } from '@ant-design/icons';
import { Button, Checkbox, Divider, Input, Menu, Popconfirm, Select, Table, Tooltip } from 'antd';
import useDebounce from 'hooks/useDebounce';
import { useLocation, useNavigate } from 'react-router-dom';
import useQuery from 'hooks/useQuery';
import { createUrlWithQueryString, getDateForRendering, hasAuthority, onChangeCheckbox, options } from 'utils/globalFunc.util';
import { PaginationProps } from 'antd/lib/pagination/Pagination';
import moment from 'moment/moment';
import { TableFooter } from 'components/TableFooter';
import { toast } from 'react-toastify';
import { Pageable } from '../../../types/commonResponse.type';
import { PageableRequest } from '../../../types/commonRequest.type';
import { Authority } from '../../../constants/authority';
import { ServiceDto } from '../../../types/service.type';
import { FilterContext } from '../../../contexts/filter.context';
import { GetProjectsQueryParam, ProjectDto } from 'types/project.type';
import { projectApi } from 'api/project.api';
import { DATE_TIME_FORMAT, ISO_DATE_FORMAT } from 'constants/dateFormat.constants';
import { ModalCreateProject } from './ModalCreateProject';
import { ModalUpdateProject } from './ModalUpdateProject';

const Project = () => {

  const navigate = useNavigate();
  const location = useLocation();
  const query = useQuery();
  const [keyword, setKeyword] = useState<string>(query.keyword);
  const [projects, setProjects] = useState<ProjectDto[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [isShowCustomTable, setIsShowCustomTable] = useState<boolean>(false);
  const [pageable, setPageable] = useState<Pageable>({ number: 0, size: 20 });
  const [componentShouldUpdate, setComponentShouldUpdate] = useState<boolean>(false);
  const [getProjectQueryParam, setGetProjectsQueryParam] = useState<GetProjectsQueryParam>({});
  const keywordSearch = useDebounce(getProjectQueryParam.keyword as string, 500);
  const [pageableRequest, setPageableRequest] = useState<PageableRequest>({ size: query.size || 20, page: query.page || 0 });
  const [queryString, setQueryString] = useState<string>(location.search);
  const [showCreateProjectModal, setShowCreateProjectModal] = useState<boolean>(false);
  const [showUpdateProjectModal, setShowUpdateProjectModal] = useState<boolean>(false);
  const [updateProjectModalData, setUpdateProjectModalData] = useState<ProjectDto>({});
  // const { services } = useContext(FilterContext);
  const columns: any = [
    {
      title: 'Tên dự án', dataIndex: 'name', key: 'name', show: true, widthExcel: 30,
    }, {
      title: 'Nguồn Vốn', dataIndex: 'fundingSource', key: 'fundingSource', show: true, widthExcel: 30,
    }, {
      title: 'Ngày bắt đầu', 
      show: true, 
      widthExcel: 30,
      render: (item: ProjectDto) => (<div>{getDateForRendering(item.startDate as string, ISO_DATE_FORMAT)}</div>),
    }, {
      title: 'Ngày kết thúc', 
      show: true, 
      widthExcel: 30,
      render: (item: ProjectDto) => (<div>{getDateForRendering(item.endDate as string, ISO_DATE_FORMAT)}</div>),

    }, {
      title: 'Tác vụ', key: 'action', show: true, render: (item: ProjectDto) => (<Menu className='flex flex-row items-center'>
        {hasAuthority(Authority.PROJECT_UPDATE) && <Menu.Item>
          <Tooltip title='Cập nhật thông tin dự án'>
            <EditTwoTone twoToneColor="#52c41a"
              onClick={() => {
                setUpdateProjectModalData(item);
                setShowUpdateProjectModal(true);
              }} />
          </Tooltip>
        </Menu.Item>}
        {hasAuthority(Authority.PROJECT_DELETE) && <Menu.Item>
          <Tooltip title='Xóa'>
            <Popconfirm
              title='Bạn muốn xóa nhà dự án này?'
              onConfirm={() => handleDeleteProject(item.id as number)}
              okText='Xóa'
              cancelText='Hủy'
            >
              <DeleteTwoTone twoToneColor="red" />
            </Popconfirm>
          </Tooltip>
        </Menu.Item>}
      </Menu>),
    },
  ];
  const [columnTable, setColumnTable] = useState<any>(columns);
  const onChangeQueryParams = (key: string, value: string | moment.Moment[] | undefined | number | PageableRequest) => {
    let pagebaleClone: PageableRequest;
    pagebaleClone = { ...pageableRequest };
    let getProjectQueryParamClone: GetProjectsQueryParam = getProjectQueryParam;
    if (key === '') { //if key is empty, it means that the component should be updated
      setComponentShouldUpdate(true);
    }
    if (key === 'keyword') {
      getProjectQueryParamClone = { ...getProjectQueryParam, keyword: value as string };
    }
    if (key === 'pageable') {
      pagebaleClone = { ...pageableRequest, page: (value as PageableRequest).page, size: (value as PageableRequest).size };
    }
    setGetProjectsQueryParam(getProjectQueryParamClone);
    setPageableRequest(pagebaleClone);
    const url = createUrlWithQueryString(location.pathname, getProjectQueryParamClone, pagebaleClone);
    setQueryString(url);
    // navigate(url);
  };

  const pagination: PaginationProps = {
    current: pageable.number as number + 1,
    total: pageable.totalElements,
    pageSize: pageable.size,
    showTotal: (total: number) => `Tổng cộng: ${total} dự án`,
    onChange: (page, pageSize) => {
      onChangeQueryParams('pageable', { page: page - 1, size: pageSize });
    },
    showQuickJumper: true,
  };
  const handleDeleteProject = (id: number) => {
    projectApi.deleteProjects(id).then(() => {
      toast.success('Dự án được xoá thành công!');
      onChangeQueryParams('', '');
    });
  };
  const searchListOfProject = (queryParams: GetProjectsQueryParam, pageableRequest: PageableRequest) => {
    projectApi.getProjects(queryParams, pageableRequest).then((res) => {
      if (res.data.success) {
        setProjects(res.data.data.content);
        setPageable(res.data.data.page);
        setLoading(false);

      }
    });
  };
  useEffect(() => {
    setLoading(true);
    setComponentShouldUpdate(false);
    setGetProjectsQueryParam({
      keyword: query.keyword || '',
    });
    setPageableRequest({
      page: Number(query.page) || 0, size: Number(query.size) || 20,
    });
    searchListOfProject(getProjectQueryParam, pageableRequest);
    // setLoading(false);
  }, [componentShouldUpdate, queryString, keywordSearch]);
  return (<div>
    <div className='flex-between-center'>
      <div className='title'> DANH SÁCH DỰ ÁN</div>
      <div className='flex flex-row gap-6'>
        <Button
          className='flex-center text-slate-900 gap-2 rounded-3xl border-[#5B69E6] border-2'
          onClick={() => setShowCreateProjectModal(true)}
        >
          <PlusCircleFilled />
          <div className='font-medium text-md text-[#5B69E6]'>Thêm mới</div>
        </Button>
      </div>
    </div>
    <Divider />
    <div className='flex justify-between flex-col'>
      <div
        className='flex flex-row gap-4 items-center mb-4'
        onClick={() => setIsShowCustomTable(!isShowCustomTable)}
      >
        <SelectOutlined />
        <div className='font-medium text-center cursor-pointer text-base'>Tùy chọn trường hiển thị</div>
      </div>
      {isShowCustomTable && <div className='flex flex-row gap-4'>
        {columnTable.length > 0 && columnTable.map((item: any) => (<div>
          <Checkbox
            defaultChecked={item?.show}
            onChange={(e: any) => onChangeCheckbox(item, e, columnTable, setColumnTable)}
          />
          <div>{item?.title}</div>
        </div>))}
      </div>}
      <div className='flex-between-center gap-4 p-4'>
        {/* <Select
          style={{ width: '100%', maxWidth: '300px', minWidth: '200px' }}
          showSearch
          placeholder=' Chọn loại dịch vụ'
          optionFilterProp='children'
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={options(services)}
          onChange={(e) => {
            onChangeQueryParams('serviceId', e);
          }}
        /> */}
        <Input
          placeholder='Tìm kiếm nhà dự án'
          allowClear
          value={keyword}
          onChange={(e) => {
            setKeyword(e.target.value);
            onChangeQueryParams('keyword', e.target.value);
          }}
          className='input'
        />
        <div>
          <FilterFilled />
        </div>
      </div>
    </div>
    <Table
      columns={columnTable.filter((item: any) => item.show)}
      dataSource={projects}
      className='mt-6 shadow-md table-responsive'
      footer={() => (<>
        <TableFooter paginationProps={pagination} />
      </>)}
      pagination={false}
      loading={loading}
    />

    <ModalCreateProject
      showCreateProjectModal={showCreateProjectModal}
      hideCreateProjectModal={() => setShowCreateProjectModal(false)}
      callback={() => {
        onChangeQueryParams('', '');
      }} />
    <ModalUpdateProject
      showUpdateProjectModal={showUpdateProjectModal}
      hideUpdateProjectModal={() => setShowUpdateProjectModal(false)}
      callback={() => {
        onChangeQueryParams('', '');
      }}
      updateProjectModalData={updateProjectModalData}
    />
  </div>);
};

export default Project;
