import {Column, Pie} from '@ant-design/plots';
import { Avatar, Card, Divider, Tabs } from 'antd';
import equipmentApi from 'api/medicalEquipment.api';
import inUse from 'assets/active.png';
import repairing from 'assets/repairing.png';
import inactive from 'assets/inactive.png';
import liquidated from 'assets/liquidated.png';
import news from 'assets/news.png';
import broken from 'assets/broken.png';
import maintenance from 'assets/maintenance.png';
import inspection from 'assets/inspection.png';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { CountEquipmentByDepartment, CountEquipmentByDepartmentAndStatus, CountEquipmentByDepartmentAndStatusTable, CountEquipmentByRiskLevel, CountEquipmentByStatus, StatisticDashboard } from 'types/statistics.type';
import './index.css';
import { EquipmentStatus } from '../../types/equipment.type';
import { useTranslation } from 'react-i18next';
import { ClusterOutlined, DesktopOutlined } from '@ant-design/icons';
import OfficeDashboard from './officeDashboard';
import { getCurrentUser, hasAuthority } from 'utils/globalFunc.util';
import { Authority } from 'constants/authority';

const { Meta } = Card;

const Dashboard = () => {
  const navigate = useNavigate();
  const [countEquipmentByStatuses, setCountEquipmentByStatuses] = useState<CountEquipmentByStatus[]>([]);
  const [countEquipmentByDepartments, setCountEquipmentByDepartments] = useState<CountEquipmentByDepartment[]>([]);
  const [countEquipmentByRiskLevels, setCountEquipmentByRiskLevels] = useState<CountEquipmentByRiskLevel[]>([]);
  const [countBrokenEquipmentByDepartments, setCountBrokenEquipmentByDepartments] = useState<CountEquipmentByDepartment[]>([]);
  const [sumBrokenEquipments, setSumBrokenEquipments] = useState<number>(0);
  const [countRepairingEquipmentByDepartments, setCountRepairingEquipmentByDepartments] = useState<CountEquipmentByDepartment[]>([]);
  const [sumRepairingEquipments, setSumRepairingEquipments] = useState<number>(0);
  const [countEquipmentByDepartmentAndStatus, setCountEquipmentByDepartmentAndStatus] = useState<CountEquipmentByDepartment[]>([]);
  const { t } = useTranslation();

  /////
  // const currentDepartment = query?.departmentId;
  const [department, setDepartment] = useState<any>(null);

  /////



  const count = (departmentId : number) => {
    equipmentApi.statisticMedicalDashboard().then((res) => {
      if (res.data.success) {
        const data: StatisticDashboard = res.data.data;
        setCountEquipmentByDepartments(data.countByDepartment);
        setCountEquipmentByRiskLevels(data.countByRiskLevels);
        let countEquipmentByStatuses = data.countByEquipmentStatuses
        .map((item: CountEquipmentByStatus) => {
          switch (item.status) {
            case EquipmentStatus.NEW:
              item.image = news;
              break;
            case EquipmentStatus.IN_USE:
              item.image = inUse;
              break;
            case EquipmentStatus.BROKEN:
              item.image = broken;
              break;
            case EquipmentStatus.REPAIRING:
              item.image = repairing;
              break;
            case EquipmentStatus.INACTIVE:
              item.image = inactive;
              break;
            case EquipmentStatus.LIQUIDATED:
              item.image = liquidated;
              break;
            case EquipmentStatus.UNDER_MAINTENANCE:
              item.image = maintenance;
              break;
            case EquipmentStatus.UNDER_INSPECTION:
              item.image = inspection;
              break;
          }
          return item;
        });
        setCountEquipmentByStatuses(countEquipmentByStatuses);
        setCountBrokenEquipmentByDepartments(data.countBrokenByDepartment);
        setCountRepairingEquipmentByDepartments(data.countRepairingByDepartment);
        let sumBroken = data?.countBrokenByDepartment?.reduce(function(acc: number, obj: CountEquipmentByDepartment) {
          return acc + obj.count;
        }, 0);
        setSumBrokenEquipments(sumBroken);
        let sumRepair = data?.countRepairingByDepartment?.reduce(function(acc: number, obj: CountEquipmentByDepartment) {
          return acc + obj.count;
        }, 0);
        setSumRepairingEquipments(sumRepair);

        


        // const flag : CountEquipmentByDepartment[] = data.countEquipmentByDepartmentAndStatus.sort((a,b) => a.departmentId - b.departmentId);
        // setCountEquipmentByDepartmentAndStatus(data.countEquipmentByDepartmentAndStatus);
        // if (departmentId !== undefined && departmentId !== null) {
        //   const count : CountEquipmentByDepartment[] = flag.filter(item => item.departmentId === departmentId);
        //   setCountEquipmentByDepartmentAndStatus(count);
        // }

        }
      // }
     
    
    });
  };


  useEffect(() => {
    count(department);
    // console.log("hello", countEquipmentByDepartmentAndStatus);
  }, []);

  const dataCountEquipmentByDepartment = countEquipmentByDepartments.length > 0 ? countEquipmentByDepartments : [];
  const configCountEquipmentByDepartment: any = {
    data: dataCountEquipmentByDepartment, xField: 'departmentName', yField: 'count', label: {
      position: 'middle', style: {
        fill: '#FFFFFF', opacity: 0.6,
      },
    }, xAxis: {
      label: {
        autoHide: false, autoRotate: true,
      },
    }, meta: {
      departmentName: {
        alias: 'name',
      }, count: {
        alias: 'Số lượng thiết bị',
      },
    },
  };

  const dataCountEquipmentByRiskLevel = countEquipmentByRiskLevels?.length > 0 ? countEquipmentByRiskLevels : [];
  const configCountEquipmentByRiskLevel: any = {
    appendPadding: 10, data: dataCountEquipmentByRiskLevel, angleField: 'count', colorField: 'riskLevel', radius: 0.9, label: {
      type: 'inner', offset: '-30%', style: {
        fontSize: 14, textAlign: 'center',
      },
    }, interactions: [
      {
        type: 'element-active',
      },
    ],
  };

  const dataCountBrokenEquipmentByDepartments = countBrokenEquipmentByDepartments?.length > 0 ? countBrokenEquipmentByDepartments : [];
  const configCountBrokenEquipmentByDepartments: any = {
    appendPadding: 10, data: dataCountBrokenEquipmentByDepartments, angleField: 'count', colorField: 'departmentName', radius: 0.9, label: {
      type: 'inner', offset: '-30%', style: {
        fontSize: 14, textAlign: 'center',
      },
    }, interactions: [
      {
        type: 'element-active',
      },
    ],
  };

  const dataCountRepairEquipmentByDepartments = countRepairingEquipmentByDepartments?.length > 0 ? countRepairingEquipmentByDepartments : [];
  const configCountRepairEquipmentByDepartments: any = {
    appendPadding: 10, data: dataCountRepairEquipmentByDepartments, angleField: 'count', colorField: 'departmentName', radius: 0.9, label: {
      type: 'inner', offset: '-30%', style: {
        fontSize: 14, textAlign: 'center',
      },
    }, interactions: [
      {
        type: 'element-active',
      },
    ],
  };

  /////
 



  return (<>
    <div className='title text-center'>DASHBOARD</div>
    <Divider />
    <Tabs size='large'>
    {hasAuthority(Authority.MEDICAL_DASHBOARD_READ) &&  <Tabs.TabPane tab={
        <span><ClusterOutlined style={{display: 'inline-flex'}} /> Thiết bị y tế</span>
      } key="1">
        <div>
      <div className='mb-8'>
        <div className='title mb-6'>Thống kê thiết bị theo trạng thái</div>
        <div className='grid grid-cols-4 gap-5'>
          {countEquipmentByStatuses?.length > 0 && countEquipmentByStatuses.map((item: CountEquipmentByStatus) => (
          <Card
            key={item.status}

            // title={t(item.status)}
            hoverable
            // style={{ width: 200 }}
            // cover={<img alt='status' src={item.image}/>}
            // className='count'
            // className='flex'
            onClick={() => navigate(`medical-equipments?status=${item.status}`)}
          >

            <Meta
              avatar={<img style={{width: 70}} alt='status' src={item.image}/>}
              title={t(item.status)}
              style={{fontSize: 18}}
              description={`${item.count} thiết bị`}
            />
          </Card>))}
        </div>
      </div>
      <div className='mb-8'>
        <div className='title mb-6'>Thống kê thiết bị theo khoa phòng</div>
        <Card title='Biểu đồ thống kê' hoverable>
          <Column
            {...configCountEquipmentByDepartment}
            onReady={(plot) => {
              plot.on('plot:click', (evt: any) => {
                const data = evt?.data?.data as CountEquipmentByDepartment;
                navigate(`/medical-equipments?departmentId=${data.departmentId}`);
              });
            }}
          />
        </Card>
      </div>
      <div className='mb-8'>
        <div className='title mb-6'>
          Thống kê thiết bị đang báo hỏng ({sumBrokenEquipments} thiết bị)
        </div>
        <Pie
          {...configCountBrokenEquipmentByDepartments}
          onReady={(plot) => {
            plot.on('plot:click', (evt: any) => {
              const data : CountEquipmentByDepartment = evt?.data?.data;
              navigate(`/medical-equipments?status=${EquipmentStatus.BROKEN}&departmentId=${data.departmentId}`);
            });
          }}
        />
      </div>
      <div className='mb-8'>
        <div className='title mb-6'>
          Thống kê thiết bị đang sửa chữa ({sumRepairingEquipments} thiết bị)
        </div>
        <Pie
          {...configCountRepairEquipmentByDepartments}
          onReady={(plot) => {
            plot.on('plot:click', (evt: any) => {
              const data : CountEquipmentByDepartment= evt?.data?.data;
              navigate(`/medical-equipments?status=${EquipmentStatus.REPAIRING}&departmentId=${data.departmentId}`);
            });
          }}
        />
      </div>
      <div className='mb-8'>
        <div className='title mb-6'>Thống kê thiết bị theo mức độ rủi ro</div>
        
        <Pie
          {...configCountEquipmentByRiskLevel}
          onReady={(plot) => {
            plot.on('plot:click', (evt: any) => {
              const data = evt?.data?.data as CountEquipmentByRiskLevel;
              navigate(`/medical-equipments?riskLevel=${data.riskLevel}`);
            });
          }}
        />
      </div>


      
    </div>
        </Tabs.TabPane>}
      {hasAuthority(Authority.OFFICE_DASHBOARD_READ) && <Tabs.TabPane tab={
        <span><DesktopOutlined style={{display: 'inline-flex'}} />Tài sản công</span>
      } key="tab2">
        <OfficeDashboard></OfficeDashboard>
      </Tabs.TabPane>}
        
    </Tabs>
    
    
  </>);
};

export default Dashboard;
