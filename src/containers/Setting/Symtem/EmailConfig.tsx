import { ClusterOutlined, DesktopOutlined, InfoCircleFilled } from '@ant-design/icons';
import { Button, Checkbox, Divider, Modal, Table, Tabs } from 'antd';
import React, { useContext, useEffect, useState } from 'react';
import { notificationConfigApi } from '../../../api/notificationConfig.api';
import { NotificationType, UpdateNotificationConfigForm } from '../../../types/notificationConfig.type';
import { RoleFullInfoDto } from '../../../types/role.type';
import { toast } from 'react-toastify';
import { FilterContext } from '../../../contexts/filter.context';

const EmailConfig = () => {

  const [showEmailConfigModal, setShowEmailConfigModal] = useState<boolean>(false);
  const { roles } = useContext(FilterContext);
  const [loading, setLoading] = useState<boolean>(false);
  const [checkedButton, setCheckedButton] = useState<UpdateNotificationConfigForm[]>([]);
  const [shouldUpdate, setShouldUpdate] = useState<boolean>(false);
  const onCheckButton = (roleId: number, notificationType: NotificationType) => {
    if (checkedButton.filter((x) => x.roleId === roleId && x.notificationType === notificationType).length > 0) {
      setCheckedButton(checkedButton.filter((x) => x.roleId !== roleId || x.notificationType !== notificationType));
      return;
    }
    setCheckedButton([...checkedButton, { roleId, notificationType }]);
  };
  const columnCreateNew: any = [
    {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.MEDICAL_CREATE);}}
        checked={checkedButton.filter((x) => x.roleId === item.id && x.notificationType === NotificationType.MEDICAL_CREATE).length > 0}
      />),
    },
  ];
  const columnHandovers: any = [
    {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.MEDICAL_HANDOVER);}}
        checked={checkedButton.filter((x) => x.roleId === item.id && x.notificationType === NotificationType.MEDICAL_HANDOVER).length > 0}
      />),
    },
  ];
  const columnTransfers: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.MEDICAL_TRANSFER);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.MEDICAL_TRANSFER)}
      />),
    },
  ];
  const columnReportBrokens: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.MEDICAL_REPORT_BROKEN);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.MEDICAL_REPORT_BROKEN)}
      />),
    },
  ];
  const columnRepairs: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.MEDICAL_REPAIR);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.MEDICAL_REPAIR)}
      />),
    },
  ];
  const columnMaintenances: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.MEDICAL_MAINTENANCE);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.MEDICAL_MAINTENANCE)}
      />),
    },
  ];
  const columnInspections: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.MEDICAL_INSPECTION);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.MEDICAL_INSPECTION)}
      />),
    },
  ];
  const columnLiquidations: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.MEDICAL_LIQUIDATION);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.MEDICAL_LIQUIDATION)}
      />),
    },
  ];
  /// office equipments
  const columnCreateNewOffice: any = [
    {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.OFFICE_CREATE);}}
        checked={checkedButton.filter((x) => x.roleId === item.id && x.notificationType === NotificationType.OFFICE_CREATE).length > 0}
      />),
    },
  ];
  const columnHandoversOffice: any = [
    {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.OFFICE_HANDOVER);}}
        checked={checkedButton.filter((x) => x.roleId === item.id && x.notificationType === NotificationType.OFFICE_HANDOVER).length > 0}
      />),
    },
  ];
  const columnTransfersOffice: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.OFFICE_TRANSFER);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.OFFICE_TRANSFER)}
      />),
    },
  ];
  const columnReportBrokensOffice: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.OFFICE_REPORT_BROKEN);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.OFFICE_REPORT_BROKEN)}
      />),
    },
  ];
  const columnRepairsOffice: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.OFFICE_REPAIR);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.OFFICE_REPAIR)}
      />),
    },
  ];
  const columnMaintenancesOffice: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.OFFICE_MAINTENANCE);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.OFFICE_MAINTENANCE)}
      />),
    },
  ];
  const columnLiquidationsOffice: any = [
   {
      title: 'Vai trò', render: (item: RoleFullInfoDto) => (<>{item.name}</>),
    }, {
      title: ' Mô tả', render: (item: RoleFullInfoDto) => (<>{item.description}</>),
    }, {
      title: 'Trạng thái', render: (item: RoleFullInfoDto) => (<Checkbox
        onClick={() => {onCheckButton(item.id, NotificationType.OFFICE_LIQUIDATION);}}
        checked={checkedButton.some((x) => x.roleId === item.id && x.notificationType === NotificationType.OFFICE_LIQUIDATION)}
      />),
    },
  ];
  useEffect(() => {
    setLoading(true);
    setShouldUpdate(false);
    notificationConfigApi.getNotificationConfigs().then((res) => {
      let _checkedButton: UpdateNotificationConfigForm[] = [];
      res.data.data.forEach((item) => {
        _checkedButton.push({
          notificationType: item.notificationType, roleId: item.role.id,
        });
      });
      setCheckedButton(_checkedButton);
    }).finally(() => setLoading(false));
  }, [shouldUpdate]);

  const updateNotificationConfig = () => {
    notificationConfigApi.updateNotificationConfig(checkedButton).then((res) => {
      toast.success('Cập nhật cấu hình thành công');
      setShouldUpdate(true);
    });
  };

  return (<div>
    <div className='title'>CẤU HÌNH NHẬN THÔNG BÁO QUA EMAIL</div>
    <Divider />
    <div
      className='text-red-600 flex items-center cursor-pointer text-lg gap-2 mb-6'
      onClick={() => setShowEmailConfigModal(true)}
    >
      <InfoCircleFilled />
      <div>Chi tiết cấu hình</div>
    </div>
    <Tabs size='large'>
    <Tabs.TabPane tab={
        <span><ClusterOutlined style={{display: 'inline-flex'}} /> Thiết bị y tế</span>
      } key="1">
            <div className='grid grid-cols-2 gap-20 mb-20'>
    <div>
        <div className='font-medium text-base'>Tác vụ nhập mới thiết bị y tế</div>
        <Table
          rowKey="id"
          columns={columnCreateNew}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
      <div>
        <div className='font-medium text-base'>Tác vụ bàn giao thiết bị y tế</div>
        <Table
          rowKey="id"
          columns={columnHandovers}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
     
    </div>
    <div className='grid grid-cols-2 gap-20 mb-20'>
      <div>
        <div className='font-medium text-base'>Tác vụ báo hỏng thiết bị y tế</div>
        <Table
          rowKey="id"
          columns={columnReportBrokens}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
      <div>
        <div className='font-medium text-base'> Tác vụ sửa chữa thiết bị y tế</div>
        <Table
          rowKey="id"
          columns={columnRepairs}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
    </div>
    <div className='grid grid-cols-2 gap-20 mb-10'>
      <div>
        <div className='font-medium text-base'>Tác vụ thanh lý thiết bị y tế</div>
        <Table
          rowKey="id"
          columns={columnLiquidations}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
      <div>
        <div className='font-medium text-base'>Tác vụ kiểm định thiết bị y tế</div>
        <Table
          rowKey="id"
          columns={columnInspections}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
    </div>
    <div className='grid grid-cols-2 gap-20 mb-10'>
    <div>
        <div className='font-medium text-base'>Tác vụ điều chuyển thiết bị y tế</div>
        <Table
          rowKey="id"
          columns={columnTransfers}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
      <div>
        <div className='font-medium text-base'>Tác vụ bảo dưỡng thiết bị y tế</div>
        <Table
          rowKey="id"
          columns={columnMaintenances}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
    </div>
        </Tabs.TabPane>
        <Tabs.TabPane tab={
        <span><DesktopOutlined style={{display: 'inline-flex'}} />Tài sản công</span>
      } key="tab2">
                <div className='grid grid-cols-2 gap-20 mb-20'>
    <div>
        <div className='font-medium text-base'>Tác vụ nhập mới tài sản công</div>
        <Table
          rowKey="id"
          columns={columnCreateNewOffice}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
      <div>
        <div className='font-medium text-base'>Tác vụ bàn giao tài sản công</div>
        <Table
          rowKey="id"
          columns={columnHandoversOffice}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
     
    </div>
    <div className='grid grid-cols-2 gap-20 mb-20'>
      <div>
        <div className='font-medium text-base'>Tác vụ báo hỏng tài sản công</div>
        <Table
          rowKey="id"
          columns={columnReportBrokensOffice}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
      <div>
        <div className='font-medium text-base'> Tác vụ sửa chữa tài sản công</div>
        <Table
          rowKey="id"
          columns={columnRepairsOffice}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
    </div>
    
    <div className='grid grid-cols-2 gap-20 mb-10'>
    <div>
        <div className='font-medium text-base'>Tác vụ điều chuyển tài sản công</div>
        <Table
          rowKey="id"
          columns={columnTransfersOffice}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
      <div>
        <div className='font-medium text-base'>Tác vụ bảo dưỡng tài sản công</div>
        <Table
          rowKey="id"
          columns={columnMaintenancesOffice}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
    </div>
    <div className='grid grid-cols-2 gap-20 mb-10'>
      <div>
        <div className='font-medium text-base'>Tác vụ thanh lý tài sản công</div>
        <Table
          rowKey="id"
          columns={columnLiquidationsOffice}
          dataSource={roles}
          className='mt-6 shadow-md'
          pagination={false}
        />
      </div>
    </div>
      </Tabs.TabPane>
    </Tabs>

    <div className='flex justify-center'>
      <Button className='button mt-8' onClick={updateNotificationConfig}>Cập nhật</Button>
    </div>
    <Modal
      title='Cấu hình gửi tác vụ gửi mail'
      open={showEmailConfigModal}
      onCancel={() => setShowEmailConfigModal(false)}
      footer={null}
    >
      <div className='text-lg'>
        - Bạn vui lòng tích vào check box những đối tượng người dùng mà bạn muốn gửi mail
      </div>
      <div className='text-lg'>
        - Đối với mỗi tác vụ như bàn giao, điều chuyển, ... hệ thống sẽ gửi mail thông báo tới những đối tượng người dùng trong danh sách mà bạn đã cấu hình
      </div>
    </Modal>
  </div>);
};

export default EmailConfig;