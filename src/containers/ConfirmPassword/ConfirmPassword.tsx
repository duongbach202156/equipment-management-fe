import React, { useEffect, useState } from 'react';
import bg from 'assets/bg.jpg';
import authApi from '../../api/auth.api';
import { useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import { ConfirmResetPasswordForm } from '../../types/auth.type';
import { toast } from 'react-toastify';
import FormInputPassword from '../../components/FormInput/Password';
import { Button, Result, Spin } from 'antd';
import { error } from 'console';
import useQuery from 'hooks/useQuery';
import { set } from 'lodash';

const ConfirmPassword: React.FC = () => {
  const navigate = useNavigate();
  const [newPassword, setNewPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [confirmPasswordError, setConfirmPasswordError] = useState<string>('');
  const [isValid, setIsValid] = useState<boolean>(false);
  const location = useLocation();
  const [newPasswordError, setNewPasswordError] = useState<string>();
  const [searchParams, setSearchParams] = useSearchParams();
  const token = searchParams.get('token');
  const [loading, setLoading] = useState(true);
  const [success, setSuccess] = useState(false);
  useEffect(() => {
    // console.log(token);
    setLoading(true);
    authApi.confirmActiveAccount(token).then(res => {
      setLoading(false);
      setSuccess(true);
    }).
    catch(error => {
      setSuccess(false);
      setLoading(false);
      // toast.error(error);
    })
  },[])

  return (<>
    
    {loading ? (
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '100vh' }}>
      <Spin size="large" />
      </div>
        
      ) : success ? (
        <Result
          status="success"
          title="Kích hoạt tài khoản thành công!"
          extra={[
            <Button type="primary" key="console" onClick={() => {
              navigate('/signin')
            }}>
              Về trang đăng nhập
            </Button>,
          ]}
        />
      ) : (
        <Result
          status="error"
          title="Kích hoạt tài khoản thất bại"
        />
      )}


  </>);
};

export default ConfirmPassword;
