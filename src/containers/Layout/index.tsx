import React, { useContext, useEffect, useState } from 'react';
import type { MenuProps } from 'antd';
import { Avatar, Badge, Card, ConfigProvider, Divider, Dropdown, Layout, Menu, Row } from 'antd';
import { Link, NavLink, useLocation, useNavigate } from 'react-router-dom';
import {
  BarChartOutlined,
  BarsOutlined,
  BellFilled,
  ClusterOutlined,
  DesktopOutlined,
  DownOutlined,
  PartitionOutlined,
  SettingOutlined,
  SisternodeOutlined,
  TeamOutlined,
  UnorderedListOutlined,
  UsergroupAddOutlined,
  UserOutlined,
} from '@ant-design/icons';
import logo from 'assets/logo.png';
import { useDispatch } from 'react-redux';
import { authActions } from 'store/slices/auth.slice';
import { NotificationContext } from 'contexts/notification.context';
import ModalChangePassword from 'components/ModalChangePassword';
import { CURRENT_USER } from 'constants/auth.constant';
import { Authority } from 'constants/authority';
import moment from 'moment';
import { UserDetailDto } from '../../types/user.type';
import { createImageSourceFromBase64, hasAuthority } from '../../utils/globalFunc.util';
import { Notification } from '../../types/notification.type';
import userApi from 'api/user.api';
import { GenericResponse } from 'types/commonResponse.type';
import { fileApi } from 'api/file.api';

const { Header, Sider, Content, Footer } = Layout;

interface LayoutProps {
  children: React.ReactNode;
}

type MenuItem = Required<MenuProps>['items'][number];

const LayoutSystem = (props: LayoutProps) => {
  const { count, notification, resetCount } = useContext(NotificationContext);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const pathName: any = location.pathname.split('/');
  const [collapsed, setCollapsed] = useState(false);
  const [showChangePasswordModal, setShowChangePasswordModal] = useState<boolean>(false);

  const [user, setUser] = useState<UserDetailDto>();
  const [imageId, setImageId] = useState<number>(0);
  const [imageUrl, setImageUrl] = useState<string | null>('');

  // const user: UserDetailDto = JSON.parse(localStorage.getItem(CURRENT_USER) || '');
  function getItem(label: React.ReactNode,
                   key: React.Key,
                   authority?: Authority | Boolean,
                   icon?: React.ReactNode,
                   children?: MenuItem[],
                   type?: 'group'): any {
    if (hasAuthority(authority as Authority)) {
      return {
        key, icon, children, label, type,
      } as MenuItem;
    } else {
      return;
    }
  }

  const getDetail = () => {
    userApi.currentLoggedInUserDetail().then((res) => {
      const response: GenericResponse<UserDetailDto> = res.data;
      if (response.success) {
        const userDetailDto = response.data;
        setUser(userDetailDto);
        setImageId(userDetailDto.imageId);
        if (userDetailDto.imageId !== null) {
          fileApi.getImage(userDetailDto.imageId).then((res) => {
            setImageUrl(createImageSourceFromBase64(res.data.data.data));
          });
        }
      }
    });
  };
  
  useEffect(() => {
    getDetail();
  }, []);

  const items: MenuItem[] = [
    /*TODO: sua lai authority cho nay nua*/
    getItem(
      'Quản lý thiết bị y tế',
      'medical-equipments',
      Authority.MEDICAL_READ,
      <ClusterOutlined />,
      [
      getItem(<NavLink to={'/medical-equipments'} >Danh sách thiết bị</NavLink>, '/medical-equipments', Authority.MEDICAL_READ ),
        // getItem('Danh sách thiết bị', '', Authority.EQUIPMENT_READ),
        getItem(<NavLink to={'/medical-equipments/create'} >Nhập thiết bị mới</NavLink>, '/medical-equipments/create', Authority.MEDICAL_CREATE),
        // getItem('Nhập thiết bị mới', '/create', Authority.EQUIPMENT_CREATE),
        // getItem('Nhập thiết bị bằng Excel', '/import_excel_eq', Authority.EQUIPMENT_CREATE),
        getItem(<NavLink to={'/medical-equipments/handovers'} >Bàn giao</NavLink>, '/medical-equipments/handovers', Authority.MEDICAL_HANDOVER_READ),
        getItem(<NavLink to={'/medical-equipments/report-broken'} >Báo hỏng</NavLink>, '/medical-equipments/report-broken', Authority.MEDICAL_REPORT_BROKEN_READ),
        getItem(<NavLink to={'/medical-equipments/repairs'} >Sửa chữa</NavLink>, '/medical-equipments/repairs', Authority.MEDICAL_REPAIR_READ),
        getItem(
          <NavLink to={'/medical-equipments/maintenances'} >Bảo dưỡng định kỳ</NavLink>,
          '/medical-equipments/maintenances',
          Authority.MEDICAL_MAINTENANCE_READ
        ),
        getItem(<NavLink to={'/medical-equipments/inspections'} >Kiểm định</NavLink>, '/medical-equipments/inspections', Authority.MEDICAL_INSPECTION_READ),
        // getItem('Bảo hành', '14', Authority.INSURANCE_EQUIPMENT_READ),
        getItem(<NavLink to={'/medical-equipments/transfers'} >Điều chuyển</NavLink>, '/medical-equipments/transfers', Authority.MEDICAL_TRANSFER_READ),
        getItem(
          <NavLink to={'/medical-equipments/liquidations'} >Thanh lý</NavLink>,
          '/medical-equipments/liquidations',
          Authority.MEDICAL_LIQUIDATION_READ
        ),
        getItem(
          <NavLink to={'/medical-equipments/inventories'} >Kiểm kê</NavLink>,
          '/medical-equipments/inventories',
          Authority.MEDICAL_LIQUIDATION_READ
        ),
      ]
    ),

    getItem(
      'Quản lý vật tư',
      'supplies',
      Authority.SUPPLY_READ,
      <SisternodeOutlined />,
      [
        getItem(<NavLink to={'/supplies'} >Danh sách vật tư</NavLink>, '/supplies', Authority.SUPPLY_READ),
        getItem(<NavLink to={'/supplies/create'} >Thêm mới vật tư</NavLink>, '/supplies/create', Authority.SUPPLY_CREATE),
      ]
    ),
    getItem(
      'Quản lý tài sản công',
      'office-equipments',
      Authority.OFFICE_READ,
      <DesktopOutlined />,
      [
        getItem(<NavLink to={'/office-equipments'} >Danh sách thiết bị</NavLink>, '/office-equipments', Authority.OFFICE_READ),
        // getItem('Danh sách thiết bị', '', Authority.EQUIPMENT_READ),
        getItem(<NavLink to={'/office-equipments/create'} >Nhập thiết bị mới</NavLink>, '/office-equipments/create', Authority.OFFICE_CREATE),
        // getItem('Nhập thiết bị mới', '/create', Authority.EQUIPMENT_CREATE),
        // getItem('Nhập thiết bị bằng Excel', '/import_excel_eq', Authority.EQUIPMENT_CREATE),
        getItem(<NavLink to={'/office-equipments/handovers'} >Bàn giao</NavLink>, '/office-equipments/handovers', Authority.OFFICE_HANDOVER_READ),
        getItem(<NavLink to={'/office-equipments/report-broken'} >Báo hỏng</NavLink>, '/office-equipments/report-broken', Authority.OFFICE_REPORT_BROKEN_READ),
        getItem(<NavLink to={'/office-equipments/repairs'} >Sửa chữa</NavLink>, '/office-equipments/repairs', Authority.OFFICE_REPAIR_READ),
        getItem(
          <NavLink to={'/office-equipments/maintenances'} >Bảo dưỡng định kỳ</NavLink>,
          '/office-equipments/maintenances',
          Authority.OFFICE_MAINTENANCE_READ
        ),
        getItem(<NavLink to={'/office-equipments/transfers'} >Điều chuyển</NavLink>, '/office-equipments/transfers', Authority.OFFICE_TRANSFER_READ),
        getItem(
          <NavLink to={'/office-equipments/liquidations'} >Thanh lý</NavLink>,
          '/office-equipments/liquidations',
          Authority.OFFICE_LIQUIDATION_READ
        ),
      ]
    ),
    getItem(
      'Thống kê thiết bị y tế',
      'statistic/medical-equipments',
      Authority.MEDICAL_STATISTIC,
      <BarChartOutlined />,
      [
        getItem(
          <NavLink to={'/statistic/medical-equipments'} >Thống kê thiết bị</NavLink>,
          '/statistic/medical-equipments',
          Authority.MEDICAL_STATISTIC,
        ),
        // getItem(<NavLink to={'/statistic/supplies'} >Thống kê vật tư</NavLink>,
        // '/supplies', Authority.STATISTIC_EQUIPMENT),
        // ,
        getItem('Thống kê theo khoa',
        '/statistic/medical-equipments/departments', Authority.MEDICAL_STATISTIC, 
        null,
      [
        getItem(
          <NavLink to={'/statistic/medical-equipments/departments/statuses'} >Theo trạng thái</NavLink>,
          '/statistic/medical-equipments/departments/statuses',
          Authority.MEDICAL_STATISTIC),
        getItem(
            <NavLink to={'/statistic/departments/risk-levels'} >Theo mức độ rủi ro</NavLink>,
            '/statistic/medical-equipments/departments/risk-levels',
            Authority.MEDICAL_STATISTIC),
      ]
        ),
        ,
        getItem(<NavLink to={'/statistic/medical-equipments/groups'} >Thống kê theo nhóm - loại</NavLink>,
        '/statistic/medical-equipments/groups', Authority.MEDICAL_STATISTIC),
        ,
      ]
    ),
    getItem(
      'Thống kê Tài sản công',
      'statistic/office-equipments',
      Authority.OFFICE_STATISTIC,
      <BarChartOutlined />,
      [
        getItem(
          <NavLink to={'/statistic/office-equipments'} >Thống kê thiết bị</NavLink>,
          '/statistic/office-equipments',
          Authority.OFFICE_STATISTIC,
        ),
        // getItem(<NavLink to={'/statistic/supplies'} >Thống kê vật tư</NavLink>,
        // '/supplies', Authority.STATISTIC_EQUIPMENT),
        // ,
        getItem('Thống kê theo khoa',
        '/statistic/office-equipments/departments', Authority.OFFICE_STATISTIC, 
        null,
      [
        getItem(
          <NavLink to={'/statistic/office-equipments/departments/statuses'} >Theo trạng thái</NavLink>,
          '/statistic/office-equipments/departments/statuses',
          Authority.OFFICE_STATISTIC),
      ]
        ),
        ,
        getItem(<NavLink to={'/statistic/office-equipments/groups'} >Thống kê theo nhóm - loại</NavLink>,
        '/statistic/office-equipments/groups', Authority.OFFICE_STATISTIC),
        ,
      ]
    ),
    getItem(
      'Quản lý tổ chức',
      '/organization',
      Authority.DEPARTMENT_READ,
      <PartitionOutlined />,
      [
        getItem(<NavLink to={'/organization/departments'} >Khoa phòng</NavLink>, '/organization/departments', Authority.DEPARTMENT_READ),
        getItem(<NavLink to={'/organization/suppliers'} >Nhà cung cấp dịch vụ</NavLink>, '/organization/suppliers', Authority.SUPPLIER_READ),
        // getItem(<NavLink to={'/organization/users'} >Quản lý thành viên</NavLink>, '/users', Authority.USER_READ),
        getItem(<NavLink to={'/organization/projects'} >Quản lý dự án</NavLink>, '/organization/projects', Authority.PROJECT_READ),
      ]
    ),
    getItem(
      'Quản lý thành viên',
      // <NavLink to={'/users'} >Quản lý thành viên</NavLink>,
      '/users',
      Authority.USER_CREATE,
      <TeamOutlined />,
      )
    
    ,
    getItem(
      'Quản lý danh mục',
      '/category',
      Authority.MEDICAL_GROUP_READ || Authority.OFFICE_GROUP_READ,
      <UnorderedListOutlined />,
      [
        getItem(
          'Nhóm thiết bị',
          '/category/groups',
          Authority.MEDICAL_GROUP_READ || Authority.OFFICE_GROUP_READ,
          null,
          [
            getItem(
              <NavLink to={'/category/groups/medical-equipment-groups'} >Thiết bị y tế</NavLink>,
              '/category/groups/medical-equipment-groups',
              Authority.MEDICAL_GROUP_READ,
            ),
            getItem(
              <NavLink to={'/category/groups/office-equipment-groups'} >Tài sản công</NavLink>,
              '/category/groups/office-equipment-groups',
              Authority.OFFICE_GROUP_READ,
            ),
            
          ]
        ),


        getItem(
          'Loại thiết bị',
          '/category/types',
          Authority.MEDICAL_CATEGORY_READ || Authority.OFFICE_CATEGORY_READ,
          null,
          [
            getItem(
              <NavLink to={'/category/types/medical-equipment-categories'}>Thiết bị y tế</NavLink>,
              '/category/types/medical-equipment-categories',
              Authority.MEDICAL_CATEGORY_READ
            ),
            getItem(
              <NavLink to={'/category/types/office-equipment-categories'}>Tài sản công</NavLink>,
              '/category/types/office-equipment-categories',
              Authority.OFFICE_CATEGORY_READ
            ),
          ]
        ),
        getItem(
          <NavLink to={'/category/equipment-units'} >Đơn vị tính</NavLink>,
          '/category/equipment-units',
          Authority.EQUIPMENT_UNIT_READ
        ),
        getItem(<NavLink to={'/category/services'} >Loại dịch vụ</NavLink>, '/category/services', Authority.SERVICE_READ),
      ]
    ),

    

    getItem(
    // <NavLink to={'/setting'} >Cài đặt</NavLink>,
    'Cài đặt',
     '/setting', Authority.ROLE_TPVT, <SettingOutlined />, [
      getItem(<NavLink to={'/setting/email-config'} >Cấu hình hệ thống</NavLink>, '/setting/email-config', Authority.ROLE_TPVT),
      getItem(<NavLink to={'/setting/roles'} >Phân quyền</NavLink>, '/setting/roles', Authority.ROLE_TPVT),
    ]),
  ];


  const menu = (<>
  
    <Menu
      onAnimationEnd={() => {
        // console.log("endlist");
      }}
      items={notification.map((item: Notification<any>) => {
        return {
          key: item.id, label: (<div
            className={`text-base`}
          >
            <Row onClick={() => navigate('/setting/notifications')}>{item.content}</Row>
            <div className='font-medium'>
              {moment(item.createdAt).format('hh:mm:ss, DD-MM-YYYY')}
            </div>
            <Divider className='my-2.5' />
          </div>),
        };
      })}
      style={{ width: 500, height: 500, overflowY: 'scroll' }}
    />
    <div
      className='bg-red-100 p-4 cursor-pointer rounded-b-md'
      onClick={() => navigate('/setting/notifications')}
    >
      <div className='text-center text-base'>Xem tất cả thông báo</div>
    </div>
  </>);

  const onClick: MenuProps['onClick'] = (e) => {
    // if (e.keyPath[1] == undefined) {
    //   navigate(`${e.keyPath[0]}`);
    // } else {
    //   navigate(`${e.keyPath[1]}${e.keyPath[0]}`);
    // }
    navigate(`${e.keyPath[0]}`);

    // if (e.keyPath[1] == undefined) {
    //   navigate(`${e.keyPath[0]}`);
    // } else {
    //   navigate(`${e.keyPath[1]}${e.keyPath[0]}`);
    // }
  };

  const toggle = () => {
    setCollapsed(!collapsed);
  };

  const handleLogout = () => {
    dispatch(authActions.logout());
  };

  return (
  <Layout>
    <Sider
      // theme='light'
      collapsible
      collapsed={collapsed}
      onCollapse={toggle}
      width='260px'
      // className='bg-white min-h-screen'
      style={{
        overflowY: 'auto',
        // overflowX: 'hidden', 
        height: '100vh', position:'fixed', color:'white'
      }}
      // trigger={null}
    >
      <div
        className='flex flex-row items-center p-4 gap-2 cursor-pointer'
        onClick={() => navigate('/')}
      >
        <img
          src={logo}
          alt='logo'
          style={{
            width: '30px', height: '30px', borderRadius: '50%',
          }}
        />
        {!collapsed && (<div className='font-medium text-base'>Hệ thống quản lý <br />trang thiết bị y tế  <br/>và tài sản công</div>)}
      </div>
      <Divider className='m-1' />
      <Menu
        theme='dark'
        onClick={onClick}
        // style={{ width: 250 }}
        defaultSelectedKeys={[`/${pathName[2]}`]}
        defaultOpenKeys={[`/${pathName[1]}`]}
        items={items}
        mode='inline'
      />
    </Sider>
    <Layout
      className='min-h-screen'
      style={collapsed ? {
        marginLeft: '4.6rem',
      } : {
        // marginLeft: '10px',
        marginLeft: '260px',
      }}
    >
      <Header className='bg-white p-0 h-[66px]'>
        <Row className='flex justify-between items-center gap-4 px-6'>
          <BarsOutlined onClick={toggle} />
          <div className='flex flex-row items-center gap-6 cursor-pointer'>
            {/* <SettingFilled /> */}
            <div  onClick={event => resetCount}>
              <Dropdown
                overlay={menu}
                placement='bottomRight'
                arrow
                overlayClassName='rounded-3xl'
                trigger={['click']}
              >
                <Badge count={count}>
                  <BellFilled onClick={resetCount} className='h-5 w-5' />
                </Badge>
              </Dropdown>
            </div>
            <Avatar icon={<UserOutlined />} />
            {/* <Card title={user?.name}>{user?.role?.description}</Card> */}
            <div>{user?.name} - {user?.role?.description}</div>
            <div></div>
            <Dropdown
              overlay={<Menu>
                <Menu.Item key='profile'>
                  <Link to='/profile'>Tài khoản</Link>
                </Menu.Item>
                <Menu.Item key='change_password'>
                  <Row onClick={() => setShowChangePasswordModal(true)}>
                    Thay đổi mật khẩu
                  </Row>
                </Menu.Item>
                <Menu.Item key='signout' onClick={handleLogout}>
                  Đăng xuất
                </Menu.Item>
              </Menu>}
              placement='bottomRight'
            >
              <DownOutlined />
            </Dropdown>
          </div>
        </Row>
      </Header>
      <Divider className='m-0' />
      <Content
        style={{
          margin: '24px 16px',
        }}
      >
        <div
          className='site-layout-background'
          style={{
            maxWidth: '1600px', margin: '0 auto', padding: 20,
          }}
        >
          {props.children}
        </div>
      </Content>
      <Footer>
        <div className='text-base font-medium'>
          Copyright © 2024 iBME HUST
        </div>
      </Footer>
    </Layout>
    <ModalChangePassword
      showChangePasswordModal={showChangePasswordModal}
      setShowChangePasswordModal={() => setShowChangePasswordModal(false)}
    />
  </Layout>);
};

export default LayoutSystem;
