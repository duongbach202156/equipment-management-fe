import { Button, Form, Input, Modal, Select } from 'antd';
import { toast } from 'react-toastify';
import { useContext, useState } from 'react';
import { UpsertEquipmentCategoryForm } from '../../../types/equipmentCategory.type';
import { medicalEquipmentCategoryApi } from '../../../api/medicalEquipmentCategory.api';
import { options } from '../../../utils/globalFunc.util';
import { FilterContext } from '../../../contexts/filter.context';
import { officeEquipmentCategoryApi } from 'api/officeEquipmentCategory.api';

export interface ModalCreateEquipmentCategoryProps {
  showCreateEquipmentCategoryModal: boolean;
  hideCreateEquipmentCategoryModal: () => void;
  callback: () => void;
}

export const ModalCreateOfficeEquipmentCategory = (props: ModalCreateEquipmentCategoryProps) => {
  const { showCreateEquipmentCategoryModal, hideCreateEquipmentCategoryModal, callback } = props;
  const [form] = Form.useForm<UpsertEquipmentCategoryForm>();
  const [loading, setLoading] = useState<boolean>(false);
  const { officeEquipmentGroups } = useContext(FilterContext);
  const createEquipmentCategory = (values: UpsertEquipmentCategoryForm) => {
    setLoading(true);
    officeEquipmentCategoryApi.createEquipmentCategory(values).then(() => {
      toast.success('Tạo loại thiết bị thành công');
    }).finally(() => {
      setLoading(false);
      hideCreateEquipmentCategoryModal();
      form.resetFields();
      callback();
    });
  };

  return (<Modal
    title='Tạo mới loại thiết bị'
    open={showCreateEquipmentCategoryModal}
    onCancel={() => {
      hideCreateEquipmentCategoryModal();
    }}
    footer={null}
  >
    <Form form={form} layout='vertical' size='large' onFinish={createEquipmentCategory}>
      <div className='grid grid-cols-1 gap-5'>

        <Form.Item
          label='Tên loại thiết bị' name='name' required={true} rules={[
          { required: true, message: 'Vui lòng nhập tên loại thiết bị' },
        ]}>
          <Input className='input' />
        </Form.Item>
        <Form.Item
          label=' Kí hiệu' name='alias'
          required={true} rules={[
          { required: true, message: 'Vui lòng nhập kí hiệu thiết bị' },
        ]}
        >
          <Input className='input' />
        </Form.Item>

        <Form.Item
          label=' Ghi chú' name='note'>
          <Input className='input' />
        </Form.Item>

        <Form.Item
          name='groupId'
          required={true}
          label='Nhóm thiết bị'
          rules={[
            { required: true, message: 'Vui lòng chọn nhóm thiết bị' },
          ]}
        >
          <Select
            showSearch
            placeholder=' Chọn nhóm thiết bị'
            optionFilterProp='children'
            allowClear
            filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
            options={options(officeEquipmentGroups)}
          />
        </Form.Item>

      </div>
      <div className='flex flex-row justify-end gap-4'>
        <Form.Item>
          <Button htmlType='submit' className='button' loading={loading}>Xác nhận</Button>
        </Form.Item>
        <Form.Item>
          <Button onClick={() => hideCreateEquipmentCategoryModal()} className='button'>Đóng</Button>
        </Form.Item>
      </div>
    </Form>
  </Modal>);
};
