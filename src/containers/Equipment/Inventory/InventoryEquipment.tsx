import { CheckCircleOutlined, CheckOutlined, CloseCircleOutlined, EditFilled, PlusCircleFilled } from "@ant-design/icons";
import { Button, Card, Divider, Form, Input, PaginationProps, Popconfirm, Progress, Select, Table, TableColumnsType, Tag, Tooltip, message } from "antd";
import equipmentInventoryApi from "api/equipment_inventory.api";
import { TableFooter } from "components/TableFooter";
import useDebounce from "hooks/useDebounce";
import useQuery from "hooks/useQuery";
import useSearchName from "hooks/useSearchName";
import { useEffect, useState } from "react";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { PageableRequest } from "types/commonRequest.type";
import { Pageable } from "types/commonResponse.type";
import { GetInventoryEquipmentQueryParam, InventoryDto, InventoryEquipmentDto, InventoryEquipmentForm, InventoryFullInfoDto, UpsertInventoryForm } from "types/inventory.type";
import { createUrlWithQueryString, getDateForRendering, getDepartmentOptions, options } from "utils/globalFunc.util";
import { ModalCreateInventory } from "./ModalCreateInventory";
import { useForm } from "antd/lib/form/Form";
import { mapInventoryEquipmentDtoToInventoryEquipmentForm, mapInventoryFullInfoDtoToUpsertInventoryForm, } from "utils/mapper.util";
import { values } from "lodash";
import TextArea from "antd/lib/input/TextArea";
import { error } from "console";
import DatePicker from "components/DatePicker";

const InventoryEquipment = () => {
    const { onChangeSearch } = useSearchName();
    const navigate = useNavigate();
    const location = useLocation();
    const pathName: any = location?.pathname;
    const query = useQuery();
    const [searchQuery, setSearchQuery] = useState<any>({});
    const currentPage = query?.page;
    const currentName = query?.name;
    const currentDepartment = query?.departmentId;
    const [getInventoryEquipmentQueryParam, setGetInventoryEquipmentQueryParam] 
    = useState<GetInventoryEquipmentQueryParam>({});
    const [departments, setDepartments] = useState([]);
    const [pageable, setPageable] = useState<Pageable>({ number: 0, size: 10 });
  const [pageableRequest, setPageableRequest] = useState<PageableRequest>({ size: 10, page: 0 });

    const [page, setPage] = useState<number>(currentPage || 1);
    const [size, setSize] = useState<number>(20);
    const [total, setTotal] = useState<number>(1);
    const [loading, setLoading] = useState<boolean>(false);
    const [name, setName] = useState<string>(currentName);
    let searchQueryString: string;
    const [isShowCustomTable, setIsShowCustomTable] = useState<boolean>(false);
    const [showCreateInventoryModal, setShowCreateInventoryModal] = useState<boolean>(false);

  const params = useParams<{ inventoryId: string }>();
  const { inventoryId } = params;

  const [inventories, setInventories] = useState<InventoryFullInfoDto>();
  const [inventoryEquipmentList, setInventoryEquipmentList] = useState<InventoryEquipmentDto[]>([]);
  const [inventoryEquipmentFormList, setInventoryEquipmentFormList] = useState<InventoryEquipmentForm[]>([]);

  const [componentShouldUpdate, setComponentShouldUpdate] = useState<boolean>(false);

  // const currentKeyword = query?.keyword;
  const [keyword, setKeyword] = useState<string>('');
  // const keywordSearch = useDebounce(keyword, 500);
  const [queryString, setQueryString] = useState<string>(location.search);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [inventoryEquipmentForm] = Form.useForm<InventoryEquipmentForm>();
  const [inventoryForm] = Form.useForm<UpsertInventoryForm>();
  const [editingRow, setEditingRow] = useState<number>();
  const [inventoryEquipmentId, setInventoryEquipmentId] = useState<number>();
  const [percent, setPercent] = useState<number>(0);
  const [messageApi, contextHolder] = message.useMessage();
  const [totalEquipment, setTotalEquipment] = useState<number>(0);
  const [searchResult, setSearchResult] = useState<InventoryEquipmentDto[]>([]);
  const [inventoriedEquipment, setInventoriedEquipment] = useState<React.Key[]>([]);
  const [confirm, setConfirm] = useState<boolean>(false);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    // console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
    const newData = inventoryEquipmentFormList.map((item) => {
      return {
        ...item,
        inventoried: false
      }
    });
    newSelectedRowKeys.forEach((key) => {
      const index = newData.findIndex((item) => item.id === key as number)
      newData[index].inventoried = true;
    })
    console.table(newData);
    setInventoryEquipmentFormList(newData);
    inventoryForm.setFieldValue('inventoryEquipmentList', newData);
    const percent = (newSelectedRowKeys.length / newData.length) * 100 | 0;
    setPercent(percent);
    // console.table(newData);
    // console.log("s", hasSelected)
  };


  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      // Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
    ]
  };
  


  const isEditing = (item : any) => item.id === editingRow;
  const cancel = () => {
    setEditingRow(undefined);
    setInventoryEquipmentId(editingRow as number);
    
  }

  const isSelected = (id : number) => selectedRowKeys.includes(id);
  const handleSave = async (row : InventoryEquipmentDto) => {
    const formRow = (await inventoryEquipmentForm.validateFields()) as InventoryEquipmentForm;
    // console.table(formRow);
    const formData = inventoryEquipmentList.map((item)=> mapInventoryEquipmentDtoToInventoryEquipmentForm(item));
    const inventoryEquipmentFormList = [...formData];
    const index = inventoryEquipmentFormList.findIndex((item) => row.id === item.id);
    const item = inventoryEquipmentFormList[index];
    inventoryEquipmentFormList.splice(index, 1, {...item, ...formRow});
    const updateDtoList = inventoryEquipmentList.map((item, index) => {
      const isInventoried = isSelected(item.id);
      inventoryEquipmentFormList[index].inventoried = isInventoried;
        return {
          ...item,
          inventoried: isInventoried,
          note: inventoryEquipmentFormList[index].note
        }
    })
    setInventoryEquipmentFormList(inventoryEquipmentFormList);
    inventoryForm.setFieldValue('inventoryEquipmentList', inventoryEquipmentFormList);
    setInventoryEquipmentList(updateDtoList);
    setSearchResult(updateDtoList);
    // setComponentShouldUpdate(true);
    setEditingRow(undefined);
  }

  const handleEdit = (item : InventoryEquipmentForm) => {
    setEditingRow(item.id);
    // const formItem = mapInventoryEquipmentDtoToInventoryEquipmentForm(item);
    inventoryEquipmentForm.setFieldsValue({
          ...item
    })
    // console.table(item);
  }

  const save = (row : InventoryEquipmentDto) => {
      // handleEdit(row);
      handleSave(row);
  }


    const columns : TableColumnsType<InventoryEquipmentDto> = [
        {
            title: 'Tên thiết bị', 
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Model', 
            dataIndex: 'model',
            key: 'model',
            // show: true,
        },
        {
            title: 'Serial', 
            dataIndex: 'serial',
            key: 'serial',
            // show: true,
            // render(item: InventoryDto): JSX.Element {return <div>{getDateForRendering(item.inventoryDate)}</div>;},

        },
        {
            title: 'Ghi chú', 
            // dataIndex: 'note',
            key: 'note',
            render: (item: InventoryEquipmentForm) => {
              if (editingRow === item.id) {
                return <>
                 <Form.Item name="note">
                 <TextArea rows={2} className='textarea' />
                    {/* <Input allowClear className='input'></Input> */}
                  </Form.Item>
                </>
              } else {
                return <p>{item.note}</p>
              }
            }
        },

        {
          title: 'Tác vụ', 
          render: (item: InventoryEquipmentDto) => {
            const editable = isEditing(item);
            if (editable) {
              return (
                <>
                <Button type="link" onClick={() =>handleSave(item)}>Lưu</Button>
                <Popconfirm cancelText='Hủy' title="Xác nhận hủy bỏ thao tác?" onConfirm={cancel}>
                <Button type="link">Hủy</Button>
                </Popconfirm>
                </>
              )
            } else {
              return (
                <>
                <Tooltip title='Cập nhật'>
                <Link onClick={() => handleEdit(mapInventoryEquipmentDtoToInventoryEquipmentForm(item))} to={""}><EditFilled /></Link>
                </Tooltip>
                
                {/* <Button></Button> */}
                </>
              )
            }
            }
            
          
      },

        Table.SELECTION_COLUMN,
        {
          title: 'Đã kiểm kê', 
          dataIndex: 'inventoried',
          key: 'inventoried',
          // show: true,
          
      },
  
      
       
    ]


    const onPaginationChange = (page: number, size: number) => {
        setPage(page);
        setSize(size);
        searchQuery.page = page;
        searchQuery.size = size;
        setSearchQuery(searchQuery);
        searchQueryString = new URLSearchParams(searchQuery).toString();
        navigate(`${pathName}?${searchQueryString}`);
      };
    
      const pagination: PaginationProps = {
        current: pageable.number as number + 1,
        total: pageable.totalElements,
        pageSize: pageable.size,
        showTotal: (total: number) => `Tổng cộng: ${total} thiết bị`,
        onChange: onPaginationChange,
        showQuickJumper: true,
      };

      const onChangeKeyword = (key: string) => {
        setKeyword(key);
      }
      const onChangeQueryParams = (key: string, value: string | string[] | undefined | number | PageableRequest | number[]) => {
        let getInventoryEquipmentQueryParamClone: GetInventoryEquipmentQueryParam = getInventoryEquipmentQueryParam;
        if (key === '' ) { //if key is empty, it means that the component should be updated
          setComponentShouldUpdate(true);
        }
        if (key === 'keyword') {
          getInventoryEquipmentQueryParamClone = { ...getInventoryEquipmentQueryParam, keyword: value as string };
        }
        setGetInventoryEquipmentQueryParam(getInventoryEquipmentQueryParamClone);
        const url = createUrlWithQueryString(location.pathname, getInventoryEquipmentQueryParamClone);
        setQueryString(url);
        // navigate(url);
      };
      const hasSelectedAll = selectedRowKeys.length === inventoryEquipmentList.length && inventoriedEquipment.length > 0;
      const hasSelected = selectedRowKeys.length > 0;
      

      const onFinish = (values: UpsertInventoryForm) => {
        // console.table(inventoryEquipmentFormList);
        // console.log(values);
        inventoryForm.setFieldValue('inventoryEquipmentList', inventoryEquipmentFormList);
        equipmentInventoryApi.updateInventory(Number(inventoryId), values).then((res) => {
          setComponentShouldUpdate(true);
          navigate(`/medical-equipments/inventories/${res.data.data.id}`);
          toast.success('Cập nhật thông tin kiểm kê thành công!');
        }).catch(error => {
          toast.error('Cập nhật không thành công')
        })
      }

      const onConfirm = () => {
        equipmentInventoryApi.confirmInventory(Number(inventoryId)).then((res) => {
          setComponentShouldUpdate(true);
          navigate(`/medical-equipments/inventories`);
          toast.success('Hoàn thành kiểm kê!');
        }).catch(error => {
          if (selectedRowKeys.length != inventoriedEquipment.length) {
              toast.error('Vui lòng lưu thông tin trước khi xác nhận!')
          }
          // toast.error('Cập nhật không thành công')
        })
      }

      
      useEffect(() => {
        equipmentInventoryApi.getInventoryById(Number(inventoryId), getInventoryEquipmentQueryParam).then((res) => {
            if (res.data.success) {
              const inventory = res.data.data;
              inventoryForm.setFieldsValue(mapInventoryFullInfoDtoToUpsertInventoryForm(inventory))
              setInventories(inventory as InventoryFullInfoDto);

              const inventoryEquipmentList = inventory.inventoryEquipmentDtoList;
              setInventoryEquipmentList(inventoryEquipmentList as InventoryEquipmentDto[]);

              const inventoryEquipmentFormList = inventoryEquipmentList.map(
                (item) => mapInventoryEquipmentDtoToInventoryEquipmentForm(item)
              )
              setInventoryEquipmentFormList(inventoryEquipmentFormList);

              setInventoryEquipmentList(inventoryEquipmentList);
              setSearchResult(inventoryEquipmentList);
              
              const selectedRowKeys = inventoryEquipmentList.filter(item => item.inventoried === true).map(item => item.id)
              setInventoriedEquipment(selectedRowKeys);
              setSelectedRowKeys(selectedRowKeys);

              const totalEquipment = inventory.inventoryEquipmentDtoList.length;
              setTotalEquipment(totalEquipment);
              const percent = (selectedRowKeys.length / totalEquipment) * 100 | 0;
              setPercent(percent);

              setConfirm(inventory.confirmed);

              // setPageable(res.data.data.page as Pageable);
              setLoading(false);
              return res;
            }
          }).catch((err) => {
            toast.error('Có lỗi xảy ra khi tải dữ liệu');
          });
        
        setComponentShouldUpdate(false);
        // setPageableRequest({
        //   page: Number(query.page) || 0, size: Number(query.size) || 10,
        // });
        // searchListOfEquipment();
        // setLoading(false);
      }, [inventoryId, queryString, componentShouldUpdate]);

      useEffect(() => {
        if (keyword === '') {
          setSearchResult(inventoryEquipmentList);
        } else {
          const result = inventoryEquipmentList.filter((item) => 
            item.name.includes(keyword) || item.serial.includes(keyword) || item.model.includes(keyword) 
          )
          setSearchResult(result);
        }
      }, [keyword])

    return (
      
        <div>
            <div className="flex-between-center">
                <div className="title">THÔNG TIN PHIẾU KIỂM KÊ</div>
              </div>
            <Divider />
            
      <div className="flex justify-between flex-col">
      <Card>
       <Form form={inventoryForm} className='basis-2/3'
        layout='vertical'
        size='large'
        onFinish={onFinish}
        disabled={confirm}
        >
       
       <div className='grid grid-cols-2 gap-5'>
       <Form.Item
            label='Tên hiển thị'
            name='name'
            className='mb-5'
          >
            <Input
              placeholder='Nhập tên hiển thị' allowClear className='input'
            />
          </Form.Item>
        <Form.Item label='Ngày kiểm kê' name='inventoryDate' className='mb-5'>
            <DatePicker className='input w-[-webkit-fill-available]' placeholder='Chọn ngày' />
          </Form.Item>
        <Form.Item
            label='Khoa Phòng'
            name='departmentName'
            className='mb-5'
          >
           <Input
             className='input' disabled
            />
          </Form.Item>
          <Form.Item
            label='Người kiểm kê'
            name='inventoryPerson'
            className='mb-5'
          >
           <Input
             className='input' disabled
            />
          </Form.Item>
          <Form.Item name="departmentId" hidden={true}></Form.Item>
          <Form.Item name="inventoryEquipmentList" hidden={true}></Form.Item>

      </div>
         
       <div>
      <div style={{ marginBottom: 16 }} className="grid grid-cols-4 gap-5">
            <Button className="button" htmlType='submit'>
              Lưu
            </Button>
            <div>
                  {inventories?.confirmed === true ? 
                <Tag  style={{fontSize: '16px'}}  icon={<CheckCircleOutlined style={{display: 'inline-flex'}}></CheckCircleOutlined>} color="success">Đã hoàn thành</Tag> 
                : <Tag icon={<CloseCircleOutlined style={{display: 'inline-flex'}} />} color="error">Chưa hoàn thành</Tag>}
                  </div>
          </div>
          <Divider></Divider>
      <div style={{ marginBottom: 16 }} className="grid grid-cols-4 gap-5">
      <Button className="button_accept" disabled={!hasSelectedAll || confirm} onClick={onConfirm}>
              Xác nhận hoàn thành kiểm kê
            </Button>
            <Progress percent={percent} type="line" />
            <span style={{ marginLeft: 8 , fontSize: 20}}>
            {hasSelected ? `Đã chọn ${selectedRowKeys.length}/ ${totalEquipment} thiết bị` : ''}
            </span>
            
      </div>
      <Divider></Divider>
      <div className='grid grid-cols-2 gap-5'>
        </div>
                <div className="title text-center" style={{marginBottom: '20px'}}>DANH SÁCH THIẾT BỊ</div>
       
            <Input
         style={{
            width: '100%',
          }}
          placeholder='Tìm kiếm thiết bị kiểm kê theo tên, model, serial...'
          allowClear
          // value={keyword}
          className='input'
          onChange={(e) => setKeyword(e.target.value)}
        />

          
        </div>
       <Form form={inventoryEquipmentForm} component={false} >
       <Table
        rowKey={(record) => record.id}
        rowSelection={rowSelection}
        columns={columns}
        dataSource={searchResult}
        className="mt-6 shadow-md"
        // footer={() => <TableFooter paginationProps={pagination} />}
        // pagination={true}
        loading={loading}
        />
        </Form>       
        </Form> 
        </Card>   
        </div>
        </div>
    )
};
export default InventoryEquipment;

