import { CheckCircleFilled, CheckCircleOutlined, CloseCircleOutlined, PlusCircleFilled, UnorderedListOutlined } from "@ant-design/icons";
import { Button, Divider, Input, PaginationProps, Select, Table, Tag, Tooltip } from "antd";
import equipmentInventoryApi from "api/equipment_inventory.api";
import { TableFooter } from "components/TableFooter";
import useDebounce from "hooks/useDebounce";
import useQuery from "hooks/useQuery";
import useSearchName from "hooks/useSearchName";
import { useContext, useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { PageableRequest } from "types/commonRequest.type";
import { Pageable } from "types/commonResponse.type";
import { GetInventoryQueryParam, InventoryDto } from "types/inventory.type";
import { createUrlWithQueryString, getDateForRendering, getDepartmentOptions, hasAuthority } from "utils/globalFunc.util";
import { ModalCreateInventory } from "./ModalCreateInventory";
import { FilterContext } from "contexts/filter.context";
import DatePicker from "components/DatePicker";
import { Authority } from "constants/authority";

const InventoryList = () => {
    const { onChangeSearch } = useSearchName();
    const navigate = useNavigate();
    const location = useLocation();
    const pathName: any = location?.pathname;
    const query = useQuery();
    const [searchQuery, setSearchQuery] = useState<any>({});
    const currentPage = query?.page;
    const currentName = query?.name;
    const currentDepartment = query?.departmentId;
    const [getInventoryQueryParam, setgetInventoryQueryParam] 
    = useState<GetInventoryQueryParam>({departmentId: currentDepartment});
    // const [departments, setDepartments] = useState([]);
    const {departments} = useContext(FilterContext);
    const [pageable, setPageable] = useState<Pageable>({ number: 0, size: 10 });
  const [pageableRequest, setPageableRequest] = useState<PageableRequest>({ size: 10, page: 0 });

    const [page, setPage] = useState<number>(currentPage || 1);
    const [size, setSize] = useState<number>(20);
    const [total, setTotal] = useState<number>(1);
    const [loading, setLoading] = useState<boolean>(false);
    const [name, setName] = useState<string>(currentName);
    const currentKeyword = query?.keyword;
    const [keyword, setKeyword] = useState<string>(currentKeyword);
    const keywordSearch = useDebounce(currentKeyword, 500)
    // const keywordSearch = useDebounce(getInventoryQueryParam.keyword as string, 500);

    let searchQueryString: string;
    const [isShowCustomTable, setIsShowCustomTable] = useState<boolean>(false);
    const [showCreateInventoryModal, setShowCreateInventoryModal] = useState<boolean>(false);
    const [queryString, setQueryString] = useState<string>(location.search);
    const [componentShouldUpdate, setComponentShouldUpdate] = useState<boolean>(false);
    

  const [inventories, setInventories] = useState<InventoryDto[]>([]);

  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    // console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

    const columns : any = [
        {
            title: 'Tên hiển thị', 
            dataIndex: 'name',
            key: 'name',
            show: true,
        },
        {
            title: 'Khoa - Phòng', 
            dataIndex: 'departmentName',
            key: 'departmentName',
            show: true,
        },
        {
            title: 'Ngày kiểm kê', 
            // dataIndex: 'inventoryDate',
            key: 'inventoryDate',
            show: true,
            render(item: InventoryDto): JSX.Element {return <div>{getDateForRendering(item.inventoryDate)}</div>;},

        },
        {
            title: 'Người kiểm kê', 
            dataIndex: 'inventoryPerson',
            key: 'inventoryPerson',
            show: true,
        },
        {
          title: 'Trạng thái', 
          // dataIndex: 'inventoryPerson',
          key: 'isConfirm',
          render(item: InventoryDto) : JSX.Element {return item.confirmed === true ? 
                <Tag icon={<CheckCircleOutlined style={{display: 'inline-flex'}}></CheckCircleOutlined>} color="success">Đã hoàn thành</Tag> 
                : <Tag icon={<CloseCircleOutlined style={{display: 'inline-flex'}} />} color="error">Chưa hoàn thành</Tag>},
          show: true,
      },
        {
            title: 'Tác vụ',
            key: 'action',
            show: true,
            render: (item: InventoryDto) => (
              <>
              {hasAuthority(Authority.MEDICAL_INVENTORY_UPDATE) && <div>
                <Tooltip title='Danh sách thiết bị' className='mr-4'>
                  <Link to={`/medical-equipments/inventories/${item.id}`}><UnorderedListOutlined /></Link>
                </Tooltip>
              </div>}
              </>
            ),
          },
       
    ]


    const onPaginationChange = (page: number, size: number) => {
        setPage(page);
        setSize(size);
        searchQuery.page = page;
        searchQuery.size = size;
        setSearchQuery(searchQuery);
        searchQueryString = new URLSearchParams(searchQuery).toString();
        navigate(`${pathName}?${searchQueryString}`);
      };
    
      const pagination: PaginationProps = {
        current: page,
        total: total,
        pageSize: size,
        showTotal: (total: number) => `Tổng cộng: ${total} thiết bị`,
        onChange: onPaginationChange,
        showQuickJumper: true,
      };

      const searchListOfEquipment = (queryParams: GetInventoryQueryParam, pageableRequest: PageableRequest) => {
        equipmentInventoryApi.getAllInventoryMedical(queryParams, pageableRequest).then((res) => {
          if (res.data.success) {
            // console.table(res.data.data.content as InventoryDto[])
            setInventories(res.data.data.content as InventoryDto[]);
            setPageable(res.data.data.page as Pageable);
            setLoading(false);
          }
        }).catch((err) => {
          toast.error('Có lỗi xảy ra khi tải dữ liệu');
        });
      };

      const onChangeQueryParams = (key: string, value: string | string[] | undefined | number | PageableRequest | number[]) => {
        let pagebaleClone: PageableRequest;
        pagebaleClone = { ...pageableRequest, page: 0 };
        let getInventoryQueryParamClone: GetInventoryQueryParam = getInventoryQueryParam;
        getInventoryQueryParamClone.departmentId = currentDepartment;
        if (key === '') { //if key is empty, it means that the component should be updated
          setComponentShouldUpdate(true);
        }
        
        if (key === 'departmentId') {
          getInventoryQueryParamClone = { ...getInventoryQueryParam, departmentId: isNaN(Number(value)) ? undefined : Number(value) };
        }
        if (key === 'keyword') {
          getInventoryQueryParamClone = { ...getInventoryQueryParam, keyword: value as string };
        }
        if (key === 'pageable') {
          pagebaleClone = { ...pageableRequest, page: (value as PageableRequest).page, size: (value as PageableRequest).size };
        }
        if (key === 'inventoryDate') {
          value = value as string[];
          if (value != null && value[0] != null && value[1] != null) {
            getInventoryQueryParamClone = { ...getInventoryQueryParam, inventoryDateFrom: value[0], inventoryDateTo: value[1] };
          } else {
            getInventoryQueryParamClone = { ...getInventoryQueryParam, inventoryDateTo: undefined, inventoryDateFrom: undefined };
          }
        }
        setgetInventoryQueryParam(getInventoryQueryParamClone);
        setPageableRequest(pagebaleClone);
        const url = createUrlWithQueryString(location.pathname, getInventoryQueryParamClone, pagebaleClone);
        setQueryString(url);
        // navigate(url);
      };


      useEffect(() => {
        setLoading(true);
        setComponentShouldUpdate(false);
        setPageableRequest({
          page: Number(query.page) || 0, size: Number(query.size) || 10,
        });
        searchListOfEquipment(getInventoryQueryParam, pageableRequest);
        setLoading(false);
      }, [keywordSearch, componentShouldUpdate, queryString]);
    return (
        <div>
            <div className="flex-between-center">
                <div className="title">KIỂM KÊ THIẾT BỊ THEO KHOA PHÒNG</div>
                <div className='flex flex-row gap-6'>
        {hasAuthority(Authority.MEDICAL_INVENTORY_CREATE) && <Button
          className='flex-center text-slate-900 gap-2 rounded-3xl border-[#5B69E6] border-2'
          onClick={() => setShowCreateInventoryModal(true)}
        >
          <PlusCircleFilled />
          <div className='font-medium text-md text-[#5B69E6]'>Thêm mới</div>
        </Button>}
      </div>
            </div>
            <Divider />
            <div className="flex justify-between flex-col">
        <div className='flex-between-center gap-4 p-4'>    
            <Select
          showSearch
          style={{ width: '500px' }}
          placeholder='Khoa - Phòng'
          optionFilterProp='children'
          onChange={(value: any) => onChangeQueryParams('departmentId', value)}
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={getDepartmentOptions(departments)}
          value={currentDepartment}
        />
        <Tooltip
          title='Ngày thực hiện kiểm kê'>
          <DatePicker.RangePicker
            style={{
              width: '700px',
            }}
            className={'date'}
            allowClear={true}
            placeholder={['Từ ngày', 'Đến  ngày']}
            onChange={(value) => {
              if (value == null || value[0] == null || value[1] == null) {
                onChangeQueryParams('inventoryDate', undefined);
                return;
              }
              onChangeQueryParams('inventoryDate', [value[0].toISOString(), value[1].toISOString()]);
            }}
          />
        </Tooltip>
        <Input
         style={{
            width: '700px',
          }}
          placeholder='Tìm kiếm thông tin kiểm kê theo tên hiển thị, người kiểm kê,...'
          allowClear
          value={keywordSearch}
          className='input'
          onChange={(e) => onChangeQueryParams('keyword', e.target.value)}
        />
        </div>
       
        </div>
        <Table
        
        rowSelection={rowSelection}
        columns={columns.filter((item: any) => item.show)}
        dataSource={inventories}
        className="mt-6 shadow-md"
        footer={() => <TableFooter paginationProps={pagination} />}
        pagination={false}
        loading={loading}
        />
        <ModalCreateInventory
        showCreateInventoryModal={showCreateInventoryModal}
        hideCreateInventoryModal={() => setShowCreateInventoryModal(false)}
        callback={() => {
        onChangeQueryParams('', '');
      }} />
        </div>
    )
};
export default InventoryList;

