import { Button,  Form, Input, Modal, Select } from 'antd';
import equipmentGroupApi from '../../../api/medicalEquipmentGroup.api';
import { toast } from 'react-toastify';
import { useContext, useState } from 'react';
import {  UpsertEquipmentGroupForm } from '../../../types/equipmentGroup.type';
import { UpsertInventoryForm } from 'types/inventory.type';
import equipmentInventoryApi from 'api/equipment_inventory.api';
import { FilterContext } from 'contexts/filter.context';
import { getCurrentUser, getDepartmentOptions } from 'utils/globalFunc.util';
import { DATE_TIME_FORMAT, TIME_FORMAT } from 'constants/dateFormat.constants';
import DatePicker from 'components/DatePicker';

export interface ModalCreateInventoryProps {
  showCreateInventoryModal: boolean;
  hideCreateInventoryModal: () => void;
  callback: () => void;
}

export const ModalCreateInventory = (props: ModalCreateInventoryProps) => {
  const {departments} = useContext(FilterContext)
  const { showCreateInventoryModal, hideCreateInventoryModal, callback } = props;
  const [form] = Form.useForm<UpsertInventoryForm>();
  const [loading, setLoading] = useState<boolean>(false);
  const createInventory = (values: UpsertInventoryForm) => {
    setLoading(true);
    equipmentInventoryApi.createInventoryMedical(values).then(() => {
      toast.success('Tạo lịch kiểm kê thành công');
    }).finally(() => {
      setLoading(false);
      hideCreateInventoryModal();
      form.resetFields();
      callback();
    });
  };

  return (<Modal
    title='Tạo lịch kiểm kê'
    open={showCreateInventoryModal}
    onCancel={() => {
        hideCreateInventoryModal();
    }}
    footer={null}
  >
    <Form form={form} layout='vertical' size='large' onFinish={createInventory}>
      <div className='grid grid-cols-1 gap-5'>

        <Form.Item
          label='Tên hiển thị' name='name' required={true} rules={[
          { required: true, message: 'Vui lòng nhập tên hiển thị' },
        ]}>
          <Input className='input' />
        </Form.Item>
        
        <Form.Item
          label=' Khoa - Phòng' name='departmentId'
          required={true} rules={[
            { required: true, message: 'Vui lòng nhập khoa phòng' },]}
        >
          <Select
          showSearch
          placeholder='Khoa - Phòng'
          optionFilterProp='children'
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={getDepartmentOptions(departments)}
          // value={department}
        />
        </Form.Item>
        <Form.Item label='Ngày kiểm kê' name='inventoryDate' required>
          <DatePicker
            style={{
              width: '100%',
            }}
            format={DATE_TIME_FORMAT}
            // showTime={{ format: TIME_FORMAT }}
            allowClear={false}
          />
          </Form.Item>
          <Form.Item label='Người lập phiếu yêu cầu'>
          <Input className='input' disabled value={getCurrentUser().name} />
        </Form.Item>
      </div>
      <div className='flex flex-row justify-end gap-4'>
        <Form.Item>
          <Button htmlType='submit' className='button' loading={loading}>Xác nhận</Button>
        </Form.Item>
        <Form.Item>
          <Button onClick={() => hideCreateInventoryModal()} className='button'>Đóng</Button>
        </Form.Item>
      </div>
    </Form>
  </Modal>);
};
