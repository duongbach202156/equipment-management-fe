import { CheckCircleOutlined, EditTwoTone, EyeOutlined } from '@ant-design/icons';
import {
  Card,
  Descriptions,
  DescriptionsProps,
  Divider,
  Drawer,
  Menu,
  Table,
  Tag,
  Tooltip,
} from 'antd';
import equipmentHandoverApi from 'api/equipment_handover.api';
import medicalEquipmentApi from 'api/medicalEquipment.api';
import equipmentApi from 'api/medicalEquipment.api';
import officeEquipmentApi from 'api/officeEquipment.api';
import DownloadTicketAttachmentsButton from 'components/DownloadTicketAttachmentsButton';
import ModalAcceptHandoverTicket from 'components/ModalAcceptHandoverTicket';
import ModalDetailHandoverTicket from 'components/ModalDetailHandoverTicket';
import ModalUpdateHandoverTicket from 'components/ModalUpdateHandoverTicket';
import { Authority } from 'constants/authority';
import i18n from 'i18n';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { EquipmentFullInfoDto, EquipmentStatus, EquipmentType } from 'types/equipment.type';
import { HandoverTicketFullInfoDto } from 'types/handover.type';
import {
  MedicalEquipmentDto,
  MedicalEquipmentFullInfoDto,
  
} from 'types/medicalEquipment.type';
import { TicketStatus } from 'types/reportBroken.type';
import {
  formatCurrencyVn,
  getDateForRendering,
  getIfNull,
  getStatusTag,
  getTicketStatusTag,
  hasAuthority,
} from 'utils/globalFunc.util';
export interface TicketListProps {
  type : EquipmentType
}
export interface AcceptHandoverTicketModalData {
  equipment?: EquipmentFullInfoDto;
}

export interface UpdateHandoverTicketModalData {
  equipment?: EquipmentFullInfoDto;
  handoverTicket?: HandoverTicketFullInfoDto;
}
export interface DetailHandoverTicketModalData {
  equipment?: EquipmentFullInfoDto;
  handoverTicket?: HandoverTicketFullInfoDto;
}

const HandoverTicketList = (props : TicketListProps) => {
  const { equipmentId } = useParams<{ equipmentId?: string }>();
  let navigate = useNavigate();
  const [handoverTicketList, setHandoverTicketList] =
    useState<HandoverTicketFullInfoDto[]>();
  const [equipment, setEquipment] = useState<MedicalEquipmentFullInfoDto>({});
  
  const [showAcceptHandoverTicketModal, setShowAcceptHandoverTicketModal] =
    useState<boolean>(false);
  const [showUpdateHandoverTicketModal, setShowUpdateHandoverTicketModal] =
    useState<boolean>(false);
  const [acceptHandoverTicketModalData, setAcceptHandoverTicketModalData] =
    useState<AcceptHandoverTicketModalData>({});
  const [updateHandoverTicketModalData, setUpdateHandoverTicketModalData] =
    useState<UpdateHandoverTicketModalData>({});

  const [showDetailHandoverTicketModal, setShowDetailHandoverTicketModal] =
    useState<boolean>(false);
  const [detailHandoverTicketModalData, setDetailHandoverTicketModalData] =
    useState<DetailHandoverTicketModalData>({});

  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };
  const showAcceptHandoverTicketModalAndRenderData = (
    item: EquipmentFullInfoDto
  ) => {
    setShowAcceptHandoverTicketModal(true);
    setAcceptHandoverTicketModalData({ equipment: item });
  };

  const showUpdateHandoverTicketModalAndRenderData = (
    item: EquipmentFullInfoDto,
    handoverTicket: HandoverTicketFullInfoDto
  ) => {
    setShowUpdateHandoverTicketModal(true);
    setUpdateHandoverTicketModalData({
      equipment: item,
      handoverTicket: handoverTicket,
    });
  };
  const showDetailHandoverTicketModalAndRenderData = (
    item: EquipmentFullInfoDto,
    handoverTicket: HandoverTicketFullInfoDto
  ) => {
    setShowDetailHandoverTicketModal(true);
    setDetailHandoverTicketModalData({
      equipment: item,
      handoverTicket: handoverTicket,
    });
  };
  useEffect(() => {
    if (props.type === EquipmentType.MEDICAL) {
      medicalEquipmentApi.getEquipmentById(Number(equipmentId)).then((res) => {
          if (res.data.success) {
            const handoverTicketList = res.data.data.handoverTickets
              ?.sort()
              .reverse();
              setHandoverTicketList(handoverTicketList);
            // console.log(res.data.data.reportBrokenTickets);
            setEquipment(res.data.data);
          }
    }) 
    } else {
      officeEquipmentApi.getEquipmentById(Number(equipmentId)).then((res) => {
        if (res.data.success) {
          const handoverTicketList = res.data.data.handoverTickets
            ?.sort()
            .reverse();
            setHandoverTicketList(handoverTicketList);
          // console.log(res.data.data.reportBrokenTickets);
          setEquipment(res.data.data);
        }
    }) 
    }
    // equipmentHandoverApi
    //   .getAllHandoverTicketsOfAnEquipment(Number(equipmentId))
    //   .then((res) => {
    //     if (res.data.success) {
    //       setHandoverTicketList(res.data.data);
    //       console.log(res.data.data);
    //     }
    //   });
    // equipmentApi.getEquipmentById(Number(equipmentId)).then((res) => {
    //   if (res.data.success) {
    //     const handoverTicketList = res.data.data.handoverTickets
    //       ?.sort()
    //       .reverse();
    //     setHandoverTicketList(handoverTicketList);
    //     // console.log(res.data.data.handoverTickets);
    //     setEquipment(res.data.data);
    //   }
    // });
  }, [equipmentId]);

  const items: DescriptionsProps['items'] = [
    {
      key: '10',
      label: 'Tên thiết bị',
      children: <p>{getIfNull(equipment?.name, '')}</p>,
    },
    {
      key: '1',
      label: 'Khoa - Phòng',
      children: <p>{getIfNull(equipment?.department?.name, '')}</p>,
    },
    {
      key: '2',
      label: 'Trạng thái',
      children: (
        <Tag color={getStatusTag(equipment?.status)}>
          {i18n.t(equipment?.status as string)}
        </Tag>
      ),
    },
    {
      key: '3',
      label: 'Model',
      children: <p>{equipment?.model}</p>,
    },
    {
      key: '4',
      label: 'Serial',
      children: <p>{equipment?.serial}</p>,
    },
    {
      key: '6',
      label: 'Loại thiết bị',
      children: <p>{equipment?.category?.name}</p>,
    },
    {
      key: '11',
      label: 'Hãng sản xuất',
      children: <p>{getIfNull(equipment?.manufacturer, '')}</p>,
    },
    {
      key: '12',
      label: 'Quốc gia',
      children: <p>{getIfNull(equipment?.manufacturingCountry, '')}</p>,
    },
  ];

  const columnsHandover: any[] = [
    {
      title: 'Mã phiếu',
      key: 'code',
      render: (item: HandoverTicketFullInfoDto) => <>{item.code}</>,
    },
    {
      title: 'Tên thiết bị',
      key: 'name',
      render: () => <>{equipment?.name}</>,
    },
    // {
    //   title: 'Ngày tạo phiếu',
    //   key: 'handoverDate',
    //   defaultSortOrder: 'descend',
    //   //   sorter: (a, b) => {},
    //   render: (item: HandoverTicketFullInfoDto) => (
    //     <>{getDateForRendering(item?.createdDate as string)}</>
    //   ),
    // },
    // {
    //   title: 'Người tạo phiếu',
    //   key: 'name',
    //   render: (item: HandoverTicketFullInfoDto) => <>{item.creator?.name}</>,
    // },
    // {
    //   title: 'Ghi chú của người tạo phiếu',
    //   key: 'name',
    //   render: (item: HandoverTicketFullInfoDto) => <>{item.creatorNote}</>,
    // },
    {
      title: 'Người phụ trách',
      key: 'handoverInCharge',
      render: (item: HandoverTicketFullInfoDto) => (
        <>{item?.responsiblePerson?.name}</>
      ),
    },
    {
      title: 'Khoa Phòng nhận bàn giao',
      key: 'department',
      render: (item: HandoverTicketFullInfoDto) => (
        <>{item?.department?.name}</>
      ),
    },
    {
      title: ' Ngày bàn giao',
      key: 'name',
      render: (item: HandoverTicketFullInfoDto) => (
        <>{getDateForRendering(item.handoverDate as string)}</>
      ),
    },
    // {
    //   title: 'Người  phê duyệt',
    //   key: 'name',
    //   render: (item: HandoverTicketFullInfoDto) => <>{item.approver?.name}</>,
    // },
    // {
    //   title: ' Ngày duyệt',
    //   key: 'name',
    //   render: (item: HandoverTicketFullInfoDto) => (
    //     <>{getDateForRendering(item.approvalDate as string)}</>
    //   ),
    // },
    // {
    //   title: ' Ghi chú của người duyệt',
    //   key: 'name',
    //   render: (item: HandoverTicketFullInfoDto) => <>{item.creatorNote}</>,
    // },
    {
      title: ' Trạng thái duyệt',
      key: 'name',
      render: (item: HandoverTicketFullInfoDto) => (
        <>
          <Tag color={getTicketStatusTag(item?.status)}>
            {i18n.t(item?.status as string)}
          </Tag>{' '}
        </>
        // <>{i18n.t(item?.status as string)}</>
      ),
    },
    {
      title: 'Thao tác',
      key: 'action',
      render: (item: HandoverTicketFullInfoDto) => (
        <>
          <Menu className="flex flex-row items-center">
            {item.status === TicketStatus.PENDING &&
              hasAuthority(Authority.MEDICAL_HANDOVER_ACCEPT) && (
                <Menu.Item key="accept">
                  <Tooltip title=" Phê duyệt phiếu yêu cầu bàn giao">
                    <CheckCircleOutlined
                      onClick={(event) => {
                        showAcceptHandoverTicketModalAndRenderData(equipment);
                      }}
                    />
                  </Tooltip>
                </Menu.Item>
              )}
            {item.status === TicketStatus.PENDING &&
              hasAuthority(Authority.MEDICAL_HANDOVER_CREATE) && (
                <Menu.Item key="edit">
                  <Tooltip title=" Cập nhật phiếu yêu cầu bàn giao">
                    <EditTwoTone
                      twoToneColor="#52c41a"
                      onClick={(event) => {
                        showUpdateHandoverTicketModalAndRenderData(
                          equipment,
                          item
                        );
                      }}
                    />
                  </Tooltip>
                </Menu.Item>
              )}
            <DownloadTicketAttachmentsButton
              isInEquipmentDetail={true}
              ticket={item}
            />
            <Menu.Item key='detail'>
              <Tooltip title=" Chi tiết phiếu yêu cầu bàn giao">
                <EyeOutlined
                  twoToneColor="#52c41a"
                  onClick={(event) => {
                    showDetailHandoverTicketModalAndRenderData(equipment, item);
                  }}
                />
              </Tooltip>
            </Menu.Item>
          </Menu>
        </>
      ),
    },
  ];

  return (
    <>
      <div>
        <div className="title text-center">LỊCH SỬ BÀN GIAO THIẾT BỊ</div>
        <Divider></Divider>

        <Card>
          <div className="basis-2/3">
            <Descriptions items={items} column={3}></Descriptions>
          </div>
          <div className="mb-10">
            <Divider></Divider>
            <div className="font-bold text-xl mb-6">Thông tin bàn giao</div>

            <Table
            rowKey="id"
              // loading={loading}
              columns={columnsHandover}
              dataSource={equipment?.handoverTickets}
              // pagination={true}
              className="table-responsive"
            />
            <ModalAcceptHandoverTicket
              showAcceptHandoverTicketModal={showAcceptHandoverTicketModal}
              hideAcceptHandoverTicketModal={() =>
                setShowAcceptHandoverTicketModal(false)
              }
              callback={() => {
                // increaseCount();
                // getAllNotifications();
                // onChangeQueryParams('', '');
              }}
              acceptHandoverTicketModalData={acceptHandoverTicketModalData}
            />

            <ModalUpdateHandoverTicket
              showUpdateHandoverTicketModal={showUpdateHandoverTicketModal}
              hideUpdateHandoverTicketModal={() =>
                setShowUpdateHandoverTicketModal(false)
              }
              callback={() => {
                // increaseCount();
                // getAllNotifications();
                // onChangeQueryParams('', '');
              }}
              updateHandoverTicketModalData={updateHandoverTicketModalData}
            />
            <ModalDetailHandoverTicket
              showDetailHandoverTicketModal={showDetailHandoverTicketModal}
              hideDetailHandoverTicketModal={() =>
                setShowDetailHandoverTicketModal(false)
              }
              callback={() => {
                // increaseCount();
                // getAllNotifications();
                // onChangeQueryParams('', '');
              }}
              detailHandoverTicketModalData={detailHandoverTicketModalData}
            />
          </div>
        </Card>
      </div>
    </>
  );
};

export default HandoverTicketList;
