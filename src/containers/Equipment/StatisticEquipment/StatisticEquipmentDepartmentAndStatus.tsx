import { Column, Pie } from '@ant-design/plots';
import { Card, Divider, Select, Space, Table, Tabs, Tag } from 'antd';
import equipmentApi from 'api/medicalEquipment.api';
import inUse from 'assets/active.png';
import repairing from 'assets/repairing.png';
import inactive from 'assets/inactive.png';
import liquidated from 'assets/liquidated.png';
import news from 'assets/news.png';
import broken from 'assets/broken.png';
import { Key, useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { CountEquipmentByDepartment, CountEquipmentByDepartmentAndStatus, CountEquipmentByDepartmentAndStatusChart, CountEquipmentByDepartmentAndStatusTable, CountEquipmentByRiskLevel, CountEquipmentByStatus, StatisticDashboard } from 'types/statistics.type';
// import './index.css';
import { EquipmentStatus } from 'types/equipment.type';
import { useTranslation } from 'react-i18next';
import { TableFooter } from 'components/TableFooter';
import { current } from '@reduxjs/toolkit';
import useQuery from 'hooks/useQuery';
import { getDepartmentOptions } from 'utils/globalFunc.util';
import { FilterContext } from 'contexts/filter.context';
import { sortBy } from 'lodash';
import { ColumnType } from 'antd/lib/table';
import ExportToExcel, { HeaderProps } from 'components/Excel';
import AppleOutlined, { PieChartOutlined, UnorderedListOutlined } from '@ant-design/icons';
import AndroidOutlined from '@ant-design/icons';
import { tab } from '@testing-library/user-event/dist/tab';

const { Meta } = Card;

const StatisticEquipmentDepartmentAndStatus = () => {

  const [tableData, setTableData] = useState<CountEquipmentByDepartmentAndStatusTable[]>([]);
  const [chartData, setChartData] = useState<CountEquipmentByDepartmentAndStatusChart[]>([]);
  const [countEquipmentByStatuses, setCountEquipmentByStatuses] = useState<CountEquipmentByStatus[]>([]);

  const { t } = useTranslation();

  /////
  const query = useQuery();
  // const currentDepartment = query?.departmentId;
  const [department, setDepartment] = useState<any>(undefined);
  const { departments, equipmentCategories, medicalEquipmentGroups } = useContext(FilterContext);
  const navigate = useNavigate();

  /////

  const mapToChartFormat = (data : CountEquipmentByDepartmentAndStatus[]) : CountEquipmentByDepartmentAndStatusChart[] => {
    let result : CountEquipmentByDepartmentAndStatusChart[] = [];
    data.map(item => {
      item.countByEquipmentStatus.forEach((value) => {
        if (value.status === EquipmentStatus.PENDING_ACCEPT_INSPECTION || 
          value.status === EquipmentStatus.PENDING_ACCEPT_LIQUIDATION || 
          value.status === EquipmentStatus.PENDING_ACCEPT_MAINTENANCE|| 
          value.status === EquipmentStatus.PENDING_ACCEPT_REPAIR || 
          value.status === EquipmentStatus.PENDING_HANDOVER || 
          value.status === EquipmentStatus.PENDING_TRANSFER || 
          value.status === EquipmentStatus.PENDING_REPORT_BROKEN ||
          value.status === EquipmentStatus.PENDING_ACCEPTANCE_TESTING
        ) return;
        const transformedItem : CountEquipmentByDepartmentAndStatusChart = {
          departmentId: item.departmentId,
          departmentName: item.departmentName,
          count: item.count,
          status: t(value.status),
          countStatus: value.count
        }
        result.push(transformedItem);
      })
    })
    return result;
  }

  const mapToTableFormat = (data: CountEquipmentByDepartmentAndStatus[]): CountEquipmentByDepartmentAndStatusTable[] => {
    return data.map(item => {
      const transformedItem: CountEquipmentByDepartmentAndStatusTable = {
        departmentId: item.departmentId,
        departmentName: item.departmentName,
        count: item.count,
      };
  
      item.countByEquipmentStatus.forEach((value) => {
        transformedItem[value.status] = value.count;
      });
  
      // If there are missing risk levels, populate them with count 0
      // Object.values(RiskLevel).forEach(level => {
      //   if (!(level in transformedItem)) {
      //     transformedItem[level] = 0;
      //   }
      // });
  
      return transformedItem;
    });
  };



  const count = (departmentId : number) => {
    equipmentApi.statisticEquipmentByDepartmentAndStatus().then((res) => {
      if (res.data.success) {
        const data: CountEquipmentByDepartmentAndStatus[] = res.data.data;
        // console.log(data);

        const tableData = mapToTableFormat(data);
        const chartData = mapToChartFormat(data);


        const rootTableData : CountEquipmentByDepartmentAndStatusTable[] = tableData;
        setTableData(tableData);
        const rootChartData : CountEquipmentByDepartmentAndStatusChart[] = chartData;
        setChartData(chartData);

        if (departmentId !== undefined) {
          const filterByDepartmentTable : CountEquipmentByDepartmentAndStatusTable[] = rootTableData.filter(item => item.departmentId === departmentId);
          setTableData(filterByDepartmentTable);
          const filterByDepartmentChart : CountEquipmentByDepartmentAndStatusChart[] = rootChartData.filter(item => item.departmentId === departmentId);
          setChartData(filterByDepartmentChart);
        }

      }
    });
    equipmentApi.statisticMedicalDashboard().then((res) => {
      if (res.data.success) {
        const data: StatisticDashboard = res.data.data;
        data.countByEquipmentStatuses.map((item) => {
          item.status = t(item.status);
        })
        setCountEquipmentByStatuses(data.countByEquipmentStatuses);
      }
    })
  };


  useEffect(() => {
    count(department);
    // console.log("hello", countEquipmentByDepartmentAndStatus);
  }, [department]);

  
  const handleCellClick = (record : any, key : any) => {
    let status;
    switch(key) {
      case 'new':
        status = EquipmentStatus.NEW;
        break;
      case 'inUse':
        status = EquipmentStatus.IN_USE;
        break;
      case 'broken':
          status = EquipmentStatus.BROKEN;
          break;
      case 'repairing':
          status = EquipmentStatus.REPAIRING;
          break;  
      case 'inactive':
          status = EquipmentStatus.INACTIVE;
          break;  
      case 'liquidated':
          status = EquipmentStatus.LIQUIDATED;
          break; 
      case 'underInspection':
          status = EquipmentStatus.UNDER_INSPECTION
          break;    
      case 'underMaintenance':
            status = EquipmentStatus.UNDER_MAINTENANCE
            break;
      default: status = null;
    }
    if (status == null) {
      navigate(`/statistic/medical-equipments?departmentId=${record.departmentId}`);
    } else {
      navigate(`/statistic/medical-equipments?departmentId=${record.departmentId}&status=${status}`);
    }
  }
  /////
  let columns: any = [
    // {
    //   title: 'Khoa - Phòng', key: 'room', show: true, render: (item: CountEquipmentByDepartment) => (<div>{item?.departmentName}</div>),
    // }, {
    //   title: 'Tổng số thiết bị', key: 'totalNumbersOfEquipments', show: true, render: (item: CountEquipmentByDepartment) => (<div>{item?.count}</div>),
    // }, 
    // {
    //   title: 'Khoa - Phòng', key: 'room', show: true, render: (item: CountEquipmentByDepartment) => (<div>{item?.departmentName}</div>),
    // },
    {
      title: 'Khoa - Phòng', key: 'room', show: true, dataIndex: 'departmentName',
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'room')
        }
      }
    },
    
    {
      title: 'Mới', key: 'new', show: true, dataIndex: 'NEW', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.NEW - b.NEW,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'new')
        }
      }
    },
    {
      title: 'Đang sử dụng', key: 'inUse', show: true, dataIndex: 'IN_USE', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.IN_USE - b.IN_USE,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'inUse')
        }
      }
    },
    {
      title: 'Hỏng', key: 'broken', show: true, dataIndex: 'BROKEN', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.BROKEN - b.BROKEN,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'broken')
        }
      }
    },
    {
      title: 'Đang sửa chữa', key: 'repairing', show: true, dataIndex: 'REPAIRING', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.REPAIRING - b.REPAIRING,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'repairing')
        }
      }
    },
    {
      title: 'Đang bảo dưỡng', key: 'underMaintenance', show: true, dataIndex: 'UNDER_MAINTENANCE', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.UNDER_MAINTENANCE - b.UNDER_MAINTENANCE,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'underMaintenance')
        }
      }
    },
    {
      title: 'Đang kiểm định', key: 'underInspection', show: true, dataIndex: 'UNDER_INSPECTION', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.UNDER_INSPECTION - b.UNDER_INSPECTION,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'underInspection')
        }
      }
    },
    {
      title: 'Ngừng sử dụng', key: 'inactive', show: true, dataIndex: 'INACTIVE', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.INACTIVE - b.INACTIVE,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'inactive')
        }
      }
    },
    {
      title: 'Đã thanh lý', key: 'liquidated', show: true, dataIndex: 'LIQUIDATED', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.LIQUIDATED - b.LIQUIDATED,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'liquidated')
        }
      }
    },
    
    {
      title: 'Tổng số thiết bị', key: 'count', show: true, dataIndex: 'count', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.count - b.count,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'room')
        }
      }
    }, 
  ];
  
let nestedColumn : any = [
  , 
  {
   title: 'Trạng thái', key: 'status', show: true, render: (item: CountEquipmentByStatus) => (<div>{t(item.status)}</div>)
 },
 {
   title: 'Số lượng', key: 'count', show: true, render: (item: CountEquipmentByStatus) => (<div>{item.count}</div>)

 },
]

  const onChangeSelect = (key: string, value: any) => {
    if (key === 'departmentId') {
      setDepartment(value);
    }
  }; 
  /////


  const headerName : HeaderProps[] = [
    {
      title: 'STT', key: 'number'
    },
    {
      title: 'Khoa - Phòng', key: 'departmentName'
    },
    {
      title: 'Mới', key: 'NEW'
    },
    {
      title: 'Đang sử dụng', key: 'IN_USE'
    },
    {
      title: 'Hỏng', key: 'BROKEN'
    },
    {
      title: 'Đang sửa chữa', key: 'REPAIRING'
    },
    {
      title: 'Đang bảo dưỡng', key: 'UNDER_MAINTENANCE'
    },
    {
      title: 'Đang kiểm định', key: 'UNDER_INSPECTION'
    },
    {
      title: 'Ngừng sử dụng', key: 'INACTIVE'
    },
    {
      title: 'Đã thanh lý', key: 'LIQUIDATED'
    },
    
    {
      title: 'Tổng số thiết bị', key: 'count'
    },
    // 'STT', 'Khoa - Phòng', 'Mức độ A', 'Mức độ B', 'Mức độ C', 'Mức độ D',  'Tổng số thiết bị'
  ]  
  


  const config : any = {
    data : countEquipmentByStatuses,
    appendPadding: 10, 
    angleField: 'count', 
    colorField: 'status',
    radius: 1,
    innerRadius: 0.6,
    label: {
      type: 'inner',
      offset: '-50%',
      // content: '{count}',
      style: {
        textAlign: 'center',
        fontSize: 14,
      },
    },
    interactions: [
      {
        type: 'element-selected',
      },
      {
        type: 'element-active',
      },
    ],
    statistic: {
      title: false,
      content: {
        style: {
          whiteSpace: 'pre-wrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
        // content: 'Nhóm',
      },
    },
  }

  const configForDepartment : any = {
    data : chartData,
    appendPadding: 10, 
    angleField: 'countStatus', 
    colorField: 'status', 
    radius: 1,
    innerRadius: 0.6,
    label: {
      type: 'inner',
      offset: '-50%',
      // content: '{count}',
      style: {
        textAlign: 'center',
        fontSize: 14,
      },
    },
    interactions: [
      {
        type: 'element-selected',
      },
      {
        type: 'element-active',
      },
    ],
    statistic: {
      title: false,
      content: {
        style: {
          whiteSpace: 'pre-wrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
        // content: 'Nhóm',
      },
    },
  }

  return (<>

    <div className='title text-center'>Thống kê theo khoa và trạng thái</div>
    <Divider />
    <Card>


    <Tabs size='large'>
    <Tabs.TabPane tab={
        <span><PieChartOutlined style={{display: 'inline-flex'}} />Biểu đồ</span>
      } key="tab2">
      <div className='flex flex-between-center'>
      <Select
          style={{width: 300, marginBottom: 20}}
          showSearch
          placeholder='Khoa - Phòng'
          optionFilterProp='children'
          onChange={(value: any) => onChangeSelect('departmentId', value)}
          // onSearch={onSearch}
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={getDepartmentOptions(departments)}
          value={department}
        />
      </div>
       
      <div>
      <Card title='Biểu đồ thống kê' hoverable>
        {department == undefined ? <Pie
            {...config}
          /> : <Pie
          {...configForDepartment}
        />}
        </Card>
      </div>
      
      </Tabs.TabPane>
      <Tabs.TabPane tab={
        <span><UnorderedListOutlined style={{display: 'inline-flex'}} /> Danh sách</span>
      } key="1" >
        <div>
    <div className='flex flex-between-center'>
      <Select
          style={{width: 300}}
          showSearch
          placeholder='Khoa - Phòng'
          optionFilterProp='children'
          onChange={(value: any) => onChangeSelect('departmentId', value)}
          // onSearch={onSearch}
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={getDepartmentOptions(departments)}
          value={department}
        />
      <ExportToExcel 
        data={tableData} 
        fileName={'Thống kê thiết bị theo khoa và trạng thái'} 
        headerName={headerName} 
        sheetName={''} 
        title={'Thống kê số lượng thiết bị theo khoa và trạng thái'}
      ></ExportToExcel>
    </div>
      <Table
      rowKey='id'
      columns={columns}
      dataSource={tableData}
      // rowKey={(record) => record.departmentName}
      // expandable={{
      //   expandedRowRender: (record) => {
      //     return <Table columns={nestedColumn} dataSource={record.countByEquipmentStatus} pagination={false}
      //     footer={() => (<><Space></Space></>)}
      //     >
      //     </Table>
      //   },
      //   rowExpandable: (record) => true,
      // }}
      className='mt-6 shadow-md ant-table-column'
      footer={() => (<>
        {/* <TableFooter paginationProps={pagination} /> */}
      </>)}
      // loading={loading}
      />
    </div>
      </Tabs.TabPane>
    

    </Tabs>  
    </Card>
  </>);
};

export default StatisticEquipmentDepartmentAndStatus;
