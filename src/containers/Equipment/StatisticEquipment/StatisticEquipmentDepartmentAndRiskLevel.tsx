import { Column, Pie } from '@ant-design/plots';
import { Card, Divider, Select, Space, Table, Tabs, Tag } from 'antd';
import equipmentApi from 'api/medicalEquipment.api';
import inUse from 'assets/active.png';
import repairing from 'assets/repairing.png';
import inactive from 'assets/inactive.png';
import liquidated from 'assets/liquidated.png';
import news from 'assets/news.png';
import broken from 'assets/broken.png';
import { Key, useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { CountEquipmentByDepartment, CountEquipmentByDepartmentAndRiskLevel, CountEquipmentByDepartmentAndRiskLevelChart, CountEquipmentByDepartmentAndRiskLevelTable, CountEquipmentByRiskLevel, CountEquipmentByStatus, StatisticDashboard } from 'types/statistics.type';
// import './index.css';
import { EquipmentStatus, RiskLevel } from 'types/equipment.type';
import { useTranslation } from 'react-i18next';
import { TableFooter } from 'components/TableFooter';
import { current } from '@reduxjs/toolkit';
import useQuery from 'hooks/useQuery';
import { getDepartmentOptions } from 'utils/globalFunc.util';
import { FilterContext } from 'contexts/filter.context';
import { sortBy } from 'lodash';
import { ColumnType } from 'antd/lib/table';
import ExportToExcel, { HeaderProps } from 'components/Excel';
import { Tab } from 'docx';
import { AndroidOutlined, AppleOutlined, PieChartOutlined, TableOutlined, UnorderedListOutlined } from '@ant-design/icons';

const { Meta } = Card;

const StatisticEquipmentDepartmentAndRiskLevel = () => {

  const [tableData, setTableData] = useState<CountEquipmentByDepartmentAndRiskLevelTable[]>([]);
  const [chartData, setChartData] = useState<CountEquipmentByDepartmentAndRiskLevelChart[]>([])
  const { t } = useTranslation();
  const navigate = useNavigate();

  /////
  const query = useQuery();
  // const currentDepartment = query?.departmentId;
  const [department, setDepartment] = useState<any>(undefined);
  const { departments, equipmentCategories, medicalEquipmentGroups } = useContext(FilterContext);

  /////

  /////

  const mapToChartFormat = (data : CountEquipmentByDepartmentAndRiskLevel[]) : CountEquipmentByDepartmentAndRiskLevelChart[] => {
    let result : CountEquipmentByDepartmentAndRiskLevelChart[] = [];
    data.map(item => {
      item.countByRiskLevel.forEach((value) => {
        const transformedItem : CountEquipmentByDepartmentAndRiskLevelChart = {
          departmentId: item.departmentId,
          departmentName: item.departmentName,
          count: item.count,
          riskLevel: value.riskLevel,
          countRiskLevel: value.count
        }
        result.push(transformedItem);
      })
    })
    return result;
  }


  const mapToTableFormat = (data: CountEquipmentByDepartmentAndRiskLevel[]): CountEquipmentByDepartmentAndRiskLevelTable[] => {
    return data.map(item => {
      const transformedItem: CountEquipmentByDepartmentAndRiskLevelTable = {
        departmentId: item.departmentId,
        departmentName: item.departmentName,
        count: item.count,
      };
  
      item.countByRiskLevel.forEach((value) => {
        transformedItem[value.riskLevel] = value.count;
      });
  
      // If there are missing risk levels, populate them with count 0
      // Object.values(RiskLevel).forEach(level => {
      //   if (!(level in transformedItem)) {
      //     transformedItem[level] = 0;
      //   }
      // });
  
      return transformedItem;
    });
  };

  /////


  const count = (departmentId : number) => {
    equipmentApi.statisticEquipmentByDepartmentAndRiskLevel().then((res) => {
      if (res.data.success) {
        const data: CountEquipmentByDepartmentAndRiskLevel[] = res.data.data;
        // console.log(data);
        ///
        const tableData: CountEquipmentByDepartmentAndRiskLevelTable[] = mapToTableFormat(data);
        const chartData: CountEquipmentByDepartmentAndRiskLevelChart[] = mapToChartFormat(data);
        // console.log("chart", chartData);
        ///
        const rootTableData : CountEquipmentByDepartmentAndRiskLevelTable[] = tableData;
        setTableData(tableData);

        const rootChartData : CountEquipmentByDepartmentAndRiskLevelChart[] = chartData;
        setChartData(chartData);

        if (departmentId !== undefined) {
          const filterByDepartmentTable : CountEquipmentByDepartmentAndRiskLevelTable[] = rootTableData.filter(item => item.departmentId === departmentId);
          setTableData(filterByDepartmentTable);
          const filterByDepartmentChart : CountEquipmentByDepartmentAndRiskLevelChart[] = rootChartData.filter(item => item.departmentId === departmentId);
          setChartData(filterByDepartmentChart);
        }

      }
       
    
    });
  };


  useEffect(() => {
    count(department);
    // console.log("hello", countEquipmentByDepartmentAndRiskLevel);
  }, [department]);

  const handleCellClick = (record : any, key : any) => {
    let riskLevel;
    switch(key) {
      case 'A':
        riskLevel = RiskLevel.A;
        break;
      case 'B':
        riskLevel = RiskLevel.B;
        break;
      case 'C':
        riskLevel = RiskLevel.C;
          break;
      case 'D':
        riskLevel = RiskLevel.D;
          break;    
      default: riskLevel = null;
    }
    if (riskLevel == null) {
      navigate(`/statistic/medical-equipments?departmentId=${record.departmentId}`);
    } else {
      navigate(`/statistic/medical-equipments?departmentId=${record.departmentId}&riskLevel=${riskLevel}`);
    }
  }
  /////
  let columns: any = [
    // {
    //   title: 'Khoa - Phòng', key: 'room', show: true, render: (item: CountEquipmentByDepartment) => (<div>{item?.departmentName}</div>),
    // }, {
    //   title: 'Tổng số thiết bị', key: 'totalNumbersOfEquipments', show: true, render: (item: CountEquipmentByDepartment) => (<div>{item?.count}</div>),
    // }, 
    // {
    //   title: 'Khoa - Phòng', key: 'room', show: true, render: (item: CountEquipmentByDepartment) => (<div>{item?.departmentName}</div>),
    // },
    {
      title: 'Khoa - Phòng', key: 'room', show: true, dataIndex: 'departmentName',
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'room')
        }
      }
    },
    
    {
      title: 'Mức độ A', key: 'A', show: true, dataIndex: 'A', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.A - b.A,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'A')
        }
      }
    },

    {
      title: 'Mức độ B', key: 'B', show: true, dataIndex: 'B', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.B - b.B,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'B')
        }
      }
    },
    {
      title: 'Mức độ C', key: 'C', show: true, dataIndex: 'C', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.C - b.C,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'C')
        }
      }
    },
    {
      title: 'Mức độ D', key: 'D', show: true, dataIndex: 'D', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.D - b.D,
      },
      onCell: (record: any) => {
        return {
            onClick: () => handleCellClick(record, 'D')
        }
      }
    },
    
    {
      title: 'Tổng số thiết bị', key: 'count', show: true, dataIndex: 'count', align: 'right',
      sorter: {
        compare: (a : any, b : any) => a.count - b.count,
      }
    }, 
  ];
  
let nestedColumn : any = [
  , 
  {
   title: 'Trạng thái', key: 'status', show: true, render: (item: CountEquipmentByStatus) => (<div>{t(item.status)}</div>)
 },
 {
   title: 'Số lượng', key: 'count', show: true, render: (item: CountEquipmentByStatus) => (<div>{item.count}</div>)

 },
]

  const onChangeSelect = (key: string, value: any) => {
    if (key === 'departmentId') {
      setDepartment(value);
    }
  }; 
  /////

// config header key with corresponding key of data
  const headerName : HeaderProps[] = [
    {
      title: 'STT', key: 'number'
    },
    {
      title: 'Khoa - Phòng', key: 'departmentName'
    },
    {
      title: 'Mức độ A', key: 'A'
    },
    {
      title: 'Mức độ B', key: 'B'
    },
    {
      title: 'Mức độ C', key: 'C'
    },
    {
      title: 'Mức độ D', key: 'D'
    },
    {
      title: 'Tổng số thiết bị', key: 'count'
    },
    // 'STT', 'Khoa - Phòng', 'Mức độ A', 'Mức độ B', 'Mức độ C', 'Mức độ D',  'Tổng số thiết bị'
  ]

  const config : any = {
    data : chartData,
    isGroup: true,
    xField: 'departmentName',
    yField: 'countRiskLevel',
    seriesField: 'riskLevel',
    xAxis: {
      label: {
        autoHide: false, autoRotate: true,
      },
    }, 
  }

  return (<>
  
    <div className='title text-center'>Thống kê theo khoa và mức độ rủi ro</div>
    <Divider />
    <Card>
    <div>
    
    <div>
    <Tabs size='large'>
    <Tabs.TabPane tab={
        <span><PieChartOutlined style={{display: 'inline-flex'}} />Biểu đồ</span>
      } key="tab2">
      <div className='flex flex-between-center'>
      <Select
          style={{width: 300, marginBottom: 20}}
          showSearch
          placeholder='Khoa - Phòng'
          optionFilterProp='children'
          onChange={(value: any) => onChangeSelect('departmentId', value)}
          // onSearch={onSearch}
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={getDepartmentOptions(departments)}
          value={department}
        />
      </div>
       
      <div>
      <Card title='Biểu đồ thống kê' hoverable>
          <Column
            {...config}
            onReady={(plot) => {
              plot.on('plot:click', (evt: any) => {
                const data = evt?.data?.data as CountEquipmentByDepartment;
                // navigate(`/equipments?departmentId=${data.departmentId}`);
              });
            }}
          />
        </Card>
      </div>
      
      </Tabs.TabPane>
      <Tabs.TabPane tab={
        <span><UnorderedListOutlined style={{display: 'inline-flex'}} /> Danh sách</span>
      } key="1">
      <div className='flex flex-between-center'>
      <Select
          style={{width: 300}}
          showSearch
          placeholder='Khoa - Phòng'
          optionFilterProp='children'
          onChange={(value: any) => onChangeSelect('departmentId', value)}
          // onSearch={onSearch}
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={getDepartmentOptions(departments)}
          value={department}
        />
      <ExportToExcel 
        data={tableData} 
        fileName={'Thống kê thiết bị theo khoa và mức độ rủi ro'} 
        headerName={headerName} 
        sheetName={''} 
        title={'Thống kê số lượng thiết bị theo khoa và mức độ rủi ro'}
      ></ExportToExcel>
    </div>
        <Table
          rowKey='id'
          columns={columns}
          dataSource={tableData}
          // rowKey={(record) => record.departmentName}
          // expandable={{
          //   expandedRowRender: (record) => {
          //     return <Table columns={nestedColumn} dataSource={record.countByEquipmentStatus} pagination={false}
          //     footer={() => (<><Space></Space></>)}
          //     >
          //     </Table>
          //   },
          //   rowExpandable: (record) => true,
          // }}
          className='mt-6 shadow-md ant-table-column'
          footer={() => (<>
            {/* <TableFooter paginationProps={pagination} /> */}
          </>)}
          // loading={loading}
          />
      </Tabs.TabPane>

  
    </Tabs>
    </div>
      
    </div>
    </Card>
    
  </>);
};

export default StatisticEquipmentDepartmentAndRiskLevel;
