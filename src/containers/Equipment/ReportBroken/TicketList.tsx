import { CheckCircleOutlined, EditTwoTone, EyeOutlined } from '@ant-design/icons';
import {
  Card,
  Descriptions,
  DescriptionsProps,
  Divider,
  Drawer,
  Menu,
  Table,
  Tag,
  Tooltip,
} from 'antd';
import medicalEquipmentApi from 'api/medicalEquipment.api';
import officeEquipmentApi from 'api/officeEquipment.api';
import DownloadTicketAttachmentsButton from 'components/DownloadTicketAttachmentsButton';
import ModalAcceptReportBrokenTicket from 'components/ModalAcceptReportBrokenTicket';
import ModalDetailReportBrokenTicket from 'components/ModalDetailReportBrokenTicket';
import ModalUpdateReportBrokenTicket from 'components/ModalUpdateBrokenTicket';
import { Authority } from 'constants/authority';
import i18n from 'i18n';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { EquipmentFullInfoDto, EquipmentType } from 'types/equipment.type';
import {
  MedicalEquipmentDto,
  MedicalEquipmentFullInfoDto,
  
} from 'types/medicalEquipment.type';
import { ReportBrokenTicketFullInfoDto, TicketStatus } from 'types/reportBroken.type';
import {
  formatCurrencyVn,
  getDateForRendering,
  getIfNull,
  getStatusTag,
  getTicketStatusTag,
  hasAuthority,
} from 'utils/globalFunc.util';

export interface TicketListProps {
  type : EquipmentType
}

export interface AcceptReportBrokenTicketModalData {
  equipment?: EquipmentFullInfoDto;
}

export interface UpdateReportBrokenTicketModalData {
  equipment?: EquipmentFullInfoDto;
  reportBrokenTicket?: ReportBrokenTicketFullInfoDto;
}
export interface DetailReportBrokenTicketModalData {
  equipment?: EquipmentFullInfoDto;
  reportBrokenTicket?: ReportBrokenTicketFullInfoDto;
}

const ReportBrokenTicketList = (props : TicketListProps) => {
  const { equipmentId } = useParams<{ equipmentId?: string }>();
  let navigate = useNavigate();
  const [reportBrokenTicketList, setReportBrokenTicketList] =
    useState<ReportBrokenTicketFullInfoDto[]>();
  const [equipment, setEquipment] = useState<EquipmentFullInfoDto>({});
  
  const [showAcceptReportBrokenTicketModal, setShowAcceptReportBrokenTicketModal] =
    useState<boolean>(false);
  const [showUpdateReportBrokenTicketModal, setShowUpdateReportBrokenTicketModal] =
    useState<boolean>(false);
  const [acceptReportBrokenTicketModalData, setAcceptReportBrokenTicketModalData] =
    useState<AcceptReportBrokenTicketModalData>({});
  const [updateReportBrokenTicketModalData, setUpdateReportBrokenTicketModalData] =
    useState<UpdateReportBrokenTicketModalData>({});

  const [showDetailReportBrokenTicketModal, setShowDetailReportBrokenTicketModal] =
    useState<boolean>(false);
  const [detailReportBrokenTicketModalData, setDetailReportBrokenTicketModalData] =
    useState<DetailReportBrokenTicketModalData>({});

  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };
  const showAcceptReportBrokenTicketModalAndRenderData = (
    item: EquipmentFullInfoDto
  ) => {
    setShowAcceptReportBrokenTicketModal(true);
    setAcceptReportBrokenTicketModalData({ equipment: item });
  };

  const showUpdateReportBrokenTicketModalAndRenderData = (
    item: EquipmentFullInfoDto,
    reportBrokenTicket: ReportBrokenTicketFullInfoDto
  ) => {
    setShowUpdateReportBrokenTicketModal(true);
    setUpdateReportBrokenTicketModalData({
      equipment: item,
      reportBrokenTicket: reportBrokenTicket,
    });
  };
  const showDetailReportBrokenTicketModalAndRenderData = (
    item: EquipmentFullInfoDto,
    reportBrokenTicket: ReportBrokenTicketFullInfoDto
  ) => {
    setShowDetailReportBrokenTicketModal(true);
    setDetailReportBrokenTicketModalData({
      equipment: item,
      reportBrokenTicket: reportBrokenTicket,
    });
  };
  useEffect(() => {
    if (props.type === EquipmentType.MEDICAL) {
      medicalEquipmentApi.getEquipmentById(Number(equipmentId)).then((res) => {
          if (res.data.success) {
            const reportBrokenTicketList = res.data.data.reportBrokenTickets
              ?.sort()
              .reverse();
            setReportBrokenTicketList(reportBrokenTicketList);
            // console.log(res.data.data.reportBrokenTickets);
            setEquipment(res.data.data);
          }
    }) 
    } else {
      officeEquipmentApi.getEquipmentById(Number(equipmentId)).then((res) => {
        if (res.data.success) {
          const reportBrokenTicketList = res.data.data.reportBrokenTickets
            ?.sort()
            .reverse();
          setReportBrokenTicketList(reportBrokenTicketList);
          // console.log(res.data.data.reportBrokenTickets);
          setEquipment(res.data.data);
        }
    }) 
    }

    // equipmentReportBrokenApi
    //   .getAllReportBrokenTicketsOfAnEquipment(Number(equipmentId))
    //   .then((res) => {
    //     if (res.data.success) {
    //       setReportBrokenTicketList(res.data.data);
    //       // console.log(res.data.data);
    //     }
    //   });
    // equipmentApi.getEquipmentById(Number(equipmentId)).then((res) => {
    //   if (res.data.success) {
    //     const reportBrokenTicketList = res.data.data.reportBrokenTickets
    //       ?.sort()
    //       .reverse();
    //     setReportBrokenTicketList(reportBrokenTicketList);
    //     // console.log(res.data.data.reportBrokenTickets);
    //     setEquipment(res.data.data);
    //   }
    // });
  }, [equipmentId]);

  const items: DescriptionsProps['items'] = [
    {
      key: '10',
      label: 'Tên thiết bị',
      children: <p>{getIfNull(equipment?.name, '')}</p>,
    },
    {
      key: '1',
      label: 'Khoa - Phòng',
      children: <p>{getIfNull(equipment?.department?.name, '')}</p>,
    },
    {
      key: '2',
      label: 'Trạng thái',
      children: (
        <Tag color={getStatusTag(equipment?.status)}>
          {i18n.t(equipment?.status as string)}
        </Tag>
      ),
    },
    {
      key: '3',
      label: 'Model',
      children: <p>{equipment?.model}</p>,
    },
    {
      key: '4',
      label: 'Serial',
      children: <p>{equipment?.serial}</p>,
    },
    {
      key: '6',
      label: 'Loại thiết bị',
      children: <p>{equipment?.category?.name}</p>,
    },
    {
      key: '11',
      label: 'Hãng sản xuất',
      children: <p>{getIfNull(equipment?.manufacturer, '')}</p>,
    },
    {
      key: '12',
      label: 'Quốc gia',
      children: <p>{getIfNull(equipment?.manufacturingCountry, '')}</p>,
    },
  ];

  const columnsReportBroken: any[] = [
    {
      title: 'Mã phiếu',
      key: 'code',
      render: (item: ReportBrokenTicketFullInfoDto) => <>{item.code}</>,
    },
    {
      title: 'Tên thiết bị',
      key: 'name',
      render: () => <>{equipment?.name}</>,
    },
    // {
    //   title: 'Ngày tạo phiếu',
    //   key: 'reportBrokenDate',
    //   defaultSortOrder: 'descend',
    //   //   sorter: (a, b) => {},
    //   render: (item: ReportBrokenTicketFullInfoDto) => (
    //     <>{getDateForRendering(item?.createdDate as string)}</>
    //   ),
    // },
    // {
    //   title: 'Người tạo phiếu',
    //   key: 'name',
    //   render: (item: ReportBrokenTicketFullInfoDto) => <>{item.creator?.name}</>,
    // },
    // {
    //   title: 'Ghi chú của người tạo phiếu',
    //   key: 'name',
    //   render: (item: ReportBrokenTicketFullInfoDto) => <>{item.creatorNote}</>,
    // },
    {
      title: 'Người báo hỏng',
      key: 'creator',
      render: (item: ReportBrokenTicketFullInfoDto) => (
        <>{item?.creator?.name}</>
      ),
    },
    
    {
      title: ' Ngày báo hỏng',
      key: 'brokenDate',
      render: (item: ReportBrokenTicketFullInfoDto) => (
        <>{getDateForRendering(item.createdDate as string)}</>
      ),
    },
    {
      title: 'Lý do hỏng',
      key: 'reason',
      render: (item: ReportBrokenTicketFullInfoDto) => (
        <>{item?.reason}</>
      ),
    },
    {
      title: 'Mức độ ưu tiên',
      key: 'reason',
      render: (item: ReportBrokenTicketFullInfoDto) => (
        <>{i18n.t(item.priority as string).toString()}</>
      ),
    },
    
    // {
    //   title: 'Người  phê duyệt',
    //   key: 'name',
    //   render: (item: ReportBrokenTicketFullInfoDto) => <>{item.approver?.name}</>,
    // },
    // {
    //   title: ' Ngày duyệt',
    //   key: 'name',
    //   render: (item: ReportBrokenTicketFullInfoDto) => (
    //     <>{getDateForRendering(item.approvalDate as string)}</>
    //   ),
    // },
    // {
    //   title: ' Ghi chú của người duyệt',
    //   key: 'name',
    //   render: (item: ReportBrokenTicketFullInfoDto) => <>{item.creatorNote}</>,
    // },
    {
      title: ' Trạng thái duyệt',
      key: 'name',
      render: (item: ReportBrokenTicketFullInfoDto) => (
        <>
          <Tag color={getTicketStatusTag(item?.status)}>
            {i18n.t(item?.status as string)}
          </Tag>{' '}
        </>
        // <>{i18n.t(item?.status as string)}</>
      ),
    },
    {
      title: 'Thao tác',
      key: 'action',
      render: (item: ReportBrokenTicketFullInfoDto) => (
        <>
          <Menu className="flex flex-row items-center">
            {item.status === TicketStatus.PENDING &&
              hasAuthority(Authority.MEDICAL_HANDOVER_ACCEPT) && (
                <Menu.Item key="accept">
                  <Tooltip title=" Phê duyệt phiếu yêu cầu báo hỏng">
                    <CheckCircleOutlined
                      onClick={(event) => {
                        showAcceptReportBrokenTicketModalAndRenderData(equipment);
                      }}
                    />
                  </Tooltip>
                </Menu.Item>
              )}
            {item.status === TicketStatus.PENDING &&
              hasAuthority(Authority.MEDICAL_HANDOVER_CREATE) && (
                <Menu.Item key="edit">
                  <Tooltip title=" Cập nhật phiếu yêu cầu báo hỏng">
                    <EditTwoTone
                      twoToneColor="#52c41a"
                      onClick={(event) => {
                        showUpdateReportBrokenTicketModalAndRenderData(
                          equipment,
                          item
                        );
                      }}
                    />
                  </Tooltip>
                </Menu.Item>
              )}
            <DownloadTicketAttachmentsButton
              isInEquipmentDetail={true}
              ticket={item}
            />
            <Menu.Item key='detail'>
              <Tooltip title=" Chi tiết phiếu yêu cầu báo hỏng">
                <EyeOutlined
                  twoToneColor="#52c41a"
                  onClick={(event) => {
                    showDetailReportBrokenTicketModalAndRenderData(equipment, item);
                  }}
                />
              </Tooltip>
            </Menu.Item>
          </Menu>
        </>
      ),
    },
  ];

  return (
    <>
      <div>
        <div className="title text-center">LỊCH SỬ BÁO HỎNG THIẾT BỊ</div>
        <Divider></Divider>

        <Card>
          <div className="basis-2/3">
            <Descriptions items={items} column={3}></Descriptions>
          </div>
          <div className="mb-10">
            <Divider></Divider>
            <div className="font-bold text-xl mb-6">Thông tin báo hỏng</div>

            <Table
              // loading={loading}
              columns={columnsReportBroken}
              dataSource={equipment?.reportBrokenTickets}
              // pagination={true}
              className="table-responsive"
            />
            <ModalAcceptReportBrokenTicket
              showAcceptReportBrokenTicketModal={showAcceptReportBrokenTicketModal}
              hideAcceptReportBrokenTicketModal={() =>
                setShowAcceptReportBrokenTicketModal(false)
              }
              callback={() => {
                // increaseCount();
                // getAllNotifications();
                // onChangeQueryParams('', '');
              }}
              acceptReportBrokenTicketModalData={acceptReportBrokenTicketModalData}
            />

            <ModalUpdateReportBrokenTicket
              showUpdateReportBrokenTicketModal={showUpdateReportBrokenTicketModal}
              hideUpdateReportBrokenTicketModal={() =>
                setShowUpdateReportBrokenTicketModal(false)
              }
              callback={() => {
                // increaseCount();
                // getAllNotifications();
                // onChangeQueryParams('', '');
              }}
              updateReportBrokenTicketModalData={updateReportBrokenTicketModalData}
            />
            <ModalDetailReportBrokenTicket
              showDetailReportBrokenTicketModal={showDetailReportBrokenTicketModal}
              hideDetailReportBrokenTicketModal={() =>
                setShowDetailReportBrokenTicketModal(false)
              }
              callback={() => {
                // increaseCount();
                // getAllNotifications();
                // onChangeQueryParams('', '');
              }}
              detailReportBrokenTicketModalData={detailReportBrokenTicketModalData}
            />
          </div>
        </Card>
      </div>
    </>
  );
};

export default ReportBrokenTicketList;
