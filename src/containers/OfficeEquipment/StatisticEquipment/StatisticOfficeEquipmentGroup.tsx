import { Pie } from '@ant-design/plots';
import { Card, Divider, Select, Space, Table, Tabs,} from 'antd';
import equipmentApi from 'api/medicalEquipment.api';
import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { CountEquipmentByCategory, CountEquipmentByGroupAndCategory, CountEquipmentByDepartment, CountEquipmentByDepartmentAndStatus, CountEquipmentByRiskLevel, CountEquipmentByStatus, StatisticDashboard } from 'types/statistics.type';
// import './index.css';
import { useTranslation } from 'react-i18next';
import {getEquipmentCategoryOptions, options } from 'utils/globalFunc.util';
import { FilterContext } from 'contexts/filter.context';
import { PieChartOutlined, UnorderedListOutlined } from '@ant-design/icons';

const { Meta } = Card;

const StatisticOfficeEquipmentGroup = () => {
  const navigate = useNavigate();
  const [countEquipmentByGroupAndCategory, setCountEquipmentByGroupAndCategory] = useState<CountEquipmentByGroupAndCategory[]>([]);
  const [countByCategory, setCountByCategory] = useState<CountEquipmentByCategory[]>([]);
  const [group, setGroup] = useState<any>(undefined);
  const [category, setCategory] = useState<any>(undefined);
  const { t } = useTranslation();
  const { officeEquipmentCategories, officeEquipmentGroups } = useContext(FilterContext);
  const count = (groupId: number, categoryId: number) => {
    equipmentApi.statisticOfficeEquipmentByGroup().then((res) => {
      if (res.data.success) {
        const data : CountEquipmentByGroupAndCategory[] = res.data.data;
        setCountEquipmentByGroupAndCategory(data);



        if (groupId !== undefined) {
          let filterByGroup : CountEquipmentByGroupAndCategory[] = data.filter(item => item.groupId === groupId);
          setCountEquipmentByGroupAndCategory(filterByGroup);
          setCountByCategory(filterByGroup[0].countByCategory);
          if (categoryId !== undefined) {
            filterByGroup.forEach(item => {
              const filterByCategory : CountEquipmentByCategory[] =  item.countByCategory.filter(item => item.categoryId === categoryId);
              item.countByCategory = filterByCategory;
              // setCountByCategory(filterByCategory);
            })
            setCountEquipmentByGroupAndCategory(filterByGroup);
          }
        }
      }
    });
  };

  


  useEffect(() => {
    count(group, category);
    // console.log("hello", countEquipmentByDepartmentAndStatus);
  }, [group, category]);

  
  /////
  let columns: any = [
    {
      title: 'Nhóm thiết bị', key: 'groupName', show: true, dataIndex: 'groupName',
    },
    {
      title: 'Tổng số thiết bị', key: 'count', show: true, dataIndex: 'count',
    }, 
  ];
  
let nestedColumn : any = [
  , 
  {
   title: 'Loại thiết bị', key: 'categoryName', show: true, render: (item: CountEquipmentByCategory) => (<div>{item.categoryName}</div>)
 },
 {
   title: 'Số lượng', key: 'count', show: true, render: (item: CountEquipmentByCategory) => (<div>{item.count}</div>)
 },
]

  const onChangeSelect = (key: string, value: any) => {
   
    if (key === 'groupId') {
      setGroup(value);
    }
    if (key === 'categoryId') {
      setCategory(value);
    }
  }; 

  
  /////
  const configForGroup : any = {
    appendPadding: 10,
    data: countEquipmentByGroupAndCategory,
    angleField: 'count',
    colorField: 'groupName',
    radius: 1,
    innerRadius: 0.6,
    label: {
      type: 'inner',
      offset: '-50%',
      // content: '{count}',
      style: {
        textAlign: 'center',
        fontSize: 14,
      },
    },
    interactions: [
      {
        type: 'element-selected',
      },
      {
        type: 'element-active',
      },
    ],
    statistic: {
      title: false,
      content: {
        style: {
          whiteSpace: 'pre-wrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
        // content: 'Nhóm',
      },
    },
  }

  const configForCategory : any = {
    appendPadding: 10,
    data: countByCategory,
    angleField: 'count',
    colorField: 'categoryName',
    radius: 1,
    innerRadius: 0.6,
    label: {
      type: 'inner',
      offset: '-50%',
      // content: '{count}',
      style: {
        textAlign: 'center',
        fontSize: 14,
      },
    },
    interactions: [
      {
        type: 'element-selected',
      },
      {
        type: 'element-active',
      },
    ],
    statistic: {
      title: false,
      content: {
        style: {
          whiteSpace: 'pre-wrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
        // content: 'Nhóm',
      },
    },
  }

  return (<>
    <div className='title text-center'>Thống kê thiết bị theo nhóm - loại</div>
    <Divider />
    <div>
    <Card>
    <Tabs size='large'>
    <Tabs.TabPane tab={
        <span><PieChartOutlined style={{display: 'inline-flex'}} />Biểu đồ</span>
      } key="tab2">
        <div className='flex flex-start gap-6'>
        <Select
          style={{width: 250, margin: 20}}
          showSearch
          placeholder=' Nhóm thiết bị'
          optionFilterProp='children'
          onChange={(value: string) => onChangeSelect('groupId', value)}
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={options(officeEquipmentGroups)}
          value={group}
        />
        </div>
        <Card title='Biểu đồ thống kê' hoverable>
          {group == undefined ?
        <Pie
            {...configForGroup}
          /> : <Pie {...configForCategory}>

          </Pie>}
        </Card>
        
      </Tabs.TabPane>
      <Tabs.TabPane tab={
        <span><UnorderedListOutlined style={{display: 'inline-flex'}} /> Danh sách</span>
      } key="1">
        <div className='flex flex-between-center'>
      <div className='flex flex-start gap-6'>
        <Select
          style={{width: 250}}
          showSearch
          placeholder=' Nhóm thiết bị'
          optionFilterProp='children'
          onChange={(value: string) => onChangeSelect('groupId', value)}
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={options(officeEquipmentGroups)}
          value={group}
        />
        <Select
          style={{width: 250}}
          showSearch
          placeholder='Loại thiết bị'
          optionFilterProp='children'
          onChange={(value: any) => onChangeSelect('categoryId', value)}
          // onSearch={onSearch}
          allowClear
          filterOption={(input, option) => (option!.label as unknown as string).toLowerCase().includes(input.toLowerCase())}
          options={getEquipmentCategoryOptions(officeEquipmentCategories, group)}
          value={category}
        />
      </div> 
        {/* <ExportToExcel 
          data={countEquipmentByGroupAndCategory} 
          fileName={'Theo nhom loai'} 
          headerName={columns.map((item: any) => item.title)} 
          sheetName={'Theo nhom loai'} 
          title={''}></ExportToExcel> */}
    </div>
      
      
      <Table
      columns={columns}
      dataSource={countEquipmentByGroupAndCategory}
      rowKey={(record) => record.groupId}
      expandable={{
        expandedRowRender: (record) => {
          return <Table columns={nestedColumn} dataSource={record.countByCategory} pagination={false}
          footer={() => (<><Space></Space></>)}
          >
          </Table>
        },
        rowExpandable: (record) => true,
      }}
      className='mt-6 shadow-md ant-table-column'
      footer={() => (<>
        {/* <TableFooter paginationProps={pagination} /> */}
      </>)}
      // loading={loading}
      />
      </Tabs.TabPane>


    </Tabs>
    </Card>
    

      
    </div>

   
  </>);
};

export default StatisticOfficeEquipmentGroup;
