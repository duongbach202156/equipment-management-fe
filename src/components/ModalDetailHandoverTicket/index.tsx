import { Button, Descriptions, DescriptionsProps, Form, Input, Modal, Radio, Tag } from 'antd';
import {
  getCurrentUser,
  getDateForRendering,
  getBiggestIdTicket,
  startDateBeforeEndDate,
  getDateAsMoment,
  getIfNull,
  getTicketStatusTag,
} from '../../utils/globalFunc.util';
import {
  DATE_TIME_FORMAT,
  TIME_FORMAT,
} from '../../constants/dateFormat.constants';
import { FileOutlined } from '@ant-design/icons';
import TextArea from 'antd/lib/input/TextArea';
import { toast } from 'react-toastify';
import { useEffect, useState } from 'react';
import { HandoverTicketFullInfoDto } from '../../types/handover.type';
import equipmentHandover from '../../api/equipment_handover.api';
import equipmentHandoverApi from '../../api/equipment_handover.api';
import moment from 'moment';
import { fileApi } from '../../api/file.api';
import { FileStorageDto } from '../../types/fileStorage.type';
import DownloadTicketAttachmentsButton from '../DownloadTicketAttachmentsButton';
import { date, object } from 'yup';
import DatePicker from 'components/DatePicker';
import { DetailHandoverTicketModalData } from 'containers/Equipment/Handover/TicketList';
import i18n from 'i18n';

export interface ModalDetailHandoverTicketProps {
  showDetailHandoverTicketModal: boolean;
  hideDetailHandoverTicketModal: () => void;
  callback: () => void;
  detailHandoverTicketModalData: DetailHandoverTicketModalData;
}

const ModalDetailHandoverTicket = (props: ModalDetailHandoverTicketProps) => {
  const {
    showDetailHandoverTicketModal,
    hideDetailHandoverTicketModal,
    callback,
    detailHandoverTicketModalData,
  } = props;
  const { equipment, handoverTicket } = detailHandoverTicketModalData;
  const [handoverTicketFullInfoDto, setHandoverTicketFullInfoDto] =
    useState<HandoverTicketFullInfoDto>({});
  const [loading, setLoading] = useState<boolean>();
  const [isDetail, setIsDetail] = useState<boolean>(true);

  useEffect(() => {
    if (equipment?.id === undefined || handoverTicket?.id === undefined) {
      return;
    }
    equipmentHandoverApi
      .getHandoverTicketDetail(
        equipment?.id as number,
        handoverTicket?.id as number
      )
      .then((res) => {
        if (res.data.success) {
          setHandoverTicketFullInfoDto(res.data.data);
        }
      })
      .catch((err) => {
        toast.error('Lỗi khi lấy thông tin phiếu yêu cầu bàn giao');
        console.log('Error when getting handover ticket detail: ', err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [detailHandoverTicketModalData]);

  const items: DescriptionsProps['items'] = [
    {
      label: 'Mã phiếu',
      key: 'code',
      children: <p>{getIfNull(handoverTicketFullInfoDto?.code, '')}</p>,
    },
    {
      label: 'Tên thiết bị',
      key: 'name',
      children: <p>{getIfNull(equipment?.name, '')}</p>,
    },

    {
      label: 'Ngày tạo phiếu',
      key: 'handoverDate',
      children: <>{getDateForRendering(handoverTicketFullInfoDto?.createdDate as string)}</>
    },
    {
      label: 'Người tạo phiếu',
      key: 'creator',
      children: <>{handoverTicketFullInfoDto.creator?.name}</>
    },
    {
      label: 'Ghi chú của người tạo phiếu',
      key: 'creatorNote',
      children: <>{handoverTicketFullInfoDto.creatorNote}</>
    },
    {
      label: 'Người phụ trách',
      key: 'handoverInCharge',
      children: <>{handoverTicketFullInfoDto.responsiblePerson?.name}</>
    },
    {
      label: 'Khoa Phòng nhận bàn giao',
      key: 'department',
      children:  <>{handoverTicketFullInfoDto?.department?.name}</>
    },

    {
      label: ' Ngày bàn giao',
      key: 'hadoverDate',
      children: 
        <>{getDateForRendering(handoverTicketFullInfoDto.handoverDate as string)}</>
    },
    {
      label: 'Người  phê duyệt',
      key: 'approver',
      children: <>{handoverTicketFullInfoDto.approver?.name}</>
    },
    {
      label: ' Ngày duyệt',
      key: 'acceptedDate',
      children: 
        <>{getDateForRendering(handoverTicketFullInfoDto.approvalDate as string)}</>
    },
    {
      label: ' Ghi chú của người duyệt',
      key: 'approverNote',
      children: <>{handoverTicketFullInfoDto.approverNote}</>,
    },
    {
      label: ' Trạng thái duyệt',
      key: 'status',
      children: 
        <>
          <Tag color={getTicketStatusTag(handoverTicketFullInfoDto?.status)}>
            {i18n.t(handoverTicketFullInfoDto?.status as string)}
          </Tag>{' '}
        </>
        // <>{i18n.t(item?.status as string)}</>
    },
    {
      label: 'Thao tác', key: 'action', children: <DownloadTicketAttachmentsButton isInEquipmentDetail={true} ticket={handoverTicketFullInfoDto} />,
    },
  ]
  

  return (
    <Modal
      width={1000}
      // title="Phê duyệt phiếu yêu cầu bàn giao"
      open={showDetailHandoverTicketModal}
      onCancel={() => {
        hideDetailHandoverTicketModal();
      }}
      footer={null}
    >
      <Descriptions title='Thông tin phiếu bàn giao' items={items} column={2}></Descriptions>
      {/* <div className="flex flex-row justify-end gap-4">
        <Form.Item>
          <Button htmlType="submit" className="button" loading={loading}>
            Xác nhận
          </Button>
        </Form.Item>
        <Form.Item>
          <Button
            onClick={() => hideDetailHandoverTicketModal()}
            className="button_reject"
          >
            Đóng
          </Button>
        </Form.Item>
      </div> */}
    </Modal>
  );
};
export default ModalDetailHandoverTicket;
