import { Button, Form, Input, Modal, Radio, Select, Upload, UploadProps } from 'antd';
import { getCurrentUser, getDateForRendering, getBiggestIdTicket, startDateBeforeEndDate, getDepartmentOptions, options, validateFileSize, getDateAsMoment, getUserOptions, base64ToBlob, base64ToFile } from '../../utils/globalFunc.util';
import { DATE_TIME_FORMAT, TIME_FORMAT } from '../../constants/dateFormat.constants';
import { FileOutlined, UploadOutlined } from '@ant-design/icons';
import TextArea from 'antd/lib/input/TextArea';

import { toast } from 'react-toastify';
import { useContext, useEffect, useState } from 'react';
import { UpdateReportBrokenTicketForm, ReportBrokenTicketFullInfoDto } from '../../types/reportBroken.type';
import equipmentHandover from '../../api/equipment_handover.api';
import equipmentHandoverApi from '../../api/equipment_handover.api';
import moment, { Moment } from 'moment';
import { fileApi } from '../../api/file.api';
import { FileStorageDto } from '../../types/fileStorage.type';
import DownloadTicketAttachmentsButton from '../DownloadTicketAttachmentsButton';
import { date, object } from 'yup';
import userApi from 'api/user.api';
import { FilterContext } from 'contexts/filter.context';
import { UserDetailDto } from 'types/user.type';
import { UploadFile } from 'antd/es/upload';
import { RcFile } from 'antd/lib/upload';
import DatePicker from 'components/DatePicker';
import { equipmentReportBrokenApi } from 'api/equipmentReportBrokenApi';
import { RepairPriority } from 'types/repair.type';
import { UpdateReportBrokenTicketModalData } from 'containers/Equipment/ReportBroken';

export interface ModalUpdateReportBrokenTicketProps {
  showUpdateReportBrokenTicketModal: boolean;
  hideUpdateReportBrokenTicketModal: () => void;
  callback: () => void;
  updateReportBrokenTicketModalData: UpdateReportBrokenTicketModalData;
}

const ModalUpdateReportBrokenTicket = (props: ModalUpdateReportBrokenTicketProps) => {
  const {
    showUpdateReportBrokenTicketModal, hideUpdateReportBrokenTicketModal, callback, updateReportBrokenTicketModalData,
  } = props;
  const { equipment, reportBrokenTicket } = updateReportBrokenTicketModalData;
  // const reportBrokenTicket= getBiggestIdTicket(equipment?.reportBrokenTickets);


  const [form] = Form.useForm<UpdateReportBrokenTicketForm>();


  const [reportBrokenTicketFullInfoDto, setReportBrokenTicketFullInfoDto] = useState<ReportBrokenTicketFullInfoDto>({});


  const [loading, setLoading] = useState<boolean>();

  const [isUpdate, setIsUpdate] = useState<boolean>(true);
  const [updateReportBrokenTicketDocuments, setUpdateReportBrokenTicketDocuments] =
  useState<any[]>([]);

  const { departments } = useContext(FilterContext);
  const [responsiblePersons, setResponsiblePersons] = useState<UserDetailDto[]>(
    []
  );
  const uploadProps: UploadProps = {
    name: 'updateReportBrokenTicketDocuments',
    // onChange(info) {
    //   if (info.file.status !== 'uploading') {
    //     console.log(info.file, info.fileList);
    //   }
    //   if (info.file.status === 'done') {
    //     toast.success(`${info.file.name} file uploaded successfully`);
    //   } else if (info.file.status === 'error') {
    //     toast.error(`${info.file.name} file upload failed.`);
    //   }
    // },
    onChange({ file, fileList }) {
      if (file.status !== 'uploading') {
        console.log(file, fileList);
      }
      console.log(fileList);

    },
    customRequest: (info) => {
      setUpdateReportBrokenTicketDocuments(
        updateReportBrokenTicketDocuments?.concat(info.file)
      );
    },
    onRemove: (file) => {
      setUpdateReportBrokenTicketDocuments(
        updateReportBrokenTicketDocuments?.filter(
          (item: any) => item.uid !== file.uid
        )
      );
    },
    defaultFileList: updateReportBrokenTicketDocuments,
    fileList: updateReportBrokenTicketDocuments,
    beforeUpload: validateFileSize,
  };
  

  useEffect(() => {
    if (equipment?.id === undefined || reportBrokenTicket?.id === undefined) {
      return;
    }
    setLoading(true);
    const fileStorageDtoes = reportBrokenTicket.attachments as FileStorageDto[];
    const ids: number[] = fileStorageDtoes.map(value => value.id);
    if (ids.length > 0) {
      fileApi.getDocumentsByIdIn(ids).then(res => {
        // console.log(res.data.data)
        const fileList : File[] = res.data.data.map(item => {
          let file = base64ToFile(item.data, `${item.name}.${item.extension}`)
          // const uploadFile : UploadFile = {
          //   uid: item.id as unknown as string, // Unique ID for the file
          //   name: file.name,
          //   status: 'done',
          //   url: URL.createObjectURL(file),
          //   originFileObj: file as RcFile,
          // };
          // const uploadFile: UploadFile = {
          //   uid: item.id as unknown as string,
          //   name: `${item.name}.${item.extension}`,
          //   originFileObj: file as RcFile,
          //   type: item.contentType
          // }
          return file;
        }
        )
        // console.log(fileList)
  
      setUpdateReportBrokenTicketDocuments(fileList);
      });
    }
    

    form.setFieldsValue({
    //   createdDate: moment("2020-06-09T12:40:14+0000"),
      createdDate: getDateAsMoment(reportBrokenTicket?.createdDate),
      creatorNote: reportBrokenTicket.creatorNote as string,
      reason: reportBrokenTicket.reason as string,
      priority: reportBrokenTicket.priority
    })

    // form.setFieldsValue({
    //   approvalDate: moment(new Date()),
    // });
    equipmentReportBrokenApi.getReportBrokenTicketDetail(equipment?.id as number, reportBrokenTicket?.id as number).then((res) => {
      if (res.data.success) {
        setReportBrokenTicketFullInfoDto(res.data.data);
      }
    }).catch((err) => {
      toast.error('Lỗi khi lấy thông tin phiếu báo hỏng');
      console.log('Error when getting handover ticket detail: ', err);
    }).finally(() => {
      setLoading(false);
    });
  }, [updateReportBrokenTicketModalData]);
  const updateReportBrokenTicket = (updateReportBrokenTicketForm: UpdateReportBrokenTicketForm) => {
    setLoading(true);
    // console.log(updateReportBrokenTicketDocuments);
    equipmentReportBrokenApi.updateReportBrokenTicket(updateReportBrokenTicketForm, equipment?.id as number, reportBrokenTicket?.id as number, updateReportBrokenTicketDocuments).then((res) => {
      if (res.data.success) {
        toast.success('Cập nhật phiếu báo hỏng thành công');
        callback();
        hideUpdateReportBrokenTicketModal();
        form.resetFields();
      }
    }).catch((err) => {
      toast.error('Lỗi khi Cập nhật phiếu báo hỏng');
      console.log('Error when Updateing handover ticket: ', err);
    }).finally(() => {
      setLoading(false);
    });
  };


  const validationSchema = object().shape({
    createdDate: date(),
    handoverDate: date()
    .test('start-date-before-end-date', 'Thời điểm bàn giao phải sau thời điểm tạo phiếu', function (value) {
      const startDate = getDateAsMoment(reportBrokenTicket?.createdDate);
      // console.log(startDateBeforeEndDate(value, startDate));
      return startDateBeforeEndDate(value, startDate);
    })
  });
  const yupSync = {
    async validator({ field }: any, value: any) {
      await validationSchema.validateSyncAt(field, { [field]: value });
    },
  };

  return (<Modal
    width={1000}
    title="Báo hỏng thiết bị"
    open={showUpdateReportBrokenTicketModal}
    onCancel={() => {
      hideUpdateReportBrokenTicketModal();
      form.resetFields();
    }}
    footer={null}
  >
    <Form
      form={form}
      layout="vertical"
      size="large"
      onFinish={updateReportBrokenTicket}
    >
      <div className="grid grid-cols-2 gap-5">
        <Form.Item label="Tên thiết bị">
          <Input
            className="input"
            disabled
            value={updateReportBrokenTicketModalData.equipment?.name}
          />
        </Form.Item>
        <Form.Item label='Khoa phòng'>
          <Input className='input' disabled value={updateReportBrokenTicketModalData.equipment?.department?.name} />
        </Form.Item>
        
        <Form.Item
          label=' Lý do hỏng' name='reason'
          required
          rules={[{ required: true, message: 'Hãy nhập lý do hỏng!' }]}
        >
          <TextArea placeholder='Nhập lý do hỏng' className='input' />
        </Form.Item>
        <Form.Item
          label='Mức độ quan trọng'
          name='priority'
          required
          rules={[{ required: true, message: 'Hãy chọn mức độ quan trọng!' }]}
        >
          <Radio.Group>
            <Radio value={RepairPriority.HIGH}>Cao</Radio>
            <Radio value={RepairPriority.MEDIUM}>Trung bình</Radio>
            <Radio value={RepairPriority.LOW}>Thấp</Radio>
          </Radio.Group>
        </Form.Item>
        
        
        <Form.Item
          label="Ngày tạo phiếu"
          name="createdDate"
          required
          rules={[
            { required: true, message: 'Vui lòng chọn ngày tạo phiếu' },
            yupSync,
          ]}
        >
          <DatePicker
            style={{
              width: '100%',
            }}
            format={DATE_TIME_FORMAT}
            showTime={{ format: TIME_FORMAT }}
            allowClear={false}
       
          />
        </Form.Item>
        
        <Form.Item label="Ghi chú" name="creatorNote">
          <TextArea placeholder="Nhập ghi chú" className="input" />
        </Form.Item>
        <Form.Item label="Người lập phiếu yêu cầu">
          <Input className="input" disabled value={getCurrentUser().name} />
        </Form.Item>
        <Form.Item label={'Tải lên tài liệu bàn giao'}>
          <Upload {...uploadProps}>
            <Button
              className="modalFileUploadDownloadButton"
              icon={<UploadOutlined />}
            >
              {' '}
              Tải lên tài liệu bàn giao
            </Button>
          </Upload>
        </Form.Item>
      </div>
      <div className="flex flex-row justify-end gap-4">
        <Form.Item>
          <Button htmlType="submit" className="button" loading={loading}>
            Xác nhận
          </Button>
        </Form.Item>
        <Form.Item>
          <Button
            onClick={() => hideUpdateReportBrokenTicketModal()}
            className="button_reject"
          >
            Đóng
          </Button>
        </Form.Item>
      </div>
    </Form>
  </Modal>);

};
export default ModalUpdateReportBrokenTicket;
