import { Button, Form, Input, Modal, Radio, Select, Upload, UploadProps } from 'antd';
import { getCurrentUser, getDateForRendering, getBiggestIdTicket, startDateBeforeEndDate, getDepartmentOptions, options, validateFileSize, getDateAsMoment, getUserOptions, base64ToBlob, base64ToFile } from '../../utils/globalFunc.util';
import { DATE_TIME_FORMAT, TIME_FORMAT } from '../../constants/dateFormat.constants';
import { FileOutlined, UploadOutlined } from '@ant-design/icons';
import TextArea from 'antd/lib/input/TextArea';
import { UpdateHandoverTicketModalData } from '../../containers/Equipment/Handover';
import { toast } from 'react-toastify';
import { useContext, useEffect, useState } from 'react';
import { UpdateHandoverTicketForm, HandoverTicketFullInfoDto } from '../../types/handover.type';
import equipmentHandover from '../../api/equipment_handover.api';
import equipmentHandoverApi from '../../api/equipment_handover.api';
import moment, { Moment } from 'moment';
import { fileApi } from '../../api/file.api';
import { FileStorageDto } from '../../types/fileStorage.type';
import DownloadTicketAttachmentsButton from '../DownloadTicketAttachmentsButton';
import { date, object } from 'yup';
import userApi from 'api/user.api';
import { FilterContext } from 'contexts/filter.context';
import { UserDetailDto } from 'types/user.type';
import { UploadFile } from 'antd/es/upload';
import { RcFile } from 'antd/lib/upload';
import DatePicker from 'components/DatePicker';

export interface ModalUpdateHandoverTicketProps {
  showUpdateHandoverTicketModal: boolean;
  hideUpdateHandoverTicketModal: () => void;
  callback: () => void;
  updateHandoverTicketModalData: UpdateHandoverTicketModalData;
}

const ModalUpdateHandoverTicket = (props: ModalUpdateHandoverTicketProps) => {
  const {
    showUpdateHandoverTicketModal, hideUpdateHandoverTicketModal, callback, updateHandoverTicketModalData,
  } = props;
  const { equipment, handoverTicket } = updateHandoverTicketModalData;
  // const handoverTicket= getBiggestIdTicket(equipment?.handoverTickets);


  const [form] = Form.useForm<UpdateHandoverTicketForm>();


  const [handoverTicketFullInfoDto, setHandoverTicketFullInfoDto] = useState<HandoverTicketFullInfoDto>({});


  const [loading, setLoading] = useState<boolean>();

  const [isUpdate, setIsUpdate] = useState<boolean>(true);
  const [updateHandoverTicketDocuments, setUpdateHandoverTicketDocuments] =
  useState<any[]>([]);

  const { departments } = useContext(FilterContext);
  const [responsiblePersons, setResponsiblePersons] = useState<UserDetailDto[]>(
    []
  );
  const uploadProps: UploadProps = {
    name: 'updateHandoverTicketDocuments',
    // onChange(info) {
    //   if (info.file.status !== 'uploading') {
    //     console.log(info.file, info.fileList);
    //   }
    //   if (info.file.status === 'done') {
    //     toast.success(`${info.file.name} file uploaded successfully`);
    //   } else if (info.file.status === 'error') {
    //     toast.error(`${info.file.name} file upload failed.`);
    //   }
    // },
    onChange({ file, fileList }) {
      if (file.status !== 'uploading') {
        console.log(file, fileList);
      }
      console.log(fileList);

    },
    customRequest: (info) => {
      setUpdateHandoverTicketDocuments(
        updateHandoverTicketDocuments?.concat(info.file)
      );
    },
    onRemove: (file) => {
      setUpdateHandoverTicketDocuments(
        updateHandoverTicketDocuments?.filter(
          (item: any) => item.uid !== file.uid
        )
      );
    },
    defaultFileList: updateHandoverTicketDocuments,
    fileList: updateHandoverTicketDocuments,
    beforeUpload: validateFileSize,
  };
  

  useEffect(() => {
    if (equipment?.id === undefined || handoverTicket?.id === undefined) {
      return;
    }
    setLoading(true);
    const fileStorageDtoes = handoverTicket.attachments as FileStorageDto[];
    const ids: number[] = fileStorageDtoes.map(value => value.id);
    if (ids.length > 0) {
      fileApi.getDocumentsByIdIn(ids).then(res => {
        // console.log(res.data.data)
        const fileList : File[] = res.data.data.map(item => {
          let file = base64ToFile(item.data, `${item.name}.${item.extension}`)
          // const uploadFile : UploadFile = {
          //   uid: item.id as unknown as string, // Unique ID for the file
          //   name: file.name,
          //   status: 'done',
          //   url: URL.createObjectURL(file),
          //   originFileObj: file as RcFile,
          // };
          // const uploadFile: UploadFile = {
          //   uid: item.id as unknown as string,
          //   name: `${item.name}.${item.extension}`,
          //   originFileObj: file as RcFile,
          //   type: item.contentType
          // }
          return file;
        }
        )
        console.log(fileList)
  
      setUpdateHandoverTicketDocuments(fileList);
      });
    }
    

    form.setFieldsValue({
      departmentId: handoverTicket.department?.id,
      responsiblePersonId: handoverTicket.responsiblePerson?.id,
      handoverDate: getDateAsMoment(handoverTicket?.handoverDate),
    //   createdDate: moment("2020-06-09T12:40:14+0000"),
      createdDate: getDateAsMoment(handoverTicket?.createdDate),
      creatorNote: handoverTicket.creatorNote as string
    })
    userApi.getUsers({ departmentId: handoverTicket.department?.id }, { size: 10000 })
                .then((res) => {
                  setResponsiblePersons(res.data.data.content);
                })
                .finally(() => setLoading(false));
    // form.setFieldsValue({
    //   approvalDate: moment(new Date()),
    // });
    equipmentHandoverApi.getHandoverTicketDetail(equipment?.id as number, handoverTicket?.id as number).then((res) => {
      if (res.data.success) {
        setHandoverTicketFullInfoDto(res.data.data);
      }
    }).catch((err) => {
      toast.error('Lỗi khi lấy thông tin phiếu yêu cầu bàn giao');
      console.log('Error when getting handover ticket detail: ', err);
    }).finally(() => {
      setLoading(false);
    });
  }, [updateHandoverTicketModalData]);
  const updateHandoverTicket = (updateHandoverTicketForm: UpdateHandoverTicketForm) => {
    setLoading(true);
    // console.log(updateHandoverTicketDocuments);
    equipmentHandover.updateHandoverTicket(updateHandoverTicketForm, equipment?.id as number, handoverTicket?.id as number, updateHandoverTicketDocuments).then((res) => {
      if (res.data.success) {
        toast.success('Cập nhật phiếu yêu cầu bàn giao thành công');
        callback();
        hideUpdateHandoverTicketModal();
        form.resetFields();
      }
    }).catch((err) => {
      toast.error('Lỗi khi Cập nhật phiếu yêu cầu bàn giao');
      console.log('Error when Updateing handover ticket: ', err);
    }).finally(() => {
      setLoading(false);
    });
  };


  const validationSchema = object().shape({
    createdDate: date(),
    handoverDate: date()
    .test('start-date-before-end-date', 'Thời điểm bàn giao phải sau thời điểm tạo phiếu', function (value) {
      const startDate = getDateAsMoment(handoverTicket?.createdDate);
      // console.log(startDateBeforeEndDate(value, startDate));
      return startDateBeforeEndDate(value, startDate);
    })
  });
  const yupSync = {
    async validator({ field }: any, value: any) {
      await validationSchema.validateSyncAt(field, { [field]: value });
    },
  };

  return (<Modal
    width={1000}
    title="Phiếu yêu cầu bàn giao thiết bị"
    open={showUpdateHandoverTicketModal}
    onCancel={() => {
      hideUpdateHandoverTicketModal();
      form.resetFields();
    }}
    footer={null}
  >
    <Form
      form={form}
      layout="vertical"
      size="large"
      onFinish={updateHandoverTicket}
    >
      <div className="grid grid-cols-2 gap-5">
        <Form.Item label="Tên thiết bị">
          <Input
            className="input"
            disabled
            value={updateHandoverTicketModalData.equipment?.name}
          />
        </Form.Item>
        <Form.Item
          label=" Khoa phòng nhận bàn giao"
          name="departmentId"
          required={true}
          rules={[
            {
              required: true,
              message: 'Vui lòng chọn khoa phòng nhận bàn giao',
            },
          ]}
        >
          <Select
            showSearch
            placeholder="Khoa - Phòng"
            optionFilterProp="children"
            loading={loading}
            onChange={(value) => {
              setLoading(true);
              userApi
                .getUsers({ departmentId: value }, { size: 10000 })
                .then((res) => {
                  setResponsiblePersons(res.data.data.content);
                })
                .finally(() => setLoading(false));
            }}
            allowClear
            filterOption={(input, option) =>
              (option!.label as unknown as string)
                .toLowerCase()
                .includes(input.toLowerCase())
            }
            options={getDepartmentOptions(departments)}
          />
        </Form.Item>
        <Form.Item
          label=" Người phụ trách"
          name="responsiblePersonId"
          required={true}
          rules={[
            { required: true, message: 'Vui lòng chọn người phụ trách' },
          ]}
         
        >
          <Select
            showSearch
            placeholder=" Chọn người phụ trách"
            optionFilterProp="children"
            allowClear
            filterOption={(input, option) =>
              (option!.label as unknown as string)
                .toLowerCase()
                .includes(input.toLowerCase())
            }
            options={options(responsiblePersons)}
            
          />
        </Form.Item>
        <Form.Item
          label="Ngày tạo phiếu"
          name="createdDate"
          required
          rules={[
            { required: true, message: 'Vui lòng chọn ngày tạo phiếu' },
            yupSync,
          ]}
        >
          <DatePicker
            style={{
              width: '100%',
            }}
            format={DATE_TIME_FORMAT}
            showTime={{ format: TIME_FORMAT }}
            allowClear={false}
       
          />
        </Form.Item>
        <Form.Item
          label=" Ngày bàn giao"
          name="handoverDate"
          required
          rules={[
            { required: true, message: 'Vui lòng chọn ngày bàn giao' },
            yupSync,
          ]}
        >
          <DatePicker
            style={{
              width: '100%',
            }}
            format={DATE_TIME_FORMAT}
            showTime={{ format: TIME_FORMAT }}
            allowClear={false}
          />
        </Form.Item>
        <Form.Item label="Ghi chú" name="creatorNote">
          <TextArea placeholder="Nhập ghi chú" className="input" />
        </Form.Item>
        <Form.Item label="Người lập phiếu yêu cầu">
          <Input className="input" disabled value={getCurrentUser().name} />
        </Form.Item>
        <Form.Item label={'Tải lên tài liệu bàn giao'}>
          <Upload {...uploadProps}>
            <Button
              className="modalFileUploadDownloadButton"
              icon={<UploadOutlined />}
            >
              {' '}
              Tải lên tài liệu bàn giao
            </Button>
          </Upload>
        </Form.Item>
      </div>
      <div className="flex flex-row justify-end gap-4">
        <Form.Item>
          <Button htmlType="submit" className="button" loading={loading}>
            Xác nhận
          </Button>
        </Form.Item>
        <Form.Item>
          <Button
            onClick={() => hideUpdateHandoverTicketModal()}
            className="button_reject"
          >
            Đóng
          </Button>
        </Form.Item>
      </div>
    </Form>
  </Modal>);

};
export default ModalUpdateHandoverTicket;
