import * as fs from 'file-saver';
import { Button } from 'antd';
import { FileExcelFilled } from '@ant-design/icons';
import ExcelJS from 'exceljs';
import { addRow, mergeCells } from 'utils/globalFunc.util';
import { DATE_TIME_FORMAT } from 'constants/dateFormat.constants';
import moment from 'moment';

export interface Props {
  data : any,
  fileName: string,
  // headerName: string[]
  headerName: HeaderProps[]
  sheetName : string
  title: string
  loading?: boolean
  getData?: () => any
}

export interface HeaderProps {
  title: string,
  key: string,
}

const ExportToExcel = (excel: Props) => {

  const exportToExcel = () => {
    const {data, fileName, headerName, sheetName, title, loading, getData} = excel;
    if (!data || data.length === 0) {
      return;
    }
    
    const d = new Date();
    
    // const _fileName = fileName + " " + d.toLocaleString();
    const _fileName = fileName + " " + moment(d).format(DATE_TIME_FORMAT);
    const myHeader = headerName;
    const widths = headerName.map(item => ({width : 20}));
    // let newData: any = data.slice(1, data?.length);
    exportToExcelPro(
      data,
      _fileName,
      sheetName,
      title,
      myHeader,
      widths,
      loading
    );
  };

  const exportToExcelPro = async (
    myData: any,
    fileName: any,
    sheetName: any,
    report: any,
    myHeader: HeaderProps[],
    widths: any,
    loading?: boolean
  ) => {
    if (!myData || myData.length === 0) {
      console.error('Chưa có data');
      return;
    }

    const wb = new ExcelJS.Workbook();
    if (sheetName === '' || sheetName === undefined || sheetName === null) {
      sheetName = 'Sheet 1'
    }
    const ws = wb.addWorksheet(sheetName);
    const columns = myHeader?.length;
    const headerTitle = myHeader.map((item: { title: any; }) => item.title)

    const title = {
      // border: true,
      height: 30,
      font: { size: 18, bold: false, 
        // color: { argb: 'FFFFFF' } 
      },
      alignment: { horizontal: 'left', vertical: 'middle' },
      fill: {
        type: 'pattern',
        // pattern: 'solid', //darkVertical
        fgColor: {
          argb: '#dbeafe',
        },
      },
    };
    const header = {
      border: true,
      height: 0,
      font: { size: 12, bold: true, color: { argb: '000000' } },
      alignment: null,
      fill: {
        type: 'pattern',
        pattern: 'none',
        // fgColor: 'none',
        bgColor: {
          argb:'DBEAFE',
        },
      },
    };
    const dataStyle = {
      border: true,
      height: 0,
      font: { size: 12, bold: false, color: { argb: '000000' } },
      alignment: null,
      fill: null,
    };

    // if (widths && widths.length > 0) {
    //   ws.columns = widths;
    // }
    addRow(ws, null, null);
    let row = addRow(ws, [report], title);
    mergeCells(ws, row, 1, columns);
    addRow(ws, null, null);
    addRow(ws, headerTitle, header);

    

    
    // ws.columns = myHeader.map(h => ({ title: h.title, key: h.key }));

    myData.forEach((data: any, index: any) => {
      const newRow = myHeader.reduce((row : any, header : any) => {
        if (header.key === 'number') {
          row[header.key] = index + 1;
        } else {
          row[header.key] = data[header.key];
        }
        return row;
      }, {} as Partial<Record<string, string>>)
      // addRow(ws, newRow, data);
      addRow(ws, Object.values(newRow), dataStyle);
    });

    for (let i = 1; i <= ws.columnCount; i++) {
      const column = ws.getColumn(i);
      if (column) {
        let maxLength = 0;
          column.eachCell({ includeEmpty: true }, cell => {
            if (cell.isMerged) return;
            const cellValue = cell.value;
            // console.log(cellValue);
            const cellLength = cellValue ? cellValue.toString().length : 10;
            // console.log(cellLength);

            if (cellLength > maxLength) {
              maxLength = cellLength;
            }
          });
          column.width = maxLength + 2; // Add some padding
      }
    }

    
    ws.getRow(4).eachCell((cell) => cell.fill = {
      type : 'pattern',
      pattern:'solid',
      fgColor: {
        argb:'DBEAFE',
      }
    })
    
    const buf = await wb.xlsx.writeBuffer();
    fs.saveAs(new Blob([buf]), `${fileName}.xlsx`);
  }

  return (
    <Button
      className="button_excel"
      onClick={() => exportToExcel()}
      loading={excel.loading}
    >
      <FileExcelFilled />
      Xuất Excel
    </Button>
  );
}

export default ExportToExcel;