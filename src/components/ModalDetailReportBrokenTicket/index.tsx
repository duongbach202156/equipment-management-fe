import { Button, Descriptions, DescriptionsProps, Form, Input, Modal, Radio, Tag } from 'antd';
import {
  getCurrentUser,
  getDateForRendering,
  getBiggestIdTicket,
  startDateBeforeEndDate,
  getDateAsMoment,
  getIfNull,
  getTicketStatusTag,
} from '../../utils/globalFunc.util';
import {
  DATE_TIME_FORMAT,
  TIME_FORMAT,
} from '../../constants/dateFormat.constants';
import { FileOutlined } from '@ant-design/icons';
import TextArea from 'antd/lib/input/TextArea';
import { toast } from 'react-toastify';
import { useEffect, useState } from 'react';
import { ReportBrokenTicketFullInfoDto } from '../../types/reportBroken.type';
import moment from 'moment';
import { fileApi } from '../../api/file.api';
import { FileStorageDto } from '../../types/fileStorage.type';
import DownloadTicketAttachmentsButton from '../DownloadTicketAttachmentsButton';
import { date, object } from 'yup';
import DatePicker from 'components/DatePicker';
import { DetailReportBrokenTicketModalData } from 'containers/Equipment/ReportBroken/TicketList';
import i18n from 'i18n';
import { equipmentReportBrokenApi } from 'api/equipmentReportBrokenApi';

export interface ModalDetailReportBrokenTicketProps {
  showDetailReportBrokenTicketModal: boolean;
  hideDetailReportBrokenTicketModal: () => void;
  callback: () => void;
  detailReportBrokenTicketModalData: DetailReportBrokenTicketModalData;
}

const ModalDetailReportBrokenTicket = (props: ModalDetailReportBrokenTicketProps) => {
  const {
    showDetailReportBrokenTicketModal,
    hideDetailReportBrokenTicketModal,
    callback,
    detailReportBrokenTicketModalData,
  } = props;
  const { equipment, reportBrokenTicket } = detailReportBrokenTicketModalData;
  const [reportBrokenTicketFullInfoDto, setReportBrokenTicketFullInfoDto] =
    useState<ReportBrokenTicketFullInfoDto>({});
  const [loading, setLoading] = useState<boolean>();
  const [isDetail, setIsDetail] = useState<boolean>(true);

  useEffect(() => {
    if (equipment?.id === undefined || reportBrokenTicket?.id === undefined) {
      return;
    }
    equipmentReportBrokenApi
      .getReportBrokenTicketDetail(
        equipment?.id as number,
        reportBrokenTicket?.id as number
      )
      .then((res) => {
        if (res.data.success) {
          setReportBrokenTicketFullInfoDto(res.data.data);
        }
      })
      .catch((err) => {
        toast.error('Lỗi khi lấy thông tin phiếu yêu cầu bàn giao');
        console.log('Error when getting reportBroken ticket detail: ', err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [detailReportBrokenTicketModalData]);

  const items: DescriptionsProps['items'] = [
    {
      label: 'Mã phiếu',
      key: 'code',
      children: <p>{getIfNull(reportBrokenTicketFullInfoDto?.code, '')}</p>,
    },
    {
      label: 'Tên thiết bị',
      key: 'name',
      children: <p>{getIfNull(equipment?.name, '')}</p>,
    },

    {
      label: 'Ngày tạo phiếu',
      key: 'reportBrokenDate',
      children: <>{getDateForRendering(reportBrokenTicketFullInfoDto?.createdDate as string)}</>
    },
    {
      label: 'Người tạo phiếu',
      key: 'creator',
      children: <>{reportBrokenTicketFullInfoDto.creator?.name}</>
    },
    {
      label: 'Ghi chú của người tạo phiếu',
      key: 'creatorNote',
      children: <>{reportBrokenTicketFullInfoDto.creatorNote}</>
    },
    {
      label: 'Lý do hỏng',
      key: 'reason',
      children: <>{reportBrokenTicketFullInfoDto.reason}</>
    },
    {
      label: 'Mức độ ưu tiên',
      key: 'department',
      children:  <>{i18n.t(reportBrokenTicketFullInfoDto.priority as string).toString()}</>
    },

    {
      label: 'Người  phê duyệt',
      key: 'approver',
      children: <>{reportBrokenTicketFullInfoDto.approver?.name}</>
    },
    {
      label: ' Ngày duyệt',
      key: 'acceptedDate',
      children: 
        <>{getDateForRendering(reportBrokenTicketFullInfoDto.approvalDate as string)}</>
    },
    {
      label: ' Ghi chú của người duyệt',
      key: 'approverNote',
      children: <>{reportBrokenTicketFullInfoDto.approverNote}</>,
    },
    {
      label: ' Trạng thái duyệt',
      key: 'status',
      children: 
        <>
          <Tag color={getTicketStatusTag(reportBrokenTicketFullInfoDto?.status)}>
            {i18n.t(reportBrokenTicketFullInfoDto?.status as string)}
          </Tag>{' '}
        </>
        // <>{i18n.t(item?.status as string)}</>
    },
    {
      label: 'Thao tác', key: 'action', children: <DownloadTicketAttachmentsButton isInEquipmentDetail={true} ticket={reportBrokenTicketFullInfoDto} />,
    },
  ]
  

  return (
    <Modal
      width={1000}
      // title="Phê duyệt phiếu yêu cầu bàn giao"
      open={showDetailReportBrokenTicketModal}
      onCancel={() => {
        hideDetailReportBrokenTicketModal();
      }}
      footer={null}
    >
      <Descriptions title='Thông tin phiếu bàn giao' items={items} column={2}></Descriptions>
      {/* <div className="flex flex-row justify-end gap-4">
        <Form.Item>
          <Button htmlType="submit" className="button" loading={loading}>
            Xác nhận
          </Button>
        </Form.Item>
        <Form.Item>
          <Button
            onClick={() => hideDetailReportBrokenTicketModal()}
            className="button_reject"
          >
            Đóng
          </Button>
        </Form.Item>
      </div> */}
    </Modal>
  );
};
export default ModalDetailReportBrokenTicket;
