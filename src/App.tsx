import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Signin from 'containers/Signin';
import Signup from 'containers/Signup';
import ResetPassword from 'containers/ResetPassword';
import Repair from 'containers/Equipment/Repair';
import NotFoundPage from 'containers/NotFoundPage';
import PrivateRoute from 'routes/PrivateRoute';
import Dashboard from 'containers/Dashboard';
import List from 'containers/Equipment/Action/ListEquipment';
import Detail from 'containers/Equipment/Action/ViewDetail';
import CreateEquipment from 'containers/Equipment/Action/CreateNewEquipment';
import UpdateEquipment from 'containers/Equipment/Action/UpdateEquipment';
import Maintenance from 'containers/Equipment/Maintenance';
import Department from 'containers/Organization/Department';
import CreateDepartment from 'containers/Organization/Department/create';
import DetailDepartment from 'containers/Organization/Department/detail';
import CreateUser from 'containers/User/CreateUser';
import EquipmentGroup from 'containers/Category/EquipmentGroup';
import EquipmentCategory from 'containers/Category/EquipmentCategory';
import EquipmentUnit from 'containers/Category/EquipmentUnit';
import SetRole from 'containers/Setting/Role';
import NotificationList from 'containers/Notification';
import StatisticEquipment from 'containers/Equipment/StatisticEquipment/StatisticEquipment';
import CreateSchedule from 'containers/Equipment/Repair/CreateSchedule';
import UpdateSchedule from 'containers/Equipment/Repair/UpdateSchedule';
import Liquidation from 'containers/Equipment/Liquidation';
import LiquidationDetail from 'containers/Equipment/Liquidation/detail';
import Suplly from 'containers/Supply';
import SupplyDetail from 'containers/Supply/SupplyDetail';
import { ToastContainer } from 'react-toastify';
import Transfer from 'containers/Equipment/Transfer';
import TransferDetail from 'containers/Equipment/Transfer/detail';
import EmailConfig from 'containers/Setting/Symtem/EmailConfig';
import Profile from 'containers/Profile';
import ConfirmResetPassword from './containers/ResetPassword/ConfirmResetPassword';
import UpdateMaintenance from './containers/Equipment/Maintenance/UpdateMaintenance';
import Inspection from './containers/Equipment/Inspection';
import UpdateInspection from './containers/Equipment/Inspection/UpdateInspection';
import Handover from './containers/Equipment/Handover';
import ReportBroken from 'containers/Equipment/ReportBroken';
import { Authority } from 'constants/authority';
import { CreateRole } from 'containers/Setting/Role/CreateRole';
import User from 'containers/User';
import DetailUser from 'containers/User/DetailUser';
import { UpdateRole } from 'containers/Setting/Role/UpdateRole';
import CreateSupply from 'containers/Supply/CreateSupply';
import UpdateSupply from 'containers/Supply/UpdateSupply';
import Supplier from './containers/Organization/Supplier';
import Service from './containers/Category/Service';
import AttachSupplies from './containers/Equipment/Action/AttachSupplies';
import EquipmentImportFileExcel from 'components/EquipmentImportFileExcel';
import Noti from 'components/Noti';
import StatisticEquipmentDepartment from 'containers/Equipment/StatisticEquipment/StatisticEquipmentDepartmentAndStatus';
// import StatisticEquipmentGroup from 'src/containers/Equipment/StatisticEquipment/StatisticEquipmentGroup';
import StatisticEquipmentDepartmentAndStatus from 'containers/Equipment/StatisticEquipment/StatisticEquipmentDepartmentAndStatus';
import StatisticEquipmentGroup from 'containers/Equipment/StatisticEquipment/StatisticEquipmentGroup';
import StatisticEquipmentDepartmentAndRiskLevel from 'containers/Equipment/StatisticEquipment/StatisticEquipmentDepartmentAndRiskLevel';
import Project from 'containers/Organization/Project';
import OfficeEquipmentList from 'containers/OfficeEquipment/Action/ListEquipment';
import CreateOfficeEquipment from 'containers/OfficeEquipment/Action/CreateNewEquipment';
import OfficeEquipmentGroup from 'containers/Category/OfficeEquipmentGroup';
import OfficeEquipmentCategory from 'containers/Category/OfficeEquipmentCategory';
import OfficeEquipmentDetail from 'containers/OfficeEquipment/Action/ViewDetail';
import UpdateOfficeEquipment from 'containers/OfficeEquipment/Action/UpdateEquipment';
import OfficeEquipmentHandover from 'containers/OfficeEquipment/Handover';
import OfficeEquipmentReportBroken from 'containers/OfficeEquipment/ReportBroken';
import OfficeEquipmentRepair from 'containers/OfficeEquipment/Repair';
import OfficeCreateSchedule from 'containers/OfficeEquipment/Repair/CreateSchedule';
import OfficeUpdateSchedule from 'containers/OfficeEquipment/Repair/UpdateSchedule';
import OfficeEquipmentMaintenance from 'containers/OfficeEquipment/Maintenance';
import OfficeEquipmentTransfer from 'containers/OfficeEquipment/Transfer';
import OfficeEquipmentLiquidation from 'containers/OfficeEquipment/Liquidation';
import InventoryList from 'containers/Equipment/Inventory/InventoryList';
import InventoryEquipment from 'containers/Equipment/Inventory/InventoryEquipment';
import { ConfigProvider, theme } from 'antd';
import ConfirmPassword from 'containers/ConfirmPassword/ConfirmPassword';
import ChangePasswordFirstLogin from 'containers/ChangePasswordFirstLogin/ChangePasswordFirstLogin';
import { getCurrentUser, hasAuthority } from 'utils/globalFunc.util';
import HandoverTicketList from 'containers/Equipment/Handover/TicketList';
import OfficeDashboard from 'containers/Dashboard/officeDashboard';
import StatisticOfficeEquipment from 'containers/OfficeEquipment/StatisticEquipment/StatisticOfficeEquipment';
import StatisticOfficeEquipmentDepartmentAndStatus from 'containers/OfficeEquipment/StatisticEquipment/StatisticOfficeEquipmentDepartmentAndStatus';
import StatisticOfficeEquipmentGroup from 'containers/OfficeEquipment/StatisticEquipment/StatisticOfficeEquipmentGroup';
import ReportBrokenTicketList from 'containers/Equipment/ReportBroken/TicketList';
import { EquipmentType } from 'types/equipment.type';

const App = () => {
  return (
    <>
      <div>
        <div className="app">
          <BrowserRouter>
            {/*TODO: sua lai cac authority cho dung*/}
            <Routes>
              <Route
                path="/"
                element={
                  <PrivateRoute requiredAuthority={Authority.DASHBOARD_READ}>
                    <Dashboard />
                  </PrivateRoute>
                }
              />
              {/* <Route path='/' element={<PrivateRoute requiredAuthority={Authority.OFFICE_DASHBOARD_READ}><OfficeDashboard /></PrivateRoute>} /> */}
              {/*<Route path='/' element={<Dashboard />} />*/}
              {/* Equipment Routes */}
              <Route
                path="/medical-equipments"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_READ}>
                    <List />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/excel/import"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_CREATE}>
                    <EquipmentImportFileExcel />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/:equipmentId"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_READ}>
                    <Detail />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/:equipmentId/update"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_UPDATE}>
                    <UpdateEquipment />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/create"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_CREATE}>
                    <CreateEquipment />
                  </PrivateRoute>
                }
              />

              {/* Equipment handover Routes */}
              <Route
                path="/medical-equipments/handovers"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_HANDOVER_READ}
                  >
                    <Handover />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/handovers/:equipmentId"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_HANDOVER_READ}
                  >
                    <HandoverTicketList type={EquipmentType.MEDICAL} />
                  </PrivateRoute>
                }
              />
              {/* Equipment report broken Routes */}
              <Route
                path="/medical-equipments/report-broken"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_REPORT_BROKEN_READ}
                  >
                    <ReportBroken />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/report-broken/:equipmentId"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_REPORT_BROKEN_READ}
                  >
                    <ReportBrokenTicketList type={EquipmentType.MEDICAL} />
                  </PrivateRoute>
                }
              />
              {/* Equipment Repair Routes */}
              <Route
                path="/medical-equipments/repairs"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_REPAIR_READ}
                  >
                    <Repair />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/:equipmentId/repairs/create-schedule"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_REPAIR_CREATE}
                  >
                    <CreateSchedule />
                  </PrivateRoute>
                }
              />
              {/*/equipments/repairs/update-schedule/:id/:repair-id*/}
              <Route
                path="/medical-equipments/:equipmentId/repairs/:repairTicketId/update-schedule"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_REPAIR_UPDATE}
                  >
                    <UpdateSchedule />
                  </PrivateRoute>
                }
              />

              {/* Equipment Liquidation Routes */}
              <Route
                path="/medical-equipments/liquidations"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_LIQUIDATION_READ}
                  >
                    <Liquidation />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/:id/liquidations"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_LIQUIDATION_CREATE}
                  >
                    <LiquidationDetail />
                  </PrivateRoute>
                }
              />

              {/* Equipment Transfer Routes */}
              <Route
                path="/medical-equipments/transfers"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_TRANSFER_READ}
                  >
                    <Transfer />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/transfers/:equipmentId"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_TRANSFER_READ}
                  >
                    <TransferDetail />
                  </PrivateRoute>
                }
              />
              {/* Equipment Maintainance */}
              <Route
                path="/medical-equipments/maintenances"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_MAINTENANCE_READ}
                  >
                    <Maintenance />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/:equipmentId/maintenances/:maintenanceId/update"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_MAINTENANCE_UPDATE}
                  >
                    <UpdateMaintenance />
                  </PrivateRoute>
                }
              />

              {/*Equipment Inspection*/}
              <Route
                path="/medical-equipments/inspections"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_INSPECTION_READ}
                  >
                    <Inspection />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/:equipmentId/inspections/:inspectionId/update"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_INSPECTION_UPDATE}
                  >
                    <UpdateInspection />
                  </PrivateRoute>
                }
              />

              <Route
                path="/medical-equipments/:equipmentId/attach-supplies"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_UPDATE}>
                    <AttachSupplies />
                  </PrivateRoute>
                }
              />

              {/* Equipment Maintainance */}
              <Route
                path="/medical-equipments/inventories"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_INVENTORY_READ}
                  >
                    <InventoryList />
                  </PrivateRoute>
                }
              />
              <Route
                path="/medical-equipments/inventories/:inventoryId"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.MEDICAL_INVENTORY_UPDATE}
                  >
                    <InventoryEquipment />
                  </PrivateRoute>
                }
              />

              {/* office equipment */}
              {/*<Route path='/' element={<Dashboard />} />*/}
              {/* Equipment Routes */}
              <Route
                path="/office-equipments"
                element={
                  <PrivateRoute requiredAuthority={Authority.OFFICE_READ}>
                    <OfficeEquipmentList />
                  </PrivateRoute>
                }
              />
              {/* <Route path='/office-equipments/excel/import' element={<PrivateRoute requiredAuthority={Authority.EQUIPMENT_CREATE}><EquipmentImportFileExcel/></PrivateRoute>} /> */}
              <Route
                path="/office-equipments/:equipmentId"
                element={
                  <PrivateRoute requiredAuthority={Authority.OFFICE_READ}>
                    <OfficeEquipmentDetail />
                  </PrivateRoute>
                }
              />
              <Route
                path="/office-equipments/:equipmentId/update"
                element={
                  <PrivateRoute requiredAuthority={Authority.OFFICE_UPDATE}>
                    <UpdateOfficeEquipment />
                  </PrivateRoute>
                }
              />
              <Route
                path="/office-equipments/create"
                element={
                  <PrivateRoute requiredAuthority={Authority.OFFICE_CREATE}>
                    <CreateOfficeEquipment />
                  </PrivateRoute>
                }
              />

              <Route
                path="/office-equipments/handovers"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_HANDOVER_READ}
                  >
                    <OfficeEquipmentHandover />
                  </PrivateRoute>
                }
              />
              <Route
                path="/office-equipments/handovers/:equipmentId"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_HANDOVER_READ}
                  >
                    <HandoverTicketList type={EquipmentType.OFFICE} />
                  </PrivateRoute>
                }
              />

              {/* Equipment report broken Routes */}
              <Route
                path="/office-equipments/report-broken"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_REPORT_BROKEN_READ}
                  >
                    <OfficeEquipmentReportBroken />
                  </PrivateRoute>
                }
              />
              <Route
                path="/office-equipments/report-broken/:equipmentId"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_REPORT_BROKEN_READ}
                  >
                    <ReportBrokenTicketList type={EquipmentType.OFFICE} />
                  </PrivateRoute>
                }
              />
              {/* Equipment Repair Routes */}
              <Route
                path="/office-equipments/repairs"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_REPAIR_READ}
                  >
                    <OfficeEquipmentRepair />
                  </PrivateRoute>
                }
              />
              <Route
                path="/office-equipments/:equipmentId/repairs/create-schedule"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_REPAIR_CREATE}
                  >
                    <OfficeCreateSchedule />
                  </PrivateRoute>
                }
              />
              {/*/equipments/repairs/update-schedule/:id/:repair-id*/}
              <Route
                path="/office-equipments/:equipmentId/repairs/:repairTicketId/update-schedule"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_REPAIR_UPDATE}
                  >
                    <OfficeUpdateSchedule />
                  </PrivateRoute>
                }
              />

              {/* Equipment Maintainance */}
              <Route
                path="/office-equipments/maintenances"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_MAINTENANCE_READ}
                  >
                    <OfficeEquipmentMaintenance />
                  </PrivateRoute>
                }
              />

              {/* Equipment Transfer Routes */}
              <Route
                path="/office-equipments/transfers"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_TRANSFER_READ}
                  >
                    <OfficeEquipmentTransfer />
                  </PrivateRoute>
                }
              />
              {/* Equipment Liquidation Routes */}
              <Route
                path="/office-equipments/liquidations"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_LIQUIDATION_READ}
                  >
                    <OfficeEquipmentLiquidation />
                  </PrivateRoute>
                }
              />
              {/* Supply Routes */}
              <Route
                path="/supplies"
                element={
                  <PrivateRoute requiredAuthority={Authority.SUPPLY_READ}>
                    <Suplly />
                  </PrivateRoute>
                }
              />
              <Route
                path="/supplies/:supplyId"
                element={
                  <PrivateRoute requiredAuthority={Authority.SUPPLY_READ}>
                    <SupplyDetail />
                  </PrivateRoute>
                }
              />
              <Route
                path="/supplies/create"
                element={
                  <PrivateRoute requiredAuthority={Authority.SUPPLY_CREATE}>
                    <CreateSupply />
                  </PrivateRoute>
                }
              />
              <Route
                path="/supplies/:supplyId/update"
                element={
                  <PrivateRoute requiredAuthority={Authority.SERVICE_UPDATE}>
                    <UpdateSupply />
                  </PrivateRoute>
                }
              />
              {/* StatisticEquipment Routes */}
              <Route
                path="/statistic/medical-equipments"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_STATISTIC}>
                    <StatisticEquipment />
                  </PrivateRoute>
                }
              />
              <Route
                path="/statistic/medical-equipments/departments/statuses"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_STATISTIC}>
                    <StatisticEquipmentDepartmentAndStatus />
                  </PrivateRoute>
                }
              />
              <Route
                path="/statistic/medical-equipments/departments/risk-levels"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_STATISTIC}>
                    <StatisticEquipmentDepartmentAndRiskLevel />
                  </PrivateRoute>
                }
              />

              <Route
                path="/statistic/medical-equipments/groups"
                element={
                  <PrivateRoute requiredAuthority={Authority.MEDICAL_STATISTIC}>
                    <StatisticEquipmentGroup />
                  </PrivateRoute>
                }
              />

              {/* StatisticEquipment Routes */}
              <Route
                path="/statistic/office-equipments"
                element={
                  <PrivateRoute requiredAuthority={Authority.OFFICE_STATISTIC}>
                    <StatisticOfficeEquipment />
                  </PrivateRoute>
                }
              />
              <Route
                path="/statistic/office-equipments/departments/statuses"
                element={
                  <PrivateRoute requiredAuthority={Authority.OFFICE_STATISTIC}>
                    <StatisticOfficeEquipmentDepartmentAndStatus />
                  </PrivateRoute>
                }
              />

              <Route
                path="/statistic/office-equipments/groups"
                element={
                  <PrivateRoute requiredAuthority={Authority.OFFICE_STATISTIC}>
                    <StatisticOfficeEquipmentGroup />
                  </PrivateRoute>
                }
              />

              {/* Organization Routes */}
              <Route
                path="/organization/departments"
                element={
                  <PrivateRoute requiredAuthority={Authority.DEPARTMENT_READ}>
                    <Department />
                  </PrivateRoute>
                }
              />
              <Route
                path="/organization/departments/create"
                element={
                  <PrivateRoute requiredAuthority={Authority.DEPARTMENT_CREATE}>
                    <CreateDepartment />
                  </PrivateRoute>
                }
              />
              <Route
                path="/organization/departments/:departmentId"
                element={
                  <PrivateRoute requiredAuthority={Authority.DEPARTMENT_UPDATE}>
                    <DetailDepartment />
                  </PrivateRoute>
                }
              />
              <Route
                path="/organization/suppliers"
                element={
                  <PrivateRoute requiredAuthority={Authority.SUPPLIER_READ}>
                    <Supplier />
                  </PrivateRoute>
                }
              />
              <Route
                path="/users"
                element={
                  <PrivateRoute requiredAuthority={Authority.USER_READ}>
                    <User />
                  </PrivateRoute>
                }
              />

              <Route
                path="/users/create"
                element={
                  <PrivateRoute requiredAuthority={Authority.USER_CREATE}>
                    <CreateUser />
                  </PrivateRoute>
                }
              />
              <Route
                path="/users/:userId"
                element={
                  <PrivateRoute requiredAuthority={Authority.USER_READ}>
                    <DetailUser />
                  </PrivateRoute>
                }
              />

              <Route
                path="/organization/projects"
                element={
                  <PrivateRoute requiredAuthority={Authority.PROJECT_READ}>
                    <Project />
                  </PrivateRoute>
                }
              />

              {/* Profile Routes */}
              <Route
                path="/profile"
                element={
                  <PrivateRoute requiredAuthority={Authority.USER_READ}>
                    <Profile />
                  </PrivateRoute>
                }
              />

              {/* Category Routes */}

              {/* Group */}
              <Route
                path="/category/groups/office-equipment-groups"
                element={
                  <PrivateRoute requiredAuthority={Authority.OFFICE_GROUP_READ}>
                    <OfficeEquipmentGroup></OfficeEquipmentGroup>
                  </PrivateRoute>
                }
              ></Route>
              <Route
                path="/category/groups/medical-equipment-groups"
                element={
                  <PrivateRoute requiredAuthority={Authority.OFFICE_GROUP_READ}>
                    <EquipmentGroup />
                  </PrivateRoute>
                }
              />

              {/* Type */}
              <Route
                path="/category/types/medical-equipment-categories"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_CATEGORY_READ}
                  >
                    <EquipmentCategory />
                  </PrivateRoute>
                }
              />

              <Route
                path="/category/types/office-equipment-categories"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.OFFICE_CATEGORY_READ}
                  >
                    <OfficeEquipmentCategory />
                  </PrivateRoute>
                }
              />
              {/* Unit */}
              <Route
                path="/category/equipment-units"
                element={
                  <PrivateRoute
                    requiredAuthority={Authority.EQUIPMENT_UNIT_READ}
                  >
                    <EquipmentUnit />
                  </PrivateRoute>
                }
              />
              <Route
                path="/category/services"
                element={
                  <PrivateRoute requiredAuthority={Authority.SERVICE_READ}>
                    <Service />
                  </PrivateRoute>
                }
              />
              {/* Setting Routes */}
              <Route
                path="/setting/roles"
                element={
                  <PrivateRoute requiredAuthority={Authority.ROLE_READ}>
                    <SetRole />
                  </PrivateRoute>
                }
              />
              <Route
                path="/setting/roles/:roleId/update"
                element={
                  <PrivateRoute requiredAuthority={Authority.ROLE_READ}>
                    <UpdateRole />
                  </PrivateRoute>
                }
              />
              <Route
                path="/setting/roles/create"
                element={
                  <PrivateRoute requiredAuthority={Authority.ROLE_CREATE}>
                    <CreateRole />
                  </PrivateRoute>
                }
              />
              <Route
                path="/setting/notifications"
                element={
                  <PrivateRoute requiredAuthority={Authority.ROLE_READ}>
                    <NotificationList />
                  </PrivateRoute>
                }
              />
              <Route
                path="/setting/email-config"
                element={
                  <PrivateRoute requiredAuthority={Authority.ROLE_READ}>
                    <EmailConfig />
                  </PrivateRoute>
                }
              />

              {/* Auth Routes */}
              <Route path="/signin" element={<Signin />} />
              <Route path="/signup" element={<Signup />} />
              <Route path="/reset-password" element={<ResetPassword />} />
              <Route
                path="/registration/confirm"
                element={<ConfirmPassword></ConfirmPassword>}
              ></Route>
              <Route
                path="/change-password/first-login"
                element={<ChangePasswordFirstLogin></ChangePasswordFirstLogin>}
              ></Route>
              <Route
                path="/reset-password/confirm"
                element={<ConfirmResetPassword />}
              />
              <Route path="*" element={<NotFoundPage />} />

              {/* <Route path='websocket' element={<Noti />} /> */}
            </Routes>
          </BrowserRouter>
          <ToastContainer
            position="top-right"
            autoClose={2000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="light"
          />
        </div>
      </div>
    </>
  );
};

export default App;
