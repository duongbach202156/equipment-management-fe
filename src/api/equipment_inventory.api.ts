import axiosClient from './axiosClient';
import { GenericResponse, PageResponse } from 'types/commonResponse.type';
import { AxiosResponse } from 'axios';
import { GetInventoryEquipmentQueryParam, GetInventoryQueryParam, InventoryDto, InventoryEquipmentDto, InventoryEquipmentForm, InventoryFullInfoDto, UpsertInventoryForm } from 'types/inventory.type';
import { PageableRequest } from 'types/commonRequest.type';
import { reducePageNumberByOne } from 'utils/globalFunc.util';
import qs from 'qs';

const equipmentInventoryApi = {

  getAllInventoryMedical(queryParams: GetInventoryQueryParam, pageable: PageableRequest) 
  : Promise<AxiosResponse<GenericResponse<PageResponse<InventoryDto>>>> {
    reducePageNumberByOne(pageable);
    const queryString = qs.stringify(queryParams) + '&' + qs.stringify(pageable, { encode: false, arrayFormat: 'repeat' });
    const url = `inventories?${queryString}`;
    return axiosClient.get(url);
  },
  getInventoryById(id: Number, queryParams : GetInventoryEquipmentQueryParam) : Promise<AxiosResponse<GenericResponse<InventoryFullInfoDto>>> {
    const queryString = qs.stringify(queryParams);
    const url = `inventories/${id}?${queryString}`;
    return axiosClient.get(url);
  },
  createInventoryMedical(params: UpsertInventoryForm) : Promise<AxiosResponse<GenericResponse<InventoryFullInfoDto>>> {
    const url = 'inventories';
    return axiosClient.post(url, params);
  },
  updateInventory(inventoryId: number, params: UpsertInventoryForm) : Promise<AxiosResponse<GenericResponse<InventoryFullInfoDto>>> {
    const url = `inventories/${inventoryId}`;
    return axiosClient.put(url, params);
  },
  confirmInventory(inventoryId: number) : Promise<AxiosResponse<GenericResponse<InventoryFullInfoDto>>> {
    const url = `inventories/${inventoryId}/confirm`;
    return axiosClient.put(url);
  },

  updateInventoryEquipment(params: InventoryEquipmentForm, id: number) : Promise<AxiosResponse<GenericResponse<InventoryEquipmentDto>>> {
    const url = `inventory-equipments/${id}`;
    return axiosClient.put(url, params);
  },

  getListEquipmentsOfDepartment(params: any): Promise<AxiosResponse<GenericResponse<any>>> {
    for (let i in params) {
      if (!params[i]) {
        delete params[i];
      }
    }
    const paramString = new URLSearchParams(params).toString();
    const url = `equipment_inventory/list_equipments_of_department?${paramString}`;
    return axiosClient.get(url);
  },
  createInventoryNotes(params: any): Promise<AxiosResponse<GenericResponse<any>>> {
    const url = 'equipment_inventory/create_inventory_notes';
    return axiosClient.post(url, params);
  },
  updateInventoryNote(params: any): Promise<AxiosResponse<GenericResponse<any>>> {
    const url = 'equipment_inventory/update_inventory_note';
    return axiosClient.patch(url, params);
  },
  getInventoryInfo(equipment_id: any): Promise<AxiosResponse<GenericResponse<any>>> {
    const url = `equipment_inventory/get_inventory_info?equipment_id=${equipment_id}`;
    return axiosClient.get(url);
  },
  approveInventoryNotes(params: any): Promise<AxiosResponse<GenericResponse<any>>> {
    const url = 'equipment_inventory/approve_inventory_notes';
    return axiosClient.patch(url, params);
  },
  getHistoryInventoryOfDepartment(params: any): Promise<AxiosResponse<GenericResponse<any>>> {
    for (let i in params) {
      if (!params[i]) {
        delete params[i];
      }
    }
    const paramString = new URLSearchParams(params).toString();
    const url = `equipment_inventory/history_inventory_of_department?${paramString}`;
    return axiosClient.get(url);
  },
  getHistoryInventoryOfEquipment(params: any): Promise<AxiosResponse<GenericResponse<any>>> {
    for (let i in params) {
      if (!params[i]) {
        delete params[i];
      }
    }
    const paramString = new URLSearchParams(params).toString();
    const url = `equipment_inventory/history_inventory_of_equipment?${paramString}`;
    return axiosClient.get(url);
  },
}

export default equipmentInventoryApi;