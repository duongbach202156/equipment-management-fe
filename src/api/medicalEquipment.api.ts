import axiosClient from './axiosClient';
import { GenericResponse, PageResponse } from 'types/commonResponse.type';
import {  CountEquipmentByDepartment, CountEquipmentByDepartmentAndRiskLevel, CountEquipmentByDepartmentAndStatus, CountEquipmentByGroupAndCategory, StatisticDashboard } from '../types/statistics.type';
import { AxiosResponse } from 'axios';
import { AttachSupplyForm, EquipmentDto, EquipmentFullInfoDto, EquipmentImportExcelForm, EquipmentListDto, GetEquipmentsQueryParam, UpsertEquipmentForm } from '../types/equipment.type';
import { PageableRequest } from '../types/commonRequest.type';
import qs from 'qs';
import { createFormData, reducePageNumberByOne } from '../utils/globalFunc.util';
import { EquipmentSupplyUsageDto } from '../types/equipmentSupplyUsage.type';
import { blob } from 'stream/consumers';
import { GetMedicalEquipmentsQueryParam, MedicalEquipmentDto, MedicalEquipmentFullInfoDto, MedicalEquipmentListDto } from 'types/medicalEquipment.type';

const equipmentApi = {
  detail(id: number): Promise<AxiosResponse<GenericResponse<any>>> {
    const url = `medical-equipment/detail?id=${id}`;
    return axiosClient.get(url);
  },
  update(params: object): Promise<AxiosResponse<GenericResponse<any>>> {
    const url = 'medical-equipment/update';
    return axiosClient.patch(url, params);
  },
  delete(id: number): Promise<AxiosResponse<GenericResponse<any>>> { //OK
    const url = `medical-equipments/${id}`;
    return axiosClient.delete(url);
  },
  search(params: any): Promise<AxiosResponse<GenericResponse<any>>> {
    for (let i in params) {
      if (!params[i]) {
        delete params[i];
      }
    }
    const paramString = new URLSearchParams(params).toString();
    const url = `medical-equipment/search?${paramString}`;
    return axiosClient.get(url);
  },
  uploadExcel(params: any): Promise<AxiosResponse<GenericResponse<any>>> {
    const url = 'medical-equipment/create_by_excel';
    return axiosClient.post(url, params);
  },
  statisticMedicalDashboard(): Promise<AxiosResponse<GenericResponse<StatisticDashboard>>> {
    const url = 'statistics/medical/dashboard';
    return axiosClient.get<GenericResponse<StatisticDashboard>, AxiosResponse<GenericResponse<StatisticDashboard>>>(url);
  },
  statisticOfficeDashboard(): Promise<AxiosResponse<GenericResponse<StatisticDashboard>>> {
    const url = 'statistics/office/dashboard';
    return axiosClient.get<GenericResponse<StatisticDashboard>, AxiosResponse<GenericResponse<StatisticDashboard>>>(url);
  },
  getEquipments(queryParams: GetMedicalEquipmentsQueryParam, pageable: PageableRequest): Promise<AxiosResponse<GenericResponse<PageResponse<MedicalEquipmentListDto>>>> {
    reducePageNumberByOne(pageable);
    const queryString = qs.stringify(queryParams) + '&' + qs.stringify(pageable, { encode: false, arrayFormat: 'repeat' });
    const url = `medical-equipments?${queryString}`;
    return axiosClient.get(url);
  },
  getEquipmentsExcel(queryParams: GetMedicalEquipmentsQueryParam, pageable: PageableRequest): Promise<AxiosResponse<GenericResponse<any>>> {
    reducePageNumberByOne(pageable);
    const queryString = qs.stringify(queryParams) + '&' + qs.stringify(pageable, { encode: false, arrayFormat: 'repeat' });
    const url = `medical-equipments/excel?${queryString}`;
    return axiosClient.get(url);
  },
  importExcel(params: EquipmentImportExcelForm, file : any) {
    const url = 'medical-equipments/excel/import';
    let formData = createFormData('equipment', params, 'file', [file]);
    return axiosClient.post(url, formData.form, formData.config);
  },
  exportExcel() {
    const url = 'medical-equipments/excel/export';
    return axiosClient.get(url);
  },

  exportEquipmentQRTicket(id : number) : any {
    const url = `medical-equipments/${id}/pdf-qr`;
    return axiosClient.get(url);
  },

  exportEquipmentDetail(id : number) : any {
    const url = `medical-equipments/${id}/pdf-detail`;
    return axiosClient.get(url);
  },


  getEquipmentById(id: number): Promise<AxiosResponse<GenericResponse<MedicalEquipmentFullInfoDto>>> {
    const url = `medical-equipments/${id}`;
    return axiosClient.get(url);
  },
  createEquipment(params: UpsertEquipmentForm, image: any): Promise<AxiosResponse<GenericResponse<MedicalEquipmentDto>>> {
    let formData = createFormData('equipment', params, 'image', [image]);
    const url = 'medical-equipments';
    return axiosClient.post(url, formData.form, formData.config);
  },
  updateEquipment(equipmentId: number, params: UpsertEquipmentForm, image: any): Promise<AxiosResponse<GenericResponse<MedicalEquipmentDto>>> {
    let formData = createFormData('equipment', params, 'image', [image]);
    const url = `medical-equipments/${equipmentId}`;
    return axiosClient.put(url, formData.form, formData.config);
  },
  statisticEquipments(queryParams: GetEquipmentsQueryParam,
                      pageable: PageableRequest): Promise<AxiosResponse<GenericResponse<PageResponse<MedicalEquipmentFullInfoDto>>>> {
    const queryString = qs.stringify(queryParams) + '&' + qs.stringify(pageable);
    const url = `medical-equipments/statistics?${queryString}`;
    return axiosClient.get(url);
  }, attachSupply(form:AttachSupplyForm) :Promise<AxiosResponse<GenericResponse<EquipmentSupplyUsageDto>>>{
    const url = `medical-equipments/attach-supplies`;
    return axiosClient.post(url, form);
  }


  ,statisticEquipmentByGroup(): Promise<AxiosResponse<GenericResponse<CountEquipmentByGroupAndCategory[]>>> {
    const url = "statistics/medical-equipments/groups"
    return axiosClient.get(url);
  }
  ,statisticEquipmentByDepartmentAndStatus(): Promise<AxiosResponse<GenericResponse<CountEquipmentByDepartmentAndStatus[]>>> {
    const url = "statistics/medical-equipments/departments/statuses"
    return axiosClient.get(url);
  },
  statisticEquipmentByDepartmentAndRiskLevel(): Promise<AxiosResponse<GenericResponse<CountEquipmentByDepartmentAndRiskLevel[]>>> {
    const url = "statistics/medical-equipments/departments/risk-levels"
    return axiosClient.get(url);
  }

  ,statisticOfficeEquipmentByGroup(): Promise<AxiosResponse<GenericResponse<CountEquipmentByGroupAndCategory[]>>> {
    const url = "statistics/office-equipments/groups"
    return axiosClient.get(url);
  }
  ,statisticOfficeEquipmentByDepartmentAndStatus(): Promise<AxiosResponse<GenericResponse<CountEquipmentByDepartmentAndStatus[]>>> {
    const url = "statistics/office-equipments/departments/statuses"
    return axiosClient.get(url);
  },

  };

  export
  default equipmentApi;